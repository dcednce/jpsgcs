
import jpsgcs.util.Main;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.Linkage;
import jpsgcs.pedmcmc.Inheritance;
import jpsgcs.pedapps.FullInfoLinkageData;
import jpsgcs.pedapps.AncestralAlleles;
import jpsgcs.pedapps.HaplotypeGraphFrame;

public class ViewHaplotypes extends ViewLinkPed
{
	public static void main(String[] args)
	{
		try
		{
			phenos = true;
			liab = true;
			arrows = false;
			sexless = false;
			mono = false;

			args = readOptions(args);

			GeneticDataSource data = null;

			switch(args.length)
			{
			case 2: data = Linkage.read(args[0],args[1]);
				break;
			default:
				System.err.println("Usage: java ViewHaplotypes input.par input.ped [-publish] [-phenos] [-liab] [-arrows] [-sexless] [-mono]");
				System.exit(1);
			}

			AncestralAlleles anc = new AncestralAlleles(data,false);
			anc.update(getInheritances(data));

			HaplotypeGraphFrame frame = new HaplotypeGraphFrame(data,anc,phenos,liab,arrows,sexless,mono);
		}
		catch (Exception e)
		{
			System.err.println("Caught in ViewHaplotypes:main()");
			e.printStackTrace();
		}
	}

	public static Inheritance[][][] getInheritances(GeneticDataSource data)
	{
		Inheritance[][][] h = new Inheritance[data.nLoci()][data.nIndividuals()][2];
		
		for (int j=0; j<data.nIndividuals(); j++)
		{
			int pa = data.pa(j);
			if (pa >= 0)
			{
				for (int i=0; i<data.nLoci(); i++)
				{
					int x = data.getAllele(i,j,0);
					int p = data.getAllele(i,pa,0);
					int m = data.getAllele(i,pa,1);
					
					if (x < 0 || (x != p && x != m))
						continue;
					
					h[i][j][0] = new Inheritance();
					h[i][j][0].setState(x == p ? 0 : 1);
				}
			}

			int ma = data.ma(j);
			if (ma >= 0)
			{
				for (int i=0; i<data.nLoci(); i++)
				{
					int x = data.getAllele(i,j,1);
					int p = data.getAllele(i,ma,0);
					int m = data.getAllele(i,ma,1);
					
					if (x < 0 || (x != p && x != m))
						continue;
					
					h[i][j][1] = new Inheritance();
					h[i][j][1].setState(x == p ? 0 : 1);
				}
			}
		}

		return h;
	}
}
