import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.Linkage;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.pedapps.GeneDropper;
import jpsgcs.pedapps.AlleleSharing;
import java.util.Random;

/**
	This program performs multi locus gene drop simulations in order to assess the
	distribution of the length of  heterozygously shared genomic regions, as
	calculated by <a href="SGS.html"> SGS </a>.

<ul>
	Usage : <b> java SimSGS input.par(ld) input.ped [n] </b> </li>
</ul>
	where 
<ul>
<li> <b> input.par(ld) </b> is the input LINKAGE parameter file which may or may not have and LD model
        appended to it. If no LD model is explicitly specified, linkage equilibrium is assumed. </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
<li> <b> n </b> is an optional parameter specifying the number of simulations to do.
	The default is 1000. </li>
</ul>

<p>
	The input for SimSGS should be exactly the same as that for SGS.
	
<p>
	The output is a table of maximum run lengths. There is one row for each
	simulation, and on each row are the maximum run length over the given markers
	for where the number of sharing individuals, S_i, is equal to n, the number of 
	probands, followed by the maximum run length where S_i >= n-1, n-2, and n-3.
*/

public class SimSGS extends AlleleSharing
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

                        LinkageFormatter parin = null;
                        LinkageFormatter pedin = null;

                        int n_sims = 1000;

                        switch(args.length)
                        {
                        case 3: n_sims = Integer.parseInt(args[2]);

                        case 2: parin = new LinkageFormatter(args[0]);
                                pedin = new LinkageFormatter(args[1]);
                                break;

                        default:
                                System.err.println("Usage: java SimSGS input.par(ld) input.ped [n_sims]");
                                System.exit(1);
                        }

                        GeneticDataSource x = Linkage.read(parin,pedin);
                        LDModel ldmod = new LDModel(parin);
                        if (ldmod.getLocusVariables() == null)
                        {
                                System.err.println("Warning: parameter file has no LD model appended.");
                                System.err.println("Assuming linkage equilibrium and given allele frequencies.");
                                ldmod = new LDModel(x);
                        }

			GeneDropper g = new GeneDropper(x,ldmod,rand);

			int np = x.nProbands();

			for (int i=0; i<n_sims; i++)
			{
				g.geneDrop();
				int[] s = hetSharing(x);
	
				int m1 = max(runs(s,np));
				int m2 = max(runs(s,np-1));
				int m3 = max(runs(s,np-2));
				int m4 = max(runs(s,np-3));
			
				System.out.print("\t"+m1+" "+m2+" "+m3+" "+m4);

				int[] p = pairedSGS(x);
				double[] q = weightedPairedSGS(x);
				System.out.println("\t"+max(p)+"\t"+max(q));
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in SimSGS:main().");
			e.printStackTrace();
		}
	}
}
