
import jpsgcs.util.InputFormatter;

public class BurnAndAverage
{
	public static double log(double x)
	{
		return Math.log(x);
	}

	public static double exp(double x)
	{
		return Math.exp(x);
	}

	public static double logit(double x)
	{
		return Math.log(x/(1-x));
	}

	public static double ilogit(double x)
	{
		return Math.exp(x)/(1+Math.exp(x));
	}

	public static void main(String[] args)
	{
		try
		{
			int burn = 0;
			int max = 100000000;

			switch (args.length)
			{
			case 2: max = Integer.parseInt(args[1]);
				break;

			case 1: burn = Integer.parseInt(args[0]);
				break;
			}

			InputFormatter f = new InputFormatter();
			int n = 0;
			double[] x = null;

			for (int i=0; f.newLine(); i++)
			{
				if (i==0)
					x = new double[f.itemsLeftOnLine()];

				if (i < burn)
					continue;

				if (i >= burn+max)
					break;

				for (int j=0; j<x.length; j++)
					x[j] += f.nextDouble();

/*
				for (int j=0; j<x.length; j++)
				{
					switch(j)
					{
					case 0:
						x[j] += log(f.nextDouble());
						break;
					case 1:
					case 3:
						x[j] += logit(f.nextDouble());
						break;
					case 2:
					case 4:
						x[j] += f.nextDouble();
						break;
					}
				}
*/

				n++;
			}

			for (int j=0; j<x.length; j++)
			{
					System.out.print("\t"+x[j]/n);
/*
				switch(j)
				{
				case 0:
					System.out.print("\t"+exp(x[j]/n));
					break;
				case 1:
				case 3:
					System.out.print("\t"+ilogit(x[j]/n));
					break;
				case 2:
				case 4:
					System.out.print("\t"+x[j]/n);
					break;
				}
*/
			}
			System.out.println();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
