
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.Linkage;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.pedapps.GeneDropper;
import jpsgcs.pedapps.AlleleSharing;
import java.util.Random;

public class SimPairedSGS extends AlleleSharing
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

                        LinkageFormatter parin = null;
                        LinkageFormatter pedin = null;

                        int n_sims = 1000;

                        switch(args.length)
                        {
                        case 3: n_sims = Integer.parseInt(args[2]);

                        case 2: parin = new LinkageFormatter(args[0]);
                                pedin = new LinkageFormatter(args[1]);
                                break;

                        default:
                                System.err.println("Usage: java SimSGS input.par(ld) input.ped [n_sims]");
                                System.exit(1);
                        }

                        GeneticDataSource x = Linkage.read(parin,pedin);
                        LDModel ldmod = new LDModel(parin);
                        if (ldmod.getLocusVariables() == null)
                        {
                                System.err.println("Warning: parameter file has no LD model appended.");
                                System.err.println("Assuming linkage equilirbiurm and given allele frequencies.");
                                ldmod = new LDModel(x);
                        }

			GeneDropper g = new GeneDropper(x,ldmod,rand);

			int np = x.nProbands();

			for (int i=0; i<n_sims; i++)
			{
				g.geneDrop();

				int[] h = hetSharing(x);
				int[] r = runs(h,x.nProbands());
				int[] s = pairedSGS(x);
				double[] t = weightedPairedSGS(x);

				System.out.println(max(r)+"\t"+max(s)+"\t"+max(t));
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in SimSGS:main().");
			e.printStackTrace();
		}
	}
}
