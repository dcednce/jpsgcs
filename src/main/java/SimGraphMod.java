import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.Variable;
import jpsgcs.markov.Function;
import jpsgcs.markov.GraphicalModel;
import java.util.Random;

public class SimGraphMod
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			double nsims = 1000;
			int nvert = 6;
			double prob = 0.9;

			switch(args.length)
			{
			case 2: prob = Double.parseDouble(args[1]);
			case 1: nsims = Double.parseDouble(args[0]);
				break;

			default:
				break;
			}

			MarkovRandomField p = new MarkovRandomField();

			Variable[] v = new Variable[nvert];
			for (int i=0; i<v.length; i++)
				v[i] = new Variable(2);

			p.add(new MyFunc(v[0],v[1],prob));
			p.add(new MyFunc(v[1],v[2],prob));
			p.add(new MyFunc(v[2],v[0],prob));
			p.add(new MyFunc(v[0],v[3],prob));
			p.add(new MyFunc(v[3],v[1],prob));

			p.add(new MyFunc(v[0],v[4],prob));
			p.add(new MyFuncOne(v[5],prob));

			GraphicalModel g = new GraphicalModel(p,rand,true);

			for (int i=0; i<nsims; i++)
			{
				g.simulate();
				for (int j=0; j<v.length; j++)
					System.out.print(v[j].getState()+" ");
				System.out.println();
			}
		}
		catch (Exception e)
		{
		}
	}
}

class MyFuncOne extends Function
{
	public MyFuncOne(Variable a, double prob)
	{
		v = new Variable[1];
		v[0] = a;
		p = prob;
	}

	public double getValue()
	{
		return v[0].getState() == 1 ? p : 1-p;
	}

	public Variable[] getVariables()
	{
		return v;
	}

	private Variable[] v = null;
	private double p = 0.5;
}

class MyFunc extends Function
{
	public MyFunc (Variable a, Variable b, double prob)
	{
		v = new Variable[2];
		v[0] = a;
		v[1] = b;
		p = prob;
	}

	public double getValue()
	{
		return v[0].getState() == v[1].getState() ? p : 1-p;
	}

	public Variable[] getVariables()
	{
		return v;
	}

	private Variable[] v = null;
	private double p = 0;
}
