
import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.pedmcmc.LocusProduct;
import java.util.Random;

/**
	This program calculates the log likelihood for the data for each pedigree at 
	each locus specified in the input.
<ul>
	Usage : <b> java LocusLogLikelihoods input.par input.ped </b> </li>
</ul>
	where 
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
</ul>

<p>
	The output is a matrix with row for each kindred in the pedigree file. On each
	row is the log likelihood for the data at each locus.

*/

public class LocusLogLikelihoods
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			GeneticDataSource[] x = null;

			switch(args.length)
			{
			case 2: x = Linkage.readAndSplit(args[0],args[1]);
				break;

			default: 
				System.err.println("Usage: java LocusLogLikelihoods input.par input.ped");
				System.exit(1);
			}

			for (GeneticDataSource data : x)
			{
				data.downcodeAlleles();

				for (int i=0; i<data.nLoci(); i++)
				{
					LocusProduct p = new LocusProduct(data,i);
					GraphicalModel g = new GraphicalModel(p,rand,false);
					System.out.printf("%6.4f ",g.logPeel());
				}
				System.out.println();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in LocusLogLikelihoods:main()");
			e.printStackTrace();
		}
	}
}
