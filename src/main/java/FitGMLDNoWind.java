import jpsgcs.util.InputFormatter;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.Linkage;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedapps.CompleteHaplotypes;
import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;
import jpsgcs.jtree.MultinomialMLEScorer;
import jpsgcs.jtree.JTree;
import jpsgcs.jtree.JTreeSampler;
import jpsgcs.jtree.UniformJTree;
import jpsgcs.jtree.UniformDecomposable;
import jpsgcs.jtree.GiudiciGreen;
import jpsgcs.graph.Network;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.util.Monitor;
import jpsgcs.util.Main;

import java.io.PrintStream;
import java.util.ConcurrentModificationException;
import java.util.Random;

public class FitGMLDNoWind
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			Monitor.quiet(false);

			boolean visualize = false;
			double eprob = 0.001;
			double penalty = 1;

			int totalits = 1000000;
			int metropergibbs = 10000;
			int randomits = 1000;

			String[] bargs = Main.strip(args,"-v");
			if (bargs != args)
			{
				visualize = true;
				args = bargs;
			}

			GeneticDataSource x = null;
			LinkageParameterData par = null;
			LinkagePedigreeData ped = null;

			switch (args.length)
			{
			case 6: eprob = Double.parseDouble(args[5]);

			case 5: penalty = Double.parseDouble(args[4]);
		
			case 4: metropergibbs = Integer.parseInt(args[3]);

			case 3: totalits = Integer.parseInt(args[2]);

			case 2: par = new LinkageParameterData(new LinkageFormatter(args[0]));
                                ped = new LinkagePedigreeData(new LinkageFormatter(args[1]),par);
                                x = new LinkageInterface(new LinkageDataSet(par,ped));
				break;

			default:
				System.err.println("Usage: java FitGMLDNoWind in.par in.ped [s] [m] [-v]");
				System.exit(1);
			}

			Monitor.show("Read");

			// Initalize the LD model and get the variables.

			LDModel ld = new LDModel(x);
			Variable[] loci = ld.getLocusVariables();

			Monitor.show("LDModel");

			// Set up a view of the input genotypes as a completely imputed haplotypes set.

			CompleteHaplotypes haps = new CompleteHaplotypes(x,ld,eprob,rand);
			haps.simulate();

			// Give the data to a log-likelihood/df calculator.

			penalty *= 0.5 * Math.log(haps.getHaplotypes().length);
			MultinomialMLEScorer scorer= new MultinomialMLEScorer(loci,haps,penalty,rand);

			Monitor.show("Scorer");

			// Set the inital graphical model to be the trivial graph, and find the junction tree.
			// Make a graph viewer if required.

			Network<Variable,Object> g = new Network<Variable,Object>();
			for (Variable v : loci)
				g.add(v);

			//for (int i=1; i<loci.length; i++)
			//	g.connect(loci[i],loci[0]);

			GraphFrame frame = ( visualize ? new GraphFrame<Variable,Object>(g) : null );

			// Set up the sampler, and randomize the intitial configuration.

			JTree<Variable> jt = new JTree<Variable>(g,rand);
			//JTreeSampler<Variable> jts = new UniformDecomposable<Variable>(jt,scorer);
			//JTreeSampler<Variable> jts = new GiudiciGreen<Variable>(jt,scorer);
			JTreeSampler<Variable> jts = new UniformJTree<Variable>(jt,scorer);
			jts.randomize();

			Monitor.show("Randomized");

			// Go into the simulation loop.

			jts.setTemperature(1);

			for (int i=1; i<=totalits; i++)
			{
				if (i % metropergibbs == 0)
				{
					ld.clear();
					for (Function f : scorer.fitModel(jt).getFunctions())
						ld.add(f);

					haps.setModel(ld);
					haps.simulate();
					scorer.clear();

					System.err.print("-");
				}

				if (i % randomits == 0)
					jts.randomize();
					
				jts.randomUpdate();
			}

			System.err.println();

			// Go into the maximization loop.

			jts.setTemperature(0);
			scorer.clear();

			for (int i=1; i<=totalits; i++)
			{
				if (i % metropergibbs == 0)
				{
					ld.clear();
					for (Function f : scorer.fitModel(jt).getFunctions())
						ld.add(f);

					haps.setModel(ld);
					haps.maximize();
					scorer.clear();

					System.err.print("+");
				}

				if (i % randomits == 0)
					jts.randomize();
					
				if (rand.nextDouble() < 0.5)
					jts.randomConnection();
				else
					jts.randomDisconnection();
			}

			System.err.println();

			// Output model.

			par.writeTo(System.out);
			System.out.println();
			ld.writeStateSizes(System.out);
			ld.writeFunctions(System.out);

			Monitor.show("Done");
		}
		catch (ConcurrentModificationException cme)
		{
		}
		catch (Exception e)
		{
			System.err.println("Caught in FitGMLDNoWind.main()");
			e.printStackTrace();
		}
	}
}
