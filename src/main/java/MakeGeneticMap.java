import jpsgcs.pedapps.GeneticMap;
import jpsgcs.util.InputFormatter;

public class MakeGeneticMap 
{
	public static void main(String[] args)
	{
		try
		{
			switch(args.length)
			{
			case 2: break;
			default:
				System.err.println("Usage: java GeneticMap knownmap newmap");
				System.exit(1);
			}

			GeneticMap gm = GeneticMap.readMap(args[0]);
			InputFormatter f = new InputFormatter(args[1]);
			long prev = 0;

			while (f.newLine())
			{
				System.out.print(f.nextString()+"\t"+f.nextString());
				long phys = f.nextLong();
				if (phys < prev)
					System.err.println("Physical locations must be increasings. Decreasing on line "+f.lastLine());

				double cm = gm.centiMorgan(phys);
				double pcm = gm.centiMorgan(prev);
				double theta = gm.cMToThetaKosambi(phys-pcm);
				prev = phys;

				System.out.println("\t"+cm+"\t"+phys+"\t"+theta);
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in MakeGeneticMap.main()");
			e.printStackTrace();
		}
	}
}
