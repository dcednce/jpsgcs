import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedapps.HaplotypeGraphFrame;

import jpsgcs.util.Main;

/**
 	This is an interactive graphical program for viewing pedigree data.

<ul>
        Usage : <b> java ViewLinkPed < input.ped </b> </li>
</ul>
        where
<ul>
<li> <b> input.ped </b> is a LINKAGE pedigree file. </li>
</ul>
<p>
	The controls are similar to those for 
	<a href="ViewGraph.html"> ViewGraph. </a>
*/

public class ViewLinkPed extends Main
{
	public static boolean usetriplets = false;
	public static boolean arrows = true;
	public static boolean sexless = false;
	public static boolean phenos = false;
	public static boolean liab = false;
	public static boolean mono = false;

	public static void main(String[] args)
	{
		try
		{
			arrows = true;
			sexless = false;
			phenos = false;
			liab = false;
			mono = false;

			args = readOptions(args);

			GeneticDataSource data = null;

			if (usetriplets) 
			{
				data = Linkage.readTriplets();
				phenos = false;
				liab = false;
			}
			else
			{
				LinkageParameterData par = null;
				LinkagePedigreeData ped = null;

				switch (args.length)
				{
				case 2: par = new LinkageParameterData(args[0]);
					ped = new LinkagePedigreeData(args[1],par);
					break;

				case 1: ped = new LinkagePedigreeData(args[0],par);
					break;

				case 0: ped = new LinkagePedigreeData(new LinkageFormatter(),par);
					break;

				default:
					System.err.println("Usage: java ViewLinkPed [input.par] input.ped [-publish] [-phenos] [-liab] [-arrows] [-sexless]  [-mono]");
					System.exit(1);
				}

				data = new LinkageInterface(new LinkageDataSet(par,ped));
			}

			HaplotypeGraphFrame f = new HaplotypeGraphFrame(data,phenos,liab,arrows,sexless,mono);
		}
		catch (Exception e)
		{
			System.err.println("Caught in ViewLinkPed:main()");
			e.printStackTrace();
		}
	}

	public static String[] readOptions(String[] args)
	{
		String[] bargs = strip(args,"-publish");
		if (bargs != args)
		{
			phenos = true;
			liab = true;
			mono = true;
			arrows = false;
			sexless = true;
			args = bargs;
		}

		bargs = strip(args,"-phenos");
		if (bargs != args)
		{
			phenos = !phenos;
			args = bargs;
		}

		bargs = strip(args,"-liab");
		if (bargs != args)
		{
			liab = !liab;
			args = bargs;
		}

		if (liab)
			phenos = true;

		bargs= strip(args,"-arrows");
		if (bargs != args)
		{
			arrows = !arrows;
			args = bargs;
		}

		bargs = strip(args,"-sexless");
		if (bargs != args)
		{
			sexless = !sexless;
			args = bargs;
		}

		bargs = strip(args,"-mono");
		if (bargs != args)
		{
			mono = !mono;
			args = bargs;
		}

		return args;
	}
}

