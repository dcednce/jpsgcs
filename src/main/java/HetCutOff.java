import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.util.Main;
import jpsgcs.pedapps.GeneCounter;
import java.io.PrintStream;
import java.util.Vector;


/**
	This program selects from the input data the marker loci for which the
	heterozygosity score is higher than a given threshold.
	By default the score is calculated from the frequencies in the given parameter file,
	not as estimated from the data. If you need the heterozygosities 
	calculated from the data, use the <b> -r </b> option or,use GeneCountAlleles to first calculate
	the observed frequencies.
<ul>
	Usage : <b> java HetCutOff input.par input.ped [output.par] [output.ped] [thresh] [-r]</b> </li>
</ul>
	where 
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
<li> <b> output.par </b> is the output LINKAGE parameter file. 
	The default is to write to standard output.</li>
<li> <b> output.ped </b> is the output LINKAGE pedigree file. 
	The default is to write to standard output.</li>
<li> <b> thresh </b> is the minimum hetrozygosity required. 
	The default value is 0.1.</li>
<li> <b> -r </b> if this option is specified frequencies are recalcuated from the data in
	in the pedigree file and then used for the heterozygosity scores. </li>
</ul>

<p>
	The heterozygosity
	score is 1 minus the sum of the squared allele frequencies. This value
	is between 0 and 0.5.

*/

public class HetCutOff
{
	public static void main(String[] args)
	{
		try
		{
			boolean recalculate = false;
			double thresh = 0.1;
			LinkageDataSet ld = null;
			PrintStream parout = System.out;
			PrintStream pedout = System.out;

			String[] bargs = Main.strip(args,"-r");
			if (bargs != args)
			{
				recalculate = true;
				args = bargs;
			}

			switch(args.length)
			{
			case 5: thresh = Double.parseDouble(args[4]);
			case 4: pedout = new PrintStream(args[3]);
			case 3: parout = new PrintStream(args[2]);
			case 2: ld = new LinkageDataSet(args[0],args[1]);
				break;
			default:
				System.err.println("Usage: java HetCutOff input.par input.ped [output.par] [output.ped] [thresh]");
				System.exit(0);
			}

			GeneticDataSource d = new LinkageInterface(ld);
			if (recalculate)
			{
				System.err.println("Recalculating allele frequencies fromt the given data");
				for (int i=1; i<d.nLoci(); i++)
					GeneCounter.geneCount(d,i,0.000001,100);
			}

			Vector<Integer> out = new Vector<Integer>();
			for (int i=0; i<d.nLoci(); i++)
			{
				double[] a = d.alleleFreqs(i);
				double h = 0;
				for (int j=0; j<a.length; j++)
					h += a[j];
				for (int j=0; j<a.length; j++)
					a[j] /= h;
				h = 1;
				for (int j=0; j<a.length ;j++)
					h -= a[j]*a[j];
				
				if (h > thresh)
                                    out.add(i);
			}

			if (out.size() == 0)
			{
				System.err.println("No loci reached the required threshold.");
				System.exit(1);
			}

			int[] x = new int[out.size()];
			for (int i=0; i<out.size(); i++)
				x[i] = out.get(i).intValue();
			
			ld = new LinkageDataSet(ld,x);
			ld.getParameterData().writeTo(parout);
			ld.getPedigreeData().writeTo(pedout);
		}
		catch (Exception e)
		{
			System.err.println("Caught in GetPolymorphisms:main()");
			e.printStackTrace();
		}
	}
}
