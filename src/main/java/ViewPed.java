import jpsgcs.linkage.Linkage;
import jpsgcs.genio.PedigreeData;
import jpsgcs.pedapps.MarriageNodeGraph;
import jpsgcs.viewgraph.VertexRepresentation;
import jpsgcs.viewgraph.PaintableGraph;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.graph.GraphLocator;
import jpsgcs.graph.DAGLocator;

import java.util.Map;
import java.util.LinkedHashMap;
import java.awt.Panel;
import java.awt.Frame;

/**
        This is an interactive graphical program for viewing pedigree data.

<ul>
        Usage : <b> java ViewPed < triplet.file </b> </li>
</ul>
        where
<ul>
<li> <b> triplet.file </b> is a file specifiying the pedigree as a list
        of triplet. One triplet per line. Order is assumed to be individual id,
        father's id, mother's id. Any other data following this is ignored.  </li>
</ul>

<p>
        The controls are the same as those for
        <a href="ViewLinkPed.html"> ViewLinkPed. </a>

*/

public class ViewPed extends ViewLinkPed
{
	static
	{
		usetriplets = true;
	}

/*
	public static void main(String[] args)
	{
		try
		{
			PedigreeData data = Linkage.readTriplets();
			launchGUI(data);
		}
		catch (Exception e)
		{
			System.err.println("Caught in ViewLinkPed:main()");
			e.printStackTrace();
		}
	}
*/
}
