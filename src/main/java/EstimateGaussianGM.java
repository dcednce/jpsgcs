
import jpsgcs.jtree.ContinuousDataMatrix;
import jpsgcs.jtree.WSMGraphLaw;
import jpsgcs.jtree.SMGraphLaw;
import jpsgcs.jtree.GaussianMarginalLikelihood;
import jpsgcs.jtree.EdgePenaltyPrior;
import jpsgcs.jtree.ProductGraphLaw;
import jpsgcs.jtree.JTree;
import jpsgcs.jtree.JTreeSampler;
import jpsgcs.jtree.GiudiciGreen;
import jpsgcs.jtree.UniformDecomposable;
import jpsgcs.jtree.MultiPairJTree2;
import jpsgcs.jtree.SubMatrix;
import jpsgcs.graph.Network;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.markov.Parameter;
import jpsgcs.util.Monitor;
import jpsgcs.util.ArgParser;

import java.util.ConcurrentModificationException;
import java.util.Random;
import java.util.Set;

public class EstimateGaussianGM
{
	public static void main(String[] args)
	{
		try
		{
		// Create random number generator.

			Random rand = new Random();

		// Set the parameters.

			ArgParser ap = new ArgParser(args);

			boolean average = !ap.gotOpt("-mean");
			boolean lastone = !ap.gotOpt("-final");
			boolean showgraph = !ap.gotOpt("-v");
			int n_samples = ap.intAfter("-n",10000000);
			int sampler = ap.intAfter("-s",0); 
			int randomits = ap.intAfter("-r",1000);
			double penalty = ap.doubleAfter("-p",0.0);

		// Read the data.

			ContinuousDataMatrix data = new ContinuousDataMatrix();

		// Create a graph of Integers for the conditional independence graph
		// and find a junction tree of the graph.

			Network<Integer,Object> g = new Network<Integer,Object>();
			for (int i=0; i<data.nColumns(); i++)
                            g.add(i);

			JTree<Integer> jt = new JTree<Integer>(g,rand);

		// Make a prior that penalizes the number of edges in the graph.
		// Default is to have no penalty.

			WSMGraphLaw<Integer> prior = new EdgePenaltyPrior<Integer>(penalty);

		// Make a Gaussian marginal likelihood from the data.

			//SMGraphLaw<Integer> likelihood = new GaussianMarginalLikelihood(data,1,1);
			GaussianMarginalLikelihood likelihood = new GaussianMarginalLikelihood(data,1,1);

		// Make a posterior by combining the prior and likelihood.

			WSMGraphLaw<Integer> posterior = new ProductGraphLaw<Integer>(prior,likelihood);
			
		// Create a junction tree sampler to sample from the poserior.

		 	JTreeSampler<Integer> jts = null;

			switch(sampler)
			{
			// Samples uniformly on decomposable graphs.
			case 2: jts = new GiudiciGreen<Integer>(jt,posterior);
				break;

			// Samples uniformly on decomposable graphs.
			case 1: jts = new UniformDecomposable<Integer>(jt,posterior);
				break;
			
			// Samples uniformly on decomposable graphs.
			case 0:
			default:
				jts = new MultiPairJTree2<Integer>(jt,posterior,true);
			}

		// Randomize the initial junction tree.

			jts.randomize();

		// Create a slider bar to control annealing temperature. Scale is %.

			Parameter temp = new Parameter("Temperature",0,10,1);

		// Create the graphial interface elements.

			GraphFrame frame = null;
			if (showgraph)
				frame = new GraphFrame<Integer,Object>(g,temp);

		// Run the sampler.

			double[][] x = new double[data.nColumns()][data.nColumns()]; 
			double nprec = 0;

			Monitor.restart();
			Monitor.show("Start");
		
			for (int i=1; i<=n_samples; i++)
			{
				if (i % 10000 == 0)
					System.err.print(".");
				if (i % 1000000 == 0)
					System.err.println();

				jts.setTemperature(temp.getValue());

				if (i % randomits == 0)
				{
					jts.randomize();

					if (average)
					{
						nprec += 1;

						for (Set<Integer> c : jt.getCliques())
						{
							SubMatrix m = likelihood.subPrecision(c);
							for (int k=0; k<m.index.length; k++)
								for (int j=0; j<m.index.length; j++)
									x[m.index[k]][m.index[j]] += m.matrix[k][j];
						}
	
						for (Set<Integer> s : jt.getAllSeparators())
						{
							SubMatrix m = likelihood.subPrecision(s);
							for (int k=0; k<m.index.length; k++)
								for (int j=0; j<m.index.length; j++)
									x[m.index[k]][m.index[j]] -= m.matrix[k][j];
						}
					}

				}

				jts.randomUpdate();
			}

			System.err.println("Number of graphs sampled = "+nprec);

			if (average)
			{
				System.out.println();

				for (int i=0; i<x.length; i++)
				{
					for (int j=0; j<x[i].length; j++)
						System.out.printf("%8.4f\t",x[i][j]/nprec);
					System.out.println();
				}
			}

			if (lastone)
			{
				for (int i=0; i<x.length; i++)
					for (int j=0; j<x[i].length; j++)
						x[i][j] = 0;

				for (Set<Integer> c : jt.getCliques())
				{
					SubMatrix m = likelihood.subPrecision(c);
					for (int k=0; k<m.index.length; k++)
						for (int j=0; j<m.index.length; j++)
							x[m.index[k]][m.index[j]] += m.matrix[k][j];
				}

				for (Set<Integer> s : jt.getAllSeparators())
				{
					SubMatrix m = likelihood.subPrecision(s);
					for (int k=0; k<m.index.length; k++)
						for (int j=0; j<m.index.length; j++)
							x[m.index[k]][m.index[j]] -= m.matrix[k][j];
				}

				System.out.println();

				for (int i=0; i<x.length; i++)
				{
					for (int j=0; j<x[i].length; j++)
						System.out.printf("%8.4f\t",x[i][j]);
					System.out.println();
				}
			}

			Monitor.show("Done");
		}
		catch (ConcurrentModificationException cme)
		{
		}
		catch (Exception e)
		{
			System.err.println("Caught in EstimateGaussianGM.main()");
			e.printStackTrace();
		}
	}
}
