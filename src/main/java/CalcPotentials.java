import jpsgcs.markov.Variable;
import jpsgcs.jtree.DiscreteDataMatrix;
import jpsgcs.jtree.MultinomialMarginalLikelihood;
import java.util.Set;
import java.util.TreeSet;
import java.util.ConcurrentModificationException;
import java.util.Random;

public class CalcPotentials
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			DiscreteDataMatrix data = new DiscreteDataMatrix();

			Variable[] vars = new Variable[data.nColumns()];
			for (int j=0; j<vars.length; j++)
				vars[j] = new Variable(data.statesOfColumn(j));

			MultinomialMarginalLikelihood like = new MultinomialMarginalLikelihood(vars,data,1);

			Set<Variable> set = new TreeSet<Variable>();
			Set<Variable> cut = new TreeSet<Variable>();
			Set<Variable> sep = new TreeSet<Variable>();

			double tot = 0;

/*
			for (int i=0; i < 2000; i++)
			{
				int j = (int) (rand.nextDouble() * vars.length);
				
				if (rand.nextDouble() < 0.5)
					set.add(vars[j]);
				else
					set.remove(vars[j]);

				//System.out.println(set+"\t"+like.logPotential(set));
				tot += like.logPotential(set);
			}
*/

			//like.showStore();

/*
			for (int i=0; i<vars.length; i++)
			{
				Set<Variable> si = new TreeSet<Variable>();
				si.add(vars[i]);
				for (int j=i+1; j<vars.length; j++)
				{
					Set<Variable> sj = new TreeSet<Variable>();
					sj.add(vars[j]);
					set.clear();
					set.add(vars[i]);
					set.add(vars[j]);
					System.out.println(set+"\t"+
						like.logPotential(set)+"\t"+
						like.logPotential(si)+"\t"+
						like.logPotential(sj)+"\t"+
			(like.logPotential(set)-like.logPotential(si) - like.logPotential(sj)));
						
				}
			}
*/

/*

			cut.add(vars[5]);
			System.out.println(cut+"\t"+sep+"\t"+
				like.logPotential(cut)+"\t"+
				like.logPotential(sep)+"\t"+
			(like.logPotential(cut)-like.logPotential(sep)));

			cut.clear();
			cut.add(vars[0]);
			cut.add(vars[4]);
			sep.add(vars[0]);
			System.out.println(cut+"\t"+sep+"\t"+
				like.logPotential(cut)+"\t"+
				like.logPotential(sep)+"\t"+
			(like.logPotential(cut)-like.logPotential(sep)));

			cut.clear();
			sep.clear();
			cut.add(vars[0]);
			cut.add(vars[1]);
			cut.add(vars[2]);
			sep.add(vars[0]);
			sep.add(vars[1]);
			System.out.println(cut+"\t"+sep+"\t"+
				like.logPotential(cut)+"\t"+
				like.logPotential(sep)+"\t"+
			(like.logPotential(cut)-like.logPotential(sep)));

			cut.clear();
			cut.add(vars[0]);
			cut.add(vars[1]);
			cut.add(vars[3]);
			System.out.println(cut+"\t"+sep+"\t"+
				like.logPotential(cut)+"\t"+
				like.logPotential(sep)+"\t"+
			(like.logPotential(cut)-like.logPotential(sep)));
			
			cut.add(vars[0]);
			cut.add(vars[1]);
			cut.add(vars[2]);
			System.out.println(cut+"\t"+like.logPotential(cut));
			
			double xx = 0;

			cut.clear();
			cut.add(vars[0]);
			cut.add(vars[1]);
			xx += like.logPotential(cut);
			System.out.println(cut+"\t"+like.logPotential(cut));
			
			cut.clear();
			cut.add(vars[0]);
			cut.add(vars[2]);
			xx += like.logPotential(cut);
			System.out.println(cut+"\t"+like.logPotential(cut));
			
			cut.clear();
			cut.add(vars[1]);
			cut.add(vars[2]);
			xx += like.logPotential(cut);
			System.out.println(cut+"\t"+like.logPotential(cut));
			
			cut.clear();
			cut.add(vars[0]);
			xx -= like.logPotential(cut);
			System.out.println(cut+"\t"+like.logPotential(cut));
			
			cut.clear();
			cut.add(vars[1]);
			xx -= like.logPotential(cut);
			System.out.println(cut+"\t"+like.logPotential(cut));
			
			cut.clear();
			cut.add(vars[2]);
			xx -= like.logPotential(cut);
			System.out.println(cut+"\t"+like.logPotential(cut));
			
			System.out.println(xx);
*/

			cut.clear();
			sep.clear();
			cut.add(vars[5]);
			double old = like.logPotential(cut)-like.logPotential(sep);
			System.out.println(
				like.logPotential(cut)+"\t"+
				like.logPotential(sep)+"\t"+
				old);

			cut.clear();
			sep.clear();
			cut.add(vars[5]);
			cut.add(vars[0]);
			sep.add(vars[0]);
			double gnu  = like.logPotential(cut)-like.logPotential(sep);
			System.out.println(
				like.logPotential(cut)+"\t"+
				like.logPotential(sep)+"\t"+
				gnu);

			System.out.println(old-gnu);

		}
		catch (ConcurrentModificationException cme)
		{
		}
		catch (Exception e)
		{
			System.err.println("Caught in FitMarkovLoop.main()");
			e.printStackTrace();
		}
	}
}
