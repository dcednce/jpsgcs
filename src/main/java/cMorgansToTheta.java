import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkageFormatter;

/**
        This program reads in a LINKAGE parameter file assuming that the 
	distances between the loci are specified as cMorgans
	and converts these to recombination fractions.

<ul>
        Usage : <b> java cMorgansToTheta < input.par > output.par </b> </li>
</ul>
        where
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> output.par </b> is the checked output LINKAGE parameter file.
</ul>

<p>
	The results are output to a new LINKAGE parameter file.
*/

public class cMorgansToTheta
{
	public static void main(String[] args)
	{
		try
		{
			LinkageParameterData p = new LinkageParameterData(new LinkageFormatter(),true);
			p.writeTo(System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in CheckParameters:main().");
			System.err.println("Likely to be an unexpected type of data formatting error.");
			e.printStackTrace();
		}
	}
}
