import jpsgcs.util.Main;
import jpsgcs.infect.Event;
import jpsgcs.infect.InfSimulator;
import java.util.Random;

public class SimInfection
{
	public static void main(String[] args)
	{
		Random rand = new Random();

		double transmission = 0.01;
		double importation = 0.1;
		double detection = 0.95;

		double ndays = 100; // Total time in days.
		int capacity = 100;

		double meanstay = 10.0;
		double balance = 0.75;
		double testperpatday = 0.10;

		boolean withinf = false;
		boolean testonadmit = true; 
		//boolean testonadmit = false;

		String[] bargs = Main.strip(args,"+inf");
		if (bargs != args)
		{
			args = bargs;
			withinf = true;
		}
		
		bargs = Main.strip(args,"-inf");
		if (bargs != args)
		{
			args = bargs;
			withinf = false;
		}
		
		switch(args.length)
		{
		case 8: testperpatday = Double.parseDouble(args[7]);

		case 7: balance = Double.parseDouble(args[6]);

		case 6: meanstay = Double.parseDouble(args[5]);

		case 5: capacity = Integer.parseInt(args[4]);

		case 4: ndays = Integer.parseInt(args[3]);

		case 3: detection = 1 - Double.parseDouble(args[2]);
			
		case 2: importation = Double.parseDouble(args[1]);
		
		case 1: transmission = Double.parseDouble(args[0]);

		case 0: break;
		
		default:
			System.err.println("Usage: java SimInfection [tranmission rate | 0.01] [imprtation prob | 0.1]"+
				" [false negative prob | 0.05] [days simulated | 100] [number of beds | 100]"+
				" [mean stay | 10.0] [balance | 0.75] [tests per patient per day | 0.1]");
			System.exit(1);
		}

//		System.err.println(transmission+"\t"+importation+"\t"+0+"\t"+(1-detection));

		InfSimulator s = new InfSimulator(transmission,importation,detection,ndays,capacity,meanstay,balance,testperpatday,withinf,testonadmit,rand);

		while (s.hasNext())
		{
			System.out.println(s.nextEvent());
		}
	}
}
