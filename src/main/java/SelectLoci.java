import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.pedmcmc.LDModel;

import java.io.PrintStream;
import java.util.Vector;

/**
 	This program selects a subset of loci from LINKAGE parameter and pedigree
	files.

<ul>
        Usage : <b> java SelectLoci input.par input.ped output.par output.ped l1 [l2] ...</b> </li>
</ul>
        where
<ul>
<li> <b> input.par </b> is the original LINKAGE parameter file </li>
<li> <b> input.ped </b> is the original LINKAGE pedigree file </li>
<li> <b> output.par </b> is the file where the selected loci will be put.</li>
<li> <b> output.ped </b> is the file where the matching pedigree data will be put.</li>
<li> <b> l1 [l2] ... </b> is a list of at least one locus index. The indexes match the order of the 
	loci in the input parameter file. The first locus has index 0, the last of n has index n-1.</li>
</ul>

*/
	
public class SelectLoci
{
	public static void main(String[] args)
	{
		try
		{
			if (args.length < 5)
				System.err.println("Usage: java SelectLoci input.par input.ped output.par output.ped l1 [l2] ...");

			LinkageFormatter parin = new LinkageFormatter(args[0]);
			LinkageFormatter pedin = new LinkageFormatter(args[1]);
			PrintStream parout = new PrintStream(args[2]);
			PrintStream pedout = new PrintStream(args[3]);

			LinkageDataSet l = new LinkageDataSet(parin,pedin);
			LDModel ldmod = new LDModel(parin);
		
			Vector<Integer> out = new Vector<Integer>();
			for (int i=4; i<args.length; i++)
			{
				if (args[i].contains("-"))
				{
					String[] s = args[i].split("-");
					int a = Integer.parseInt(s[0]);
					int b = 0;
					if (s.length == 2)
                                            b = Integer.parseInt(s[1]);
					else
						b = l.getParameterData().nLoci()-1;

					if (b > l.getParameterData().nLoci()-1)
						b = l.getParameterData().nLoci()-1;

					for (int k=a; k<=b; k++)
                                            out.add(k);
				}
				else
                                    out.add(Integer.parseInt(args[i]));
			}

			int[] x = new int[out.size()];
			for (int i=0; i<x.length; i++)
				x[i] = out.get(i).intValue();

			l = new LinkageDataSet(l,x);
			l.getParameterData().writeTo(parout);
			l.getPedigreeData().writeTo(pedout);

			if (ldmod.getLocusVariables() != null)
			{
				//ldmod.peelTo(x);
				ldmod = new LDModel(ldmod,x);
				ldmod.writeTo(parout);
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in SelectLoci.main()");
			e.printStackTrace();
		}
	}
}
