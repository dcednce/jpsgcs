import jpsgcs.util.Monitor;
import jpsgcs.util.InputFormatter;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.Linkage;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.SteppedLinkageData;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;
import jpsgcs.pedmcmc.Genotype;
import jpsgcs.pedmcmc.GenotypePenetrance;
import jpsgcs.pedmcmc.GenotypeErrorPenetrance;
import jpsgcs.pedmcmc.GenotypePrior;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedmcmc.Error;
import jpsgcs.pedmcmc.ErrorPrior;
import jpsgcs.util.Main;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.pedmcmc.LocusSampler;
import jpsgcs.pedmcmc.TrueLDMultiLocusSampler;
import java.util.Random;

public class PedComplete
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			Monitor.quiet(false);

			boolean sample = false;
			double eprob = 0.000;
			int maxerrall = 0;
			boolean linkfirst = true;

			LinkageFormatter parin = null;
			LinkageFormatter pedin = null;

			String[] bargs = Main.strip(args,"-r");
			if (bargs != args)
			{
				sample = true;
				args = bargs;
			}

			switch (args.length)
			{
			case 2: parin = new LinkageFormatter(args[0]);
				pedin = new LinkageFormatter(args[1]);
				break;

			default:
				System.err.println("Usage: java PedComplete in.parld in.ped [-r] > out.ped");
				System.exit(1);
			}

			GeneticDataSource x = Linkage.read(parin,pedin);
			Monitor.show("Data read.");

			LDModel ld = new LDModel(parin);

			if (ld.getLocusVariables() == null)
			{
				System.err.println("Warning: parameter file has no LD model appended.");
				System.err.println("Will assume linakge equilibrium and given allele frequencies.");
				ld = new LDModel(x);
			}
			Monitor.show("LD model read.");

			LocusSampler mc = new TrueLDMultiLocusSampler(x,ld,linkfirst,eprob,maxerrall,false,rand);
			Monitor.show("Sampler made.");

			mc.maximize();
			Monitor.show("Sampler run.");

			for (int i=0; i<x.nLoci(); i++)
				for (int j=0; j<x.nIndividuals(); j++)
					x.setAlleles(i,j,mc.getAlleles()[i][j][0].getState(),mc.getAlleles()[i][j][1].getState());

			x.writePedigree(System.out);

		}
		catch (Exception e)
		{
			System.err.println("Caught in PedComplete.main()");
			e.printStackTrace();
		}
	}
}
