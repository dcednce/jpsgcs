package jpsgcs.stoch;

import jpsgcs.graph.Graph;
import jpsgcs.graph.Network;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.Map;
import java.util.LinkedHashSet;
import java.util.LinkedHashMap;

public class StochSys implements Function
{
    public StochSys()
    {
        fof = new LinkedHashMap<Variable,Set<Function>>();
        vof = new LinkedHashMap<Function,Set<Variable>>();
    }

    // Queries.

    public Set<Function> getFunctions()
    {
        return Collections.unmodifiableSet(vof.keySet());
    }

    public Set<Variable> getVariables()
    {
        return Collections.unmodifiableSet(fof.keySet());
    }

    public Set<Function> getFunctions(Variable v)
    {
        Set<Function> myfunc = fof.get(v);
        if (myfunc == null)
            return null;
        else
            return Collections.unmodifiableSet(myfunc);
    }

    public Set<Variable> getVariables(Function f)
    {
        Set<Variable> myvar = vof.get(f);
        if (myvar == null)
            return null;
        else
            return Collections.unmodifiableSet(myvar);
    }

    private double logValue(Set<Function> fs)
    {
        if (fs == null || fs.isEmpty())
            //return Double.NEGATIVE_INFINITY;
            return Double.NaN;

        double tot = 0;
        for (Function f : fs)
            {
                tot += f.logValue();
                if (Double.isInfinite(tot))
                    return tot;
            }
        return tot;
    }
                
    public double logValue()
    {
        return logValue(getFunctions());
    }

    public double getValue()
    {
        return Math.exp(logValue(getFunctions()));
    }

    public double logValue(Variable v)
    {
        return logValue(getFunctions(v));
    }

    public double getValue(Variable v)
    {
        return Math.exp(logValue(getFunctions(v)));
    }

    public Graph<Variable,Object> markovGraph()
    {
        Network<Variable,Object> g = new Network<Variable,Object>();
        for (Variable v : getVariables())
            g.add(v);

        for (Function f : getFunctions())
            for (Variable v : f.getVariables())
                for (Variable u : f.getVariables())
                    if (u != v)
                        g.connect(u,v);

        return g;
    }

    // Modifiers.

    public void add(Variable v)
    {
        if (fof.get(v) == null)
            fof.put(v,new LinkedHashSet<Function>());
    }

    public void add(Function f)
    {
        Set<Variable> myvars = vof.get(f);

        if (myvars == null)
            {
                myvars = new LinkedHashSet<Variable>();
                vof.put(f,myvars);
            }
                
        for (Variable v : f.getVariables())
            {
                myvars.add(v);

                Set<Function> myfunc = fof.get(v);
                if (myfunc == null)
                    {
                        myfunc = new LinkedHashSet<Function>();
                        fof.put(v,myfunc);
                    }
                myfunc.add(f);
            }
    }

    public void remove(Variable v)
    {
        Set<Function> myfunc = fof.get(v);
        if (myfunc == null)
            return;

        for (Function f : myfunc)
            vof.get(f).remove(v);

        fof.remove(v);
    }

    public void remove(Function f)
    {
        Set<Variable> myvar = vof.get(f);
        if (myvar == null)
            return;

        for (Variable v : myvar)
            fof.get(v).remove(f);

        vof.remove(f);
    }

    // Private data.

    private Map<Variable,Set<Function>> fof = null;
    private Map<Function,Set<Variable>> vof = null;
}
