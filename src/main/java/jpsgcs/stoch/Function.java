package jpsgcs.stoch;

import java.util.Collection;

public interface Function
{
    public double getValue();
    public double logValue();
    public Collection<Variable> getVariables(); 
}
