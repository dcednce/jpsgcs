package jpsgcs.stoch;

public interface Variable 
{
    public void setValue(double d);
    public double getValue();
}
