package jpsgcs.apps.zeroloop;

import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;
import java.util.Collection;

abstract public class EdgeConstraint extends Function
{
    public EdgeConstraint(Collection<? extends Variable> ed)
    {
        e = (Variable[]) ed.toArray(new Variable[0]);
    }

    public Variable[] getVariables()
    {
        return e;
    }

    protected Variable[] e = null;
}
