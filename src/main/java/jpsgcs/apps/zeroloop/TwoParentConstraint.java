package jpsgcs.apps.zeroloop;

import jpsgcs.markov.Variable;
import java.util.Set;
import java.util.Collection;

public class TwoParentConstraint extends EdgeConstraint
{
    public TwoParentConstraint(Collection<? extends Variable> ed)
    {
        super(ed);
    }
        
    public double getValue()
    {
        int tot = 0;
        for (int i=0; i<e.length; i++)
            tot += e[i].getState();
        return ( tot == 2 ? 1 : 0 );
    }
}
