package jpsgcs.apps.zeroloop;

import jpsgcs.markov.Variable;
import java.util.Set;
import java.util.Collection;

public class OneParentalMarriageConstraint extends EdgeConstraint
{
    public OneParentalMarriageConstraint(Collection<? extends Variable> ed)
    {
        super(ed);
    }
        
    public double getValue()
    {
        int tot = 0;
        for (int i=0; i<e.length; i++)
            tot += 1-e[i].getState();
        return ( tot == 0 || tot == 1 ? 1 : 0 );
    }
}
