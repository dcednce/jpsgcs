package jpsgcs.plink;

import jpsgcs.util.InputFormatter;
import jpsgcs.util.IntArray;
import jpsgcs.genio.ParameterData;
import jpsgcs.genio.PedigreeData;
import jpsgcs.genio.BasicGeneticData;
import jpsgcs.pedapps.GeneticMap;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.graph.Network;
import jpsgcs.linkage.NumberedAlleleLocus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.LinkedHashMap;
import java.io.IOException;
import java.io.PrintStream;

public class PlinkData implements ParameterData, PedigreeData, BasicGeneticData
{
	private PlinkLocus[] loc = null;
	private PlinkIndividual[] ped = null;
	private Network<String,PlinkIndividual> g = null;
	private int nprobs = 0;

	private boolean verbose = false;
	private Map<IntArray,double[][]> cache = null;

	// Default penetrance for locus 0 is dominant. (Rest are codominant 4 alleles.)
	private double[][][] penetrance = { {{1,0},{0,0}}, {{0,1},{1,1}} };

	public PlinkData(String parfile, String pedfile, String probfile, boolean errs) throws IOException
	{
		loc = readLocs(parfile);
		ped = readPeds(pedfile);
		if (probfile != null)
			readProbands(probfile);

		verbose = errs;
		cache = new LinkedHashMap<IntArray,double[][]>();

		//double[][] test = { {0.5,0.5,0.5} , {0.0005,0.5,0.5} };
		//setPenetrance(test);
	}

	public PlinkData(String parfile, String pedfile, String probfile) throws IOException
	{
		this(parfile,pedfile,probfile,false);
	}

	public PlinkData(String parfile, String pedfile) throws IOException
	{
		this(parfile,pedfile,null,false);
	}

	public void writeLinkageFiles(PrintStream parf, PrintStream pedf)
	{
		writeLinkageFiles(parf,pedf,false);
	}

	public void writeLinkageFiles(PrintStream parf, PrintStream pedf, boolean premake)
	{
		// Write the paramter file.

		parf.println(nLoci()+"\t 0 0 0");
		parf.println("0 0.0 0.0 0");
		for (int i=0; i<nLoci(); i++)
			parf.print((i+1)+" ");
		parf.println();
		parf.println("1 2 \t affection status");
		parf.println(loc[0].freqs[0]+" "+loc[0].freqs[1]);
		parf.println("2");
		parf.println(penetrance[0][0][0]+" "+penetrance[0][0][1]+" "+penetrance[0][1][1]);
		parf.println(penetrance[1][0][0]+" "+penetrance[1][0][1]+" "+penetrance[1][1][1]);
		
		for (int i=1; i<nLoci(); i++)
		{
			parf.println("3 "+loc[i].freqs.length+" "+loc[i].name);
			for (int j=0; j<loc[i].freqs.length; j++)
				parf.print(loc[i].freqs[j]+"\t");
			parf.println();
		}

		parf.println("0  0");
		for (int i=1; i<nLoci(); i++)
		{
			parf.print(getFemaleRecomFrac(i,i-1)+"  ");
		}
		parf.println();
		parf.println("1 5 0.2 0.1");

		// Write the pedigree file.

		for (int i=0; i<ped.length; i++)
			ped[i].writeAsLinkage(pedf,premake);
	}

	public void write(PrintStream mapf, PrintStream pedf, PrintStream probf)
	{
		for (int i=1; i<nLoci(); i++)
			mapf.println(loc[i].chrom+"\t"+loc[i].name+"\t"+loc[i].genetic+"\t"+loc[i].physical);

		for (int i=0; i<nIndividuals(); i++)
		{
			pedf.println(ped[i]);
			if (ped[i].proband == 1)
				probf.println(ped[i].ped+"\t"+ped[i].name);
		}
	}

// Implement ParameterData.

	public boolean sexLinked()
	{
		return false;
	}

	public int nLoci()
	{
		return loc.length;
	}

	public String locusName(int i)
	{
		return loc[i].name;
	}

	public int nAlleles(int i)
	{
		return loc[i].freqs.length;
	}

	public double[] alleleFreqs(int i)
	{
		return loc[i].freqs;
	}

	public double getFemaleRecomFrac(int i, int j)
	{
		double dist = loc[j].physical - loc[i].physical ;
		if (dist < 0)
			dist = -dist;
		return GeneticMap.cMToThetaKosambi(dist/1000000.0);
	}

	public double getMaleRecomFrac(int i, int j)
	{
		return getFemaleRecomFrac(i,j);
	}

// Implement PedigreeData.
    public double getKinshipCoeff(int i, int j) {
        throw new UnsupportedOperationException("Sorry, getKinshipCoeff() is not implemented for PlinkData.");
    }
    public int[] getRank() {
        throw new UnsupportedOperationException("Sorry, getRank() is not implemented for PlinkData.");
    }
	public String pedigreeName(int i)
	{
		return ped[i].ped;
	}

	public String individualName(int i)
	{
		return ped[i].name;
	}

	public boolean isMale(int i)
	{
		return ped[i].sex == 1;
	}

	public boolean isFemale(int i)
	{
		return ped[i].sex == 2;
	}

	public int nIndividuals()
	{
		return ped.length;
	}

	public int pa(int i)
	{
		PlinkIndividual pa = g.connection(ped[i].ped,ped[i].pa);
		return pa == null ? -1 : pa.index;
	}

	public int ma(int i)
	{
		PlinkIndividual ma = g.connection(ped[i].ped,ped[i].ma);
		return ma == null ? -1 : ma.index;
	}

	public int[] kids(int i)
	{
		Set<PlinkIndividual> k = new LinkedHashSet<PlinkIndividual>();

		PlinkIndividual x = ped[i];

		for (int j=0; j<nIndividuals(); j++)
		{
			PlinkIndividual y = ped[j];
			PlinkIndividual z = g.connection(y.ped,y.pa);
			if (z == x)
			{
				k.add(y);
			}
			else 
			{
				z = g.connection(y.ped,y.ma);
				if (z == x)
					k.add(y);
			}
		}

		int[] res = new int[k.size()];
		int j = 0;
		for (PlinkIndividual y : k)
			res[j++] = y.index;

		return res;
	}

	public int[][] nuclearFamilies()
	{
		Network<PlinkIndividual,Set<PlinkIndividual>> h = new Network<PlinkIndividual,Set<PlinkIndividual>>(true);
		Set<PlinkIndividual> pas = new LinkedHashSet<PlinkIndividual>();
		
		for (PlinkIndividual x : ped)
		{
			PlinkIndividual pa = g.connection(x.ped,x.pa);
			PlinkIndividual ma = g.connection(x.ped,x.ma);

			if (pa != null && ma != null)
			{
				Set<PlinkIndividual> kids = h.connection(pa,ma);
				if (kids == null)
				{
					kids = new LinkedHashSet<PlinkIndividual>();
					h.connect(pa,ma,kids);
				}
				kids.add(x);
				pas.add(pa);
			}
		}

		ArrayList<int[]> neucs = new ArrayList<int[]>();
		
		for (PlinkIndividual pa : pas)
		{
			for (PlinkIndividual ma : h.getNeighbours(pa))
			{
				Set<PlinkIndividual> kids = h.connection(pa,ma);
				int[] fam = new int[2+kids.size()];
				int j = 0;
				fam[j++] = pa.index;
				fam[j++] = ma.index;
				for (PlinkIndividual kid : kids)
					fam[j++] = kid.index;

				neucs.add(fam);
			}
		}

		return (int[][]) neucs.toArray(new int[0][0]);
	}

// Implement BasicGeneticData.
	
	public int proband(int i)
	{
		return ped[i].proband;
	}

	public int nProbands()
	{
		return nprobs;
	}

	public int getAllele(int i, int j, int k)
	{
		return ped[j].allele[i][k] - 1;
	}

	public boolean setAlleles(int i, int j, int a0, int a1)
	{
		if (i == 0)
			return false;

		ped[j].allele[i][0] = a0;
		ped[j].allele[i][1] = a1;
		return true;
	}

	public double[][] penetrance(int i, int j)
	{
		if (i == 0)
		{
			int affected = ped[j].aff - 1;
			if (affected < 0)
				return null;
			return penetrance[affected];
		}

		int a0 = getAllele(i,j,0);
		int a1 = getAllele(i,j,1);
		if (a0 < 0 && a1 < 0)
			return null;

		IntArray a = new IntArray(nAlleles(i),a0,a1);

		double[][] x = cache.get(a);
		
		if (x == null)
		{
			x = NumberedAlleleLocus.makePen(nAlleles(i),a0,a1);
			cache.put(a,x);
		}
	
		return x;
	}

	public void setPenetrance(double[][] p)
	{
		penetrance[0][0][0] = p[0][0];
		penetrance[0][1][0] = p[0][1];
		penetrance[0][0][1] = p[0][1];
		penetrance[0][1][1] = p[0][2];

		penetrance[1][0][0] = p[1][0];
		penetrance[1][1][0] = p[1][1];
		penetrance[1][0][1] = p[1][1];
		penetrance[1][1][1] = p[1][2];
	}

	public void downcodeAlleles()
	{
	}

// Private utilities.

	private void readProbands(String filename) throws IOException
	{
		nprobs = 0;

		InputFormatter f = new InputFormatter(filename);

		for (int line = 0; f.newLine(); line++)
		{
			String ped = f.nextString();
			String name = f.nextString();
			PlinkIndividual p = g.connection(ped,name);
			
			if (p == null && verbose)
			{
				System.err.println("Warning: File "+filename+" line "+line+". Proband "+ped+":"+name+" not found. Skipping line.");
			}
			else
			{
				nprobs++;
				p.proband = 1;
			}
		}
	}

	private PlinkLocus[] readLocs(String filename) throws IOException
	{
		ArrayList<PlinkLocus> a = new ArrayList<PlinkLocus>();
		int prev = 0;
		a.add(new PlinkLocus("?","Phenotype",0,prev,2));

		InputFormatter f = new InputFormatter(filename);
					
		for (int line = 0; f.newLine(); line++)
		{
			PlinkLocus p = new PlinkLocus(f.nextString(),f.nextString(),f.nextDouble(),f.nextInt());
			a.add(p);
			if (p.physical <= prev && verbose)
				System.err.println("Warning: File = "+filename+", line = "+line+". Locus positions are not in increasing order.");
			prev = p.physical;
		}

		return (PlinkLocus[])a.toArray(new PlinkLocus[0]);
	}

	private PlinkIndividual[] readPeds(String filename) throws IOException
	{
		g = new Network<String,PlinkIndividual>(true);

		InputFormatter f = new InputFormatter(filename);

		ArrayList<PlinkIndividual> a = new ArrayList<PlinkIndividual>();

		Set<String> peds = new LinkedHashSet<String>();

		for (int line = 0; f.newLine(); line++)
		{
			String ped = f.nextString();
			peds.add(ped);
			String name = f.nextString();
			String pa = f.nextString();
			String ma = f.nextString();
			int sex = f.nextInt();
			int aff = f.nextInt();

			if (g.connects(ped,name))
			{
				if (verbose)
					System.err.println("Warning: File "+filename+", line "+line+". Pedigree & id already used. Skipping this line.");
				continue;
			}

			PlinkIndividual p = new PlinkIndividual(ped,name,pa,ma,sex,aff,nLoci());
			g.connect(ped,name,p);
			
			p.allele[0][0] = p.allele[0][1] = aff;
			for (int i=1; i<nLoci(); i++)
			{
				p.allele[i][0] = f.nextInt();
				p.allele[i][1] = f.nextInt();
			}

			a.add(p);
		}

		for (String ped : peds)
		{
			for (String name : g.getNeighbours(ped))
			{
				PlinkIndividual ind = g.connection(ped,name);

				if (ind.pa != null)
				{
					PlinkIndividual pa = g.connection(ind.ped,ind.pa);
					if (pa == null)
					{
						pa = new PlinkIndividual(ind.ped,ind.pa,"0","0",1,0,nLoci());
						if (verbose)
							System.err.println("Warning: adding line for father of "+ind.ped+":"+ind.name+"\t:"+pa.ped+":"+pa.name);
						g.connect(ind.ped,ind.pa,pa);
					}
			
					if (pa.sex == 2 && verbose)
						System.err.println("Warning: father of "+ind.ped+":"+ind.name+" is female.");
			
				}

				if (ind.ma != null)
				{
					PlinkIndividual ma = g.connection(ind.ped,ind.ma);
					if (ma == null)
					{
						ma = new PlinkIndividual(ind.ped,ind.ma,"0","0",2,0,nLoci());
						if (verbose)
							System.err.println("Warning: adding line for mother of "+ind.ped+":"+ind.name+"\t:"+ma.ped+":"+ma.name);
						g.connect(ind.ped,ind.ma,ma);
					}
					if (ma.sex == 1 && verbose)
						System.err.println("Warning: mother of "+ind.ped+":"+ind.name+" is male.");
				}
			}
		}

		Set<PlinkIndividual> out = new LinkedHashSet<PlinkIndividual>();

		for (String ped : peds)
			for (String name : g.getNeighbours(ped))
			{
				// Reorder the individuals in cannonical pedigree order.
				append(out,g,ped,name);
				// Or don't bother.
				//out.add(g.connection(ped,name));
			}

		PlinkIndividual[] array = (PlinkIndividual[]) out.toArray(new PlinkIndividual[0]);
		for (int i=0; i<array.length; i++)
			array[i].index = i;

		return array;
	}

	private void append(Set<PlinkIndividual> out, Network<String,PlinkIndividual> g, String ped, String name)
	{
		PlinkIndividual p = g.connection(ped,name);
		if (p.pa != null && !out.contains(g.connection(ped,p.pa)))
			append(out,g,ped,p.pa);
		if (p.ma != null && !out.contains(g.connection(ped,p.ma)))
			append(out,g,ped,p.ma);
		if (!out.contains(p))
			out.add(p);
	}
}
