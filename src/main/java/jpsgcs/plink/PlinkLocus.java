package jpsgcs.plink;

import jpsgcs.util.InputFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.IOException;

public class PlinkLocus implements Comparable<PlinkLocus>
{
	protected String chrom = null;
	protected String name = null;
	protected double genetic = 0;
	protected int physical = 0;

	protected double[] freqs = null;

	public PlinkLocus(String c, String n, double p, int pos)
	{
		this(c,n,p,pos,4);
	}

	public PlinkLocus(String c, String n, double p, int pos, int nal)
	{
		chrom = c;
		name = n;
		genetic = p;
		physical = pos;

		freqs = new double[nal];
		for (int i=0; i<freqs.length; i++)
			freqs[i] = 1.0/freqs.length;
	}

	public int compareTo(PlinkLocus l)
	{
		return ( physical < l.physical ? -1 : ( physical > l.physical ? 1 : 0) );
	}

	public static PlinkLocus[] read(String filename) throws IOException
	{
		ArrayList<PlinkLocus> a = new ArrayList<PlinkLocus>();

		InputFormatter f = new InputFormatter(filename);
					
		while (f.newLine())
		{
			a.add(new PlinkLocus(f.nextString(),f.nextString(),f.nextDouble(),f.nextInt()));
		}

		return (PlinkLocus[])a.toArray(new PlinkLocus[0]);
	}

	public String toString()
	{
		return chrom+"\t"+name+"\t"+genetic+"\t"+physical;
	}

	public static void main(String[] args)
	{
		try
		{
			PlinkLocus[] l = PlinkLocus.read(args[0]);
			Arrays.sort(l);
			for (PlinkLocus p : l)
				System.out.println(p);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
