package jpsgcs.markov;

import java.util.Collection;

public class FillIn extends Function implements Trivial
{
	public FillIn(Variable[] vars)
	{
		v = vars;
	}

	public FillIn(Collection<Variable> vars)
	{
		this((Variable[]) vars.toArray(new Variable[0]));
	}

	public FillIn(Variable a, Variable b)
	{
		this(asArray(a,b));
	}

	public FillIn(Variable a)
	{
		this(asArray(a));
	}

	public double getValue()
	{
		return 1;
	}

	public Variable[] getVariables()
	{
		return v;
	}

// Private data and methods.

	private Variable[] v = null;

	private static Variable[] asArray(Variable a)
	{
		Variable[] x = {a};
		return x;
	}

	private static Variable[] asArray(Variable a, Variable b)
	{
		Variable[] x = {a, b};
		return x;
	}
}
