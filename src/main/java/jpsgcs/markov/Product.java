package jpsgcs.markov;

import java.util.Collection;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.LinkedHashMap;

public class Product
{
    public Product()
    {
        map = new LinkedHashMap<Object,Set<Function>>();
        all = new LinkedHashSet<Function>();
    }

    public void add(Function f)
    {
        all.add(f);
                
        for (Variable v : f.getVariables())
            connect(v,f);

        // This next line makes product different from MarkovRandomField.

        for (Parameter p : f.getParameters())
            connect(p,f);
    }

    public Set<Function> getFunctions()
    {
        return all;
    }

    public Set<Function> getFunctions(Object p)
    {
        return map.get(p);
    }

    public Set<Function> getFunctions(Collection<? extends Object> vs)
    {
        Set<Function> f = new LinkedHashSet<Function>();

        for (Object v : vs)
            if (getFunctions(v) != null)
                f.addAll(getFunctions(v));

        return f;
    }

    public double logValue()
    {
        double x = 0;

        for (Function f : all)
            {
                x += f.logValue();
                if (Double.isInfinite(x))
                    return x;
            }

        return x;
    }

    public double logValue(Collection<? extends Object> vars)
    {
        double x= 0;
                        
        for (Object v : vars)
            {
                Set<Function> s = map.get(v);
                        
                if (s == null)
                    continue;

                for (Function f : s)
                    f.unmark();
            }

        for (Object v : vars)
            {
                Set<Function> s = map.get(v);
                        
                if (s == null)
                    continue;

                for (Function f : s)
                    {
                        if (f.isMarked())
                            continue;

                        x += f.logValue();
                                
                        if (Double.isInfinite(x))
                            return x;

                        f.mark();
                    }
            }
                
        return x;
    }

    public double logValue(Object v)
    {
        double x = 0;
                
        Set<Function> s = map.get(v);

        if (s == null)
            return x;

        for (Function f : s)
            {
                x += f.logValue();
                if (Double.isInfinite(x))
                    return x;
            }
                
        return x;
    }

    // Private data and methods.

    private Map<Object,Set<Function>> map = null;
    private Set<Function> all = null;

    private void connect(Object v, Function f)
    {
        Set<Function> s = map.get(v);
        if (s == null)
            {
                s = new LinkedHashSet<Function>();
                map.put(v,s);
            }
        s.add(f);
    }
}
