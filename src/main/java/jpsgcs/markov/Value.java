package jpsgcs.markov;

public class Value
{
    public Value()
    {
        this(0);
    }

    public Value(double d)
    {
        setValue(d);
    }

    public double getValue()
    {
        return v;
    }

    public void setValue(double d)
    {
        v = d;
    }

    // Private data.
        
    private double v = 0;
}
