package jpsgcs.markov;

import jpsgcs.util.Monitor;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Random;

public class GraphicalModel
{
	public GraphicalModel(MarkovRandomField p, Random r)
	{
		this(p,r,false);
	}

	public GraphicalModel(MarkovRandomField p, Random r, boolean compit)
	{
		rand = r;

		ModelInfo[] m = p.compile(r);
		Map<Set<Variable>,Potential> h = new LinkedHashMap<Set<Variable>,Potential>();
		c = new Potential[m.length];

		for (int i=c.length-1; i>=0; i--)
		{
			Set<Variable> out = new LinkedHashSet<Variable>(m[i].clique);
			if (m[i].next != null)
				out.retainAll(m[i].next);
			else
				out.clear();

			c[i] = new Potential(m[i].clique,out,m[i].func,m[i].evid,h.get(m[i].next));
			c[i].next = h.get(m[i].next);
			h.put(m[i].clique,c[i]);
		}

		if (compit)
		{
			collect(false,false);
			distribute(false);

			for (int i=c.length-1; i>=0; i--)
			{
				c[i].pot = c[i].map;
				c[i].free();
			}
		}
	}

	public boolean collect()
	{
		return collect(true,false);
	}

	public boolean collect(boolean usepost, boolean max)
	{
		return collect(usepost,max,true);
	}

	public boolean collect(boolean usepost, boolean max, boolean verbose)
	{
		boolean nonz = true;

		for (int i=0; i<c.length; i++)
		{
			c[i].output().allocate();
			c[i].allocate();
			if (max)
				c[i].max(usepost);
			else
				c[i].collect(usepost);
			
			double total = c[i].output().sum();
		
			if (total > 0)
				c[i].output().scale(1.0/total);
			else
			{
				nonz = false;
			}
		}

		if (!nonz && verbose)
			System.err.println("Warning: graphical model state has zero probability");

		return nonz;
	}

	public void distribute()
	{
		distribute(false);
	}

	public void distribute(boolean marginalize)
	{
		for (int i=c.length-1; i>=0; i--)
		{
			c[i].distribute(marginalize);
			c[i].output().free();
		}
	}

	public void drop()
	{
		for (int i=c.length-1; i>=0; i--)
		{
			c[i].drop(rand.nextDouble());
		}
	}

	public Set<Function> cliquePotentials()
	{
		Set<Function> s = new LinkedHashSet<Function>();
		for (int i=0; i<c.length; i++)
			s.add(c[i]);
		return s;
	}

	public double logPeel()
	{
		return logPeel(true);
	}

	public double logPeel(boolean usepost)
	{
		double result = 0;

		for (int i=0; i<c.length; i++)
		{
			c[i].output().allocate();
			c[i].allocate();
			c[i].collect(usepost);
			c[i].free();
			for (Table t : c[i].inputs())
				t.free();

			double x = c[i].output().sum();
			if (x > 0)
			{
				c[i].output().scale(1.0/x);
				result += Math.log(x);
			}
			else
			{
				c[i].output().free();
				return Double.NEGATIVE_INFINITY;
			}
		}

		c[c.length-1].output().free();
		return result;
	}

	public double peel()
	{	
		return peel(true);
	}

	public double peel(boolean usepost)
	{
		return Math.exp(logPeel(usepost));
	}

	public void simulate()
	{
		simulate(true);
	}

	public void simulate(boolean usepost)
	{
		collect(usepost,false);
		drop();

		for (int i=c.length-1; i>=0; i--)
		{
			c[i].free();
			c[i].output().free();
		}
	}

	public void maximize()
	{
		maximize(true);
	}

	public void maximize(boolean usepost)
	{
		collect(usepost,true);
		drop();

		for (int i=c.length-1; i>=0; i--)
		{
			c[i].output().free();
			c[i].free();
		}
	}

	public Map<Variable,Function> variableMargins()
	{
		collect(true,false);
		distribute(true);

		Map<Variable,Function> margs = new LinkedHashMap<Variable,Function>();

		for (int i=c.length-1; i>=0; i--)
		{
			margs.putAll(c[i].allMargins());
			c[i].free();
		}

		return margs;
	}

	public Function getMargin(Variable a, Variable b)
	{
		collect(true,false);
		distribute(true);

		for (int i=0; i<c.length; i++)
		{
			if (c[i].invol().contains(a) && c[i].invol().contains(b))
				return c[i].getMarginal(a,b);
		}

		return null;
	}

	public void reduceStates()
	{
		collect(false,false);
		distribute(false);

		for (int i=0; i<c.length; i++)
		{
			Functions.reduceStates(c[i]);
			c[i].free();
		}
	}

// Private data.

	protected Potential[] c = null;
	private Random rand = null;
}
