package jpsgcs.jtree;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.LinkedHashSet;

public class ProperIntervalJTree<V> extends OrderedJTree<V>
{
	public ProperIntervalJTree(JTree<V> jt, V[] o, double max, Set<V> ok)
	{
		this(jt,o,max,ok,null);
	}

	public ProperIntervalJTree(JTree<V> jt, V[] o, double mx, Set<V> ok, WSMGraphLaw<V> cs)
	{
		super(jt,o,mx,ok,cs);
		uniformDecom = false;
	}

	public void randomize()
	{
	}

	protected Clique<V> arbitraryChoice(Clique<V> P, Clique<V> Cxy, Clique<V> Cx, Clique<V> Cy)
	{
		return topRank(P) < topRank(Cxy) ? Cx : Cy;
	}

	public UpdateInfo<V> proposeConnection()
	{
		UpdateInfo<V> up = new UpdateInfo<V>();

		if (jt.separators.isEmpty())
		{
			up.errcode = 1;
			return up;
		}

		up.S = jt.separators.next();

		up.Cx = up.S.getX();
		up.Cy = up.S.getY();
		if (topRank(up.Cx) > topRank(up.Cy))
		{
			Clique<V> temp = up.Cx;
			up.Cx = up.Cy;
			up.Cy = temp;
		}

		int r = topRank(up.Cx);
		while (up.S.contains(ranked(r)))
			r--;
		up.x = ranked(r);
		r = r+1;
		while (up.S.contains(ranked(r)))
			r++;
		up.y = ranked(r); 


		if (!(allowed.contains(up.x) && allowed.contains(up.y)))
		{
			up.errcode = 1;
			return up;
		}

		if (Math.abs(rank(up.x)-rank(up.y)) > max)
		{
			up.errcode = 1;
			return up;
		}

		up.Aij = jt.separators.size();
		up.type = -1;

		if (up.Cx.size() == up.S.size() + 1)
		{
			if (up.Cy.size() == up.S.size() + 1)
			{
				up.Aij /= (jt.cliques.size()-1);
				up.type = 0;
			}
			else
			{
				up.Aij /= jt.cliques.size();
				up.type = 2;
			}
		}
		else
		{
			if (up.Cy.size() == up.S.size() + 1)
			{
				up.Aij /= jt.cliques.size();
				up.type = 1;
			}
			else
			{
				up.Aij /= (jt.cliques.size()+1);
				up.type = 3;
			}
		}

		return up;
        }

	public UpdateInfo<V> proposeDisconnection()
	{
		UpdateInfo<V> up = new UpdateInfo<V>();

		if (jt.cliques.isEmpty())
		{
			up.errcode = 1;
			return up;
		}

		up.Cxy = jt.cliques.next();
		if (up.Cxy.size() < 2)
		{
			up.errcode = 2;
			return up;
		}

		up.x = bot(up.Cxy);
		up.y = top(up.Cxy);

		if (!(allowed.contains(up.x) && allowed.contains(up.y)))
		{
			up.errcode = 1;
			return up;
		}

		int N = 0;
		int Nx = 0;
		int Ny = 0;
		up.Cx = null;
		up.Cy = null;

		up.Cxy.remove(up.x);
		up.Cxy.remove(up.y);

		for (Clique<V> P : jt.getNeighbours(up.Cxy))
		{
			if (P.contains(up.x))
			{
				if (P.contains(up.y))
				{
					// Shouldn't get here.
					up.Cxy.add(up.x);
					up.Cxy.add(up.y);
					up.errcode = 3;
					return up;
				}
				else
				{
					Nx++;
					if (jt.connection(P,up.Cxy).containsAll(up.Cxy))
						up.Cx = P;
				}
			}
			else
			{
				if (P.contains(up.y))
				{
					Ny++;
					if (jt.connection(P,up.Cxy).containsAll(up.Cxy))
						up.Cy = P;
				}
				else
				{
					N++;
				}
			}
		}

		up.Cxy.add(up.x);
		up.Cxy.add(up.y);

		up.Aij = jt.cliques.size();
		up.type = -1;

		if (up.Cx == null)
		{
			if (up.Cy == null)
			{
				up.Aij /= jt.separators.size()+1;
				up.type = 0;
			}
			else
			{
				if (Ny > 1)
				{
					// Shouldn't get here.
					up.errcode = 4;
					return up;
				}
				up.Aij /= jt.separators.size();
				up.type = 2;
			}
		}
		else
		{
			if (up.Cy == null)
			{
				if (Nx > 1)
				{
					// Shouldn't get here.
					up.errcode = 5;
					return up;
				}
				up.Aij /= jt.separators.size();
				up.type = 1;
			}
			else
			{
				if (Nx > 1 || Ny > 1 || N > 0)
				{
					// Shouldn't get here.
					up.errcode = 6;
					return up;
				}
				up.Aij /= (jt.separators.size()-1);
				up.type = 3;
			}
		}

		return up;
	}
}
