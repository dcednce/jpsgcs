package jpsgcs.jtree;

import jpsgcs.util.IntValue;
import jpsgcs.graph.Graph;

import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Collection;

public class CardinalitySorter<V>
{
    public CardinalitySorter(Collection<V> x)
        {
            a = new ArrayList<LinkedHashSet<V>>();
            a.add(new LinkedHashSet<V>());
                
            h = new LinkedHashMap<V,IntValue>();
            for (V v : x)
                {
                    h.put(v,new IntValue());
                    a.get(0).add(v);
                }
        }

    public V next()
    {
        int i = a.size()-1;
        if (i < 0)
            return null;

        V  x = a.get(i).iterator().next();

        a.get(i).remove(x);
        for (int j=i; j>=0 && a.get(j).isEmpty(); j--)
            a.remove(j);

        h.remove(x);

        return x;
    }

    public void add(V v)
    {
        IntValue x = h.get(v);
        if (x == null)
            return;

        a.get(x.value()).remove(v);
        x.add(1);
        if (x.value() == a.size())
            a.add(new LinkedHashSet<V>());
        a.get(x.value()).add(v);
    }
                
    // Private data.

    private LinkedHashMap<V,IntValue> h = null;
    private ArrayList<LinkedHashSet<V>> a = null;
}
