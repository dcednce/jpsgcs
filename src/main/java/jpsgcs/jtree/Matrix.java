package jpsgcs.jtree;

public interface Matrix
{
	public int nRows();
	public int nColumns();
}
