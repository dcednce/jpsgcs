package jpsgcs.jtree;

import java.util.Set;

public class ColouredByrneDistribution<V> extends SMGraphLaw<V>
{
    public double logPotential(Set<V> a)
    {
        double b = 0;
        double w = 0;

        for (V v : a)
            {
                if ( ((Integer)v).intValue() % 2 == 0 )
                    b += 1;
                else
                    w += 1;
            }

        if (b+w > 6)
            return Double.NEGATIVE_INFINITY;
        else
            return -(b-w)*(b-w);
    }
}
