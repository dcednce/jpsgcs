package jpsgcs.jtree;

import jpsgcs.util.MatrixInverter;
import jpsgcs.util.MatrixInverseData;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.TreeSet;

public class GaussianScorer extends SMGraphLaw<Integer>
{
    public GaussianScorer(DoubleMatrix mat)
        {
            this(mat,2);
        }

    public GaussianScorer(DoubleMatrix mat, double deg)
        {
            setPenalty(Math.log((mat.nColumns()-1)/deg -1));

            data = mat;

            mu = new double[data.nColumns()];
            sigma = new double[data.nColumns()];

            for (int j=0; j<data.nColumns(); j++)
                {
                    double sum = 0;
                    double sumsq = 0;

                    for (int i=0; i<data.nRows(); i++)
                        {
                            double x = data.value(i,j);
                            sum += x;
                            sumsq += x*x;
                        }

                    mu[j] = sum / data.nRows();
                    sigma[j] = Math.sqrt( (sumsq - sum*sum/data.nRows()) / data.nRows() );
                    if (! (sigma[j] > 0) )
                        sigma[j] = 1;

                    for (int i=0; i<data.nRows(); i++)
                        data.setValue(i,j,(data.value(i,j)-mu[j])/sigma[j]);
                }

            cache = new LinkedHashMap<Set<Integer>,Double>();
        }

    public double logPotential(Set<Integer> b)
    {
        Set<Integer> a = new LinkedHashSet<Integer>(b);

        Double res = cache.get(a);
        if (res == null)
            {
                res = f(a);
                cache.put(a,res);
            }

        return res.doubleValue();
    }

    public void clear()
    {
        cache.clear();
    }

    public void setPenalty(double d)
    {
        penalty = d;
        if (cache != null)
            clear();
        System.err.println(penalty);
    }

    public double getPenalty()
    {
        return penalty;
    }

    // Private data.

    private double penalty = 0;
    private DoubleMatrix data = null;
    private double[] mu = null;
    private double[] sigma = null;

    private Map<Set<Integer>,Double> cache = null;

    private double f(Set<Integer> a)
    {
        int[] q = new int[a.size()];
        int ii = 0;
        for (Integer x : a)
            q[ii++] = x.intValue();

        double[][] s = new double[a.size()][a.size()];

        for (int j=0; j<q.length; j++)
            {
                for (int k = 0; k<q.length; k++)
                    {
                        if (k < j)
                            {
                                s[j][k] = s[k][j];
                            }
                        else
                            {
                                for (int i=0; i<data.nRows(); i++)
                                    s[j][k] += data.value(i,q[j]) * data.value(i,q[k]);
                                s[j][k] /= data.nRows();
                            }
                    }
            }

        MatrixInverseData mid = MatrixInverter.invert(s);
        s = mid.inv();

        double tot = 0;

        for (int j=0; j<q.length; j++)
            for (int k=j+1; k<q.length; k++)
                for (int i=0; i<data.nRows(); i++)
                    tot += data.value(i,q[j]) * s[j][k] * data.value(i,q[k]);

        tot *= 2;

        for (int j=0; j<q.length; j++)
            for (int i=0; i<data.nRows(); i++)
                tot += data.value(i,q[j]) * s[j][j] * data.value(i,q[j]);

        double n = data.nRows();
        double d = a.size();
        double det = mid.det();

        tot = -0.5 * tot - 0.5 * n * Math.log(det) - 0.5 * n * d * Math.log(2*Math.PI);

        double pri = - penalty * d * (d+1) * 0.5;

        return tot + pri;
    }
}
