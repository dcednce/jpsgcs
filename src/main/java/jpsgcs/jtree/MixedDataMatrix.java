package jpsgcs.jtree;

import jpsgcs.markov.Variable;
import jpsgcs.util.InputFormatter;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map;
import java.util.LinkedHashMap;

import java.util.TreeSet;

public class MixedDataMatrix 
{
	public MixedDataMatrix() throws IOException
	{
		this(new InputFormatter(),false);
	}

	public MixedDataMatrix(boolean head) throws IOException
	{
		this(new InputFormatter(),head);
	}

	public MixedDataMatrix(InputFormatter f, boolean header) throws IOException
	{
		if (header)
		{
			Vector<String> v = new Vector<String>();
			f.newLine();
			while (f.newToken())
				v.add(f.getString());
			names = (String[]) v.toArray(new String[0]);
		}

		discrete = null;
		Vector<double[]> v = new Vector<double[]>();

		while (f.newLine())
		{
			if (discrete == null)
			{
				String[] s =  f.thisLine().split("[ \t][ \t]*");
				discrete = new boolean[s.length];
				for (int i=0; i<s.length; i++)
				{
					try
					{
						Integer.parseInt(s[i]);
						discrete[i] = true;
					}
					catch (NumberFormatException e)
					{
						discrete[i] = false;
					}
				}

			}

			double[] x = new double[discrete.length];

			for (int i=0; i<discrete.length; i++)
			{
				x[i] = ( discrete[i] ? f.nextInt() : f.nextDouble() );
			}

			v.add(x);
		}

		dat = (double[][]) v.toArray(new double[0][]);
	}

	public int nCols()
	{
		return discrete.length;
	}

	public int nRows()
	{
		return dat.length;
	}

	public int asInt(int i, int j)
	{
		return (int) (dat[i][j]+0.5);
	}

	public double asDouble(int i, int j)
	{
		return dat[i][j];
	}

	public String getName(int i)
	{
		if (names == null)
			return null;
		else
			return names[i];
	}

	public String toString()
	{
		StringBuffer b = new StringBuffer();
		
		for (int i=0; i<dat.length; i++)
		{
			for (int j=0; j<dat[i].length; j++)
			{
				// Don't shorten this to 
				// b.append (discrete[i] ? asInt(i,j) : asDouble(i,j))
				// since the int gets changed to a double.
				if (discrete[j])
					b.append(asInt(i,j));
				else
					b.append(asDouble(i,j));
				if (j < dat[i].length-1)
					b.append("\t");
			}
			if (i < dat.length-1)
				b.append("\n");
		}
	
		return b.toString();
	}

	public boolean isDiscrete(int i)
	{
		return discrete[i];
	}

	public double correlation(int x, int y)
	{
		double n = 0;
		double mx = 0;
		double my = 0;
		double sxx = 0;
		double sxy = 0;
		double syy = 0;

		for (int i=0; i<nRows(); i++)
		{
			n += 1;
			mx += dat[i][x];
			my += dat[i][y];
			sxy += dat[i][x]*dat[i][y];
			sxx += dat[i][x]*dat[i][x];
			syy += dat[i][y]*dat[i][y];
		}

		mx /= n;
		my /= n;

		sxx = sxx/n - mx*mx;
		syy = syy/n - my*my;
		sxy = sxy/n - mx*my;
		
		return sxx > 0 && syy > 0 ? sxy / Math.sqrt(sxx * syy) : 0;
	}

	public int[] statesOfColumn(int j)
	{
		if (!discrete[j])
			return null;

		Set<Integer> val = new TreeSet<Integer>();
		for (int i=0; i<dat.length; i++)
			val.add(asInt(i,j));

		int[] iii = new int[val.size()];
		int n = 0;
		for (Integer i : val)
			iii[n++] = i.intValue();

		return iii;
	}


// Private data and methods.

	protected double[][] dat = null;
	protected boolean[] discrete = null;
	protected String[] names = null;
}
