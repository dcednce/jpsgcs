package jpsgcs.jtree;

import java.util.Set;

public class BinaryVariablePrior<V> extends SMGraphLaw<V>
{
    public BinaryVariablePrior(double a)
        {
            alpha = a;
        }

    public double logPotential(Set<V> c)
    {
        return -alpha * (Math.pow(2,c.size()) - 1);
    }

    private double alpha = 1;
}
