
package jpsgcs.functions;

import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;

/*
	The variable has the distribution specified by the given probabilities.
*/

public class Univariate extends SkeletonFunction
{
	double[] p = null;

	public Univariate(Variable x, double[] prob)
	{
		super(x);
		p = prob;
	}

	public double getValue()
	{
		return v[0].getState() < p.length ? p[v[0].getState()] : 0;
	}
}
