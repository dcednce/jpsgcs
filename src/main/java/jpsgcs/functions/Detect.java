package jpsgcs.functions;

import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;

import java.util.Set;

/*
	The first variable is the result of a test to detect
	whether the second variable is 0 or not with the given
	test sensitivity and specificity parameters.
*/

public class Detect extends SkeletonFunction
{
	private double sens = 1.0;
	private double spec = 1.0;

	public Detect(Variable x, Variable y, double sensitivity, double specificity)
	{
		super(x,y);
		sens = sensitivity;
		spec = specificity;
	}

	public double getValue()
	{
		if (v[1].getState() > 0)
		{
			return v[0].getState() > 0 ? sens : 1-sens;
		}
		else
		{
			return v[0].getState() > 0 ? 1-spec : spec;
		}
	}
}
