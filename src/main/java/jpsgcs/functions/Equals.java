package jpsgcs.functions;

import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;

/*
	The first variable is equal to the second.
*/

public class Equals extends SkeletonFunction
{
	public Equals(Variable x, Variable y)
	{
		super(x,y);
	}

	public double getValue()
	{
		return v[0].getState() == v[1].getState() ? 1 : 0;
	}
}
