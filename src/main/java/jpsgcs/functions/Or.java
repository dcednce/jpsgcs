package jpsgcs.functions;

import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;

import java.util.Set;

/*
	The first variable is equal to the OR of the others.
*/

public class Or extends SkeletonFunction
{
	public Or(Variable x, Variable y, Variable z)
	{
		super(x,y,z);
	}

	public Or(Variable x, Variable y)
	{
		super(x,y);
	}

	public Or(Variable x, Set<Variable> y)
	{
		super(x,y);
	}

	public double getValue()
	{
		int x = 0;

		for (int i=1; x == 0 && i<v.length; i++)
			if (v[i].getState() > 0)
				x = 1;

		return v[0].getState() == x ? 1 : 0;
	}
}
