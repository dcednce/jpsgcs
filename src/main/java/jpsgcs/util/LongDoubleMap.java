package jpsgcs.util;

public class LongDoubleMap
{
    public LongDoubleMap(int capacity)
    {
        x = new LongDoubleItem[capacity];
        head = null;
        used = 0;
    }

    public LongDoubleItem find(long i)
    {
        int h = hash(i);
                
        for (LongDoubleItem y = x[h]; y != null && y.h == h; y = y.next)
            if (y.i == i)
                return y;

        return null;
    }

    public LongDoubleItem force(long i)
    {
        int h = hash(i);

        LongDoubleItem z = null;

        for (LongDoubleItem y = x[h]; y != null && y.h == h; y = y.next)
            if (y.i == i)
                {
                    z = y;
                    break;
                }

        if (z == null)
            {
                z = new LongDoubleItem(i,0,h);
                insert(z,x[h]);
                x[h] = z;
            }
        
        return z;
    }

    public LongDoubleItem head()
    {
        return head;
    }

    public void put(long i, double d)
    {
        LongDoubleItem y = force(i);
        y.d = d;
    }

    public double get(long i)
    {
        LongDoubleItem y = find(i);
        return y == null ? 0 : y.d;
    }

    public void resize(int capacity)
    {
        LongDoubleItem y = head;

        x = new LongDoubleItem[capacity];
        head = null;
        used = 0;
                
        while (y != null)
            {
                LongDoubleItem z = y;
                y = y.next;
                z.next = z.prev = null;
                z.h = hash(z.i);
                insert(z,x[z.h]);
                x[z.h] = z;
            }
    }

    public void clean()
    {
        for (LongDoubleItem y = head; y != null; )
            {
                LongDoubleItem z = y;
                y = y.next;
                if (!(z.d > 0))
                    remove(z);
            }
    }

    public void clear()
    {
        x = new LongDoubleItem[x.length];
        head = null;
        used = 0;
    }
                
    public int size()
    {
        return used;
    }

    // Private data and methods.

    private LongDoubleItem[] x = null;
    private LongDoubleItem head = null;
    private int used = 0;

    private int hash(long i)
    {
        return (int)(i % x.length);
    }
        
    public void remove(LongDoubleItem z)
    {
        int h = z.h;

        if (x[h] == z)
            {
                x[h] = z.next;
                if (x[h] != null && x[h].h != h)
                    x[h] = null;
            }
                
        if (head == z)
            head = z.next;

        if (z.next != null)
            z.next.prev = z.prev;
        if (z.prev != null)
            z.prev.next = z.next;
                
        z.next = z.prev = null;

        used--;
    }

    private void insert(LongDoubleItem z, LongDoubleItem y)
    {
        if (y == null)
            {
                z.next = head;
                if (z.next != null)
                    z.next.prev = z;
                head = z;
            }
        else
            {
                z.next = y;
                z.prev = y.prev;
                if (head == y)
                    head = z;

                if (z.next != null)
                    z.next.prev = z;
                if (z.prev != null)
                    z.prev.next = z;
            }
                
        used++;
    }
}
