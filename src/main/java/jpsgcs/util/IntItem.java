package jpsgcs.util;

public class IntItem<V>
{
    public IntItem(int a, V x, int g)
        {
            i = a;
            d = x;
            h = g;
        }
        
    public IntItem()
        {
        }

    public int i = 0;
    public V d = null;
    public int h = 0;
    public IntItem<V> next = null;
    public IntItem<V> prev = null;
}
