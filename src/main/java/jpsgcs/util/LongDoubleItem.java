package jpsgcs.util;

public class LongDoubleItem
{
    public LongDoubleItem(long a, double x, int g)
    {
        i = a;
        d = x;
        h = g;
    }
        
    public LongDoubleItem()
    {
    }

    public long i = 0;
    public double d = 0;
    public int h = 0;
    public LongDoubleItem next = null;
    public LongDoubleItem prev = null;
}
