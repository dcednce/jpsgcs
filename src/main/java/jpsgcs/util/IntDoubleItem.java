package jpsgcs.util;

public class IntDoubleItem
{
	public IntDoubleItem(int a, double x, int g)
	{
		i = a;
		d = x;
		h = g;
	}
	
	public IntDoubleItem()
	{
	}

	public int i = 0;
	public double d = 0;
	public int h = 0;
	public IntDoubleItem next = null;
	public IntDoubleItem prev = null;
}
