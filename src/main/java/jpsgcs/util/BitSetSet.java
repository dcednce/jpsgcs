package jpsgcs.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.BitSet;

public class BitSetSet<V> implements Set<V>, RandomBag<V>
{
    private static Object[] object = null;
    private static Map<Object,Integer> index = null;

    public static void setUniverse(Set<?> s)
    {
        if (object != null)
            throw new RuntimeException("Cannot redefine universe of BitSetSet");

        object = new Object[s.size()];
        index = new LinkedHashMap<Object,Integer>();

        int i = 0;
        for (Object v : s)
            {
                object[i] = v;
                index.put(v,i);
                i++;
            }
    }

    public BitSetSet()
        {
            if (object == null)
                throw new RuntimeException("Cannot declare BitSetSet before universe is set");

            bits = new BitSet(object.length);
        }

    public BitSetSet(Collection<V> c)
        {
            this();
            addAll(c);
        }

    public Iterator<V> iterator()
    {
        return new BitSetSetIterator();
    }
        
    private class BitSetSetIterator implements Iterator<V>
    {
        private int i = 0;
        private int j = 0;

        public BitSetSetIterator()
            {
                j = -1;
                i = bits.nextSetBit(0);
            }

        public boolean hasNext()
        {
            return i >= 0;  
        }
                
        public V next()
        {
            j = i;
            i = bits.nextSetBit(i+1);
            return (V) object[j];
        }

        public void remove()
        {
            bits.clear(j);
        }
    }

    public boolean equals(Object o)
    {
        if (o instanceof BitSetSet)
            return bits.equals(((BitSetSet)o).bits);

        if (o instanceof Set)
            return o.equals(this);

        return false;
    }

    public int hashCode()
    {
        return bits.hashCode();
    }

    public V next()
    {
        double x = Math.random() * bits.cardinality();
        for (int i = bits.nextSetBit(0), j = 0; i >= 0; i = bits.nextSetBit(i+1))
            if (x <= ++j)
                return (V) object[i];
        return null;
    }

    public V draw()
    {
        V x = next();
        remove(x);
        return x;
    }

    public boolean add(V x)
    {
        int i = index(x);
        boolean b = bits.get(i);
        bits.set(i);
        return b != bits.get(i);
    }

    public boolean remove(Object x)
    {
        int i = index(x);
        boolean b = bits.get(i);
        bits.clear(i);
        return b != bits.get(i);
    }

    public boolean contains(Object x)
    {
        return bits.get(index(x));
    }

    public boolean containsAll(Collection<?> c)
    {
        if (c instanceof BitSetSet)
            {
                BitSet x = ((BitSetSet)c).bits;
                for (int i = x.nextSetBit(0); i >= 0; i = x.nextSetBit(i+1))
                    if (!bits.get(i))
                        return false;
                return true;
            }
        else
            {
                for (Object x : c)
                    if (!contains(x))
                        return false;
                return true;
            }
    }

    public boolean addAll(Collection<? extends V> c)
    {
        int s = bits.cardinality();

        if (c instanceof BitSetSet)
            {
                this.bits.or(((BitSetSet)c).bits);
            }
        else
            {
                for (V v : c)
                    bits.set(index(v));
            }

        return s != bits.cardinality();
    }

    public boolean removeAll(Collection<?> c)
    {
        int s = bits.cardinality();

        if (c instanceof BitSetSet)
            {
                this.bits.andNot(((BitSetSet)c).bits);
            }
        else
            {
                for (Object v : c)
                    bits.clear(index(v));
            }

        return s != bits.cardinality();
    }

    public boolean retainAll(Collection<?> c)
    {
        int s= bits.cardinality();

        if (c instanceof BitSetSet)
            {
                this.bits.and(((BitSetSet)c).bits);
            }
        else
            {
                for (int i = bits.nextSetBit(0); i >= 0; i = bits.nextSetBit(i+1))
                    if (!c.contains(object[i]))
                        bits.clear(i);
            }

        return s != bits.cardinality();
    }

    public int size()
    {
        return bits.cardinality();
    }
        
    public boolean isEmpty()
    {
        return bits.isEmpty();
    }

    public void clear()
    {
        bits.clear();
    }


    public <T> T[] toArray(T[] t)
    {
        return (T[]) toArray();
    }

    public Object[] toArray()
    {
        Object[] x = new Object[bits.cardinality()];
                
        for (int i= bits.nextSetBit(0), j=0; i>=0; i = bits.nextSetBit(i+1))
            x[j++] = object[i];

        return x;
    }

    public String toString()
    {
        StringBuffer b = new StringBuffer();
        b.append("[");
        for (int i=bits.nextSetBit(0); i>=0; i = bits.nextSetBit(i+1))
            b.append(object[i]+", ");

        if (bits.cardinality() > 0)
            {
                b.deleteCharAt(b.length()-1);
                b.deleteCharAt(b.length()-1);
            }
        b.append("]");

        return b.toString();
    }


    // Private data and methods.

    private BitSet bits = null;

    private static int index(Object v)
    {
        return index.get(v).intValue();
    }
}
