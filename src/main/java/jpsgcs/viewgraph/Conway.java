package jpsgcs.viewgraph;

import jpsgcs.viewgraph.*;
import jpsgcs.graph.*;
import jpsgcs.animate.*;
import java.awt.*;
import java.util.*;
import java.io.*;

public class Conway
{
    public static void main(String[] args)
    {
        try
            {
                //LifeBlob[][] blob = new LifeBlob[41][3];
                LifeBlob[][] blob = new LifeBlob[50][50];

                for (int i=0; i<blob.length; i++)
                    for (int j=0; j<blob[i].length; j++)
                        blob[i][j] = new LifeBlob();

                        
                Network<LifeBlob,Object> g = new Network<LifeBlob,Object>();
                Map<LifeBlob,VertexRepresentation> map = new LinkedHashMap<LifeBlob,VertexRepresentation>();
                for (int i=0; i<blob.length; i++)
                    for (int j=0; j<blob[i].length; j++)
                        {
                            map.put(blob[i][j],blob[i][j]);
                            g.add(blob[i][j]);

                            if (i > 0)
                                {
                                    g.connect(blob[i][j],blob[i-1][j]);
                                    if (j > 0)
                                        g.connect(blob[i][j],blob[i-1][j-1]);
                                    if (j < blob[i].length -1)
                                        g.connect(blob[i][j],blob[i-1][j+1]);
                                }

                            if (i < blob.length-1)
                                {
                                    g.connect(blob[i][j],blob[i+1][j]);
                                    if (j > 0)
                                        g.connect(blob[i][j],blob[i+1][j-1]);
                                    if (j < blob[i].length -1)
                                        g.connect(blob[i][j],blob[i+1][j+1]);
                                }

                            if (j > 0)
                                g.connect(blob[i][j],blob[i][j-1]);
                            if (j < blob[i].length -1)
                                g.connect(blob[i][j],blob[i][j+1]);

                            blob[i][j].setSize(3,3);
                        }

                for (int i=0; i<blob.length; i++)
                    for (int j=0; j<blob[i].length; j++)
                        {
                            blob[i][j].state = ( Math.random() < 0.5 ? 0 : 1 );
                        }

                /*
                  int ii = 0;
                  int jj = 1;
                  for (int i=1; i<9; i++)
                  blob[ii+i][jj].state = 1;
                  for (int i=10; i<15; i++)
                  blob[ii+i][jj].state = 1;
                  for (int i=18; i<21; i++)
                  blob[ii+i][jj].state = 1;
                  for (int i=27; i<34; i++)
                  blob[ii+i][jj].state = 1;
                  for (int i=35; i<40; i++)
                  blob[ii+i][jj].state = 1;
                */
                        

                PaintableGraph<LifeBlob,Object> pg = new PaintableGraph<LifeBlob,Object>(g,map);
                for (int i=0; i<blob.length; i++)
                    for (int j=0; j<blob[i].length; j++)
                        {
                            //jpsgcs.graph.Point x = pg.getPoint(blob[i][j]);
                            Coord x = pg.getCoord(blob[i][j]);
                            x.x0 = i+1 - (blob.length+1)/2.0;
                            x.y0 = j+1 - (blob[i].length+1)/2.0;
                        }

                GraphLocator<LifeBlob,Object> loc = new HomeBaseLocator<LifeBlob,Object>();

                Panel p = new GraphPanel<LifeBlob,Object>(pg,loc);

                Frame f = new Frame();
                f.addWindowListener(new FrameQuitter());
                f.add(p);
                f.pack();
                f.setVisible(true);

                Thread.sleep(20000);

                while (true)
                    {
                        Thread.sleep(100);
                        for (int i=0; i<blob.length; i++)
                            for (int j=0; j<blob[i].length; j++)
                                {
                                    int k = 0;

                                    for (LifeBlob n : g.getNeighbours(blob[i][j]))
                                        k += n.state;

                                    if (blob[i][j].state == 1)
                                        blob[i][j].newstate = ( k<2 || k>3 ? 0 : 1 );
                                    if (blob[i][j].state == 0)
                                        blob[i][j].newstate = ( k == 3 ? 1 : 0 );
                                }
                                
                        for (int i=0; i<blob.length; i++)
                            for (int j=0; j<blob[i].length; j++)
                                {
                                    blob[i][j].state = blob[i][j].newstate;
                                }
                                
                        /*
                          for (int i=0; i<blob.length; i++)
                          for (int j=0; j<blob[i].length; j++)
                          if (Math.random() < 0.001)
                          blob[i][j].state = 1;
                        */
                    }
            }
        catch (Exception e)
            {
                System.err.println("Caught in ViewGraph.main()");
                e.printStackTrace();
                System.exit(1);
            }
    }
}

class LifeBlob extends Blob
{
    public int state = 0;
    public int newstate = 0;

    public void paint(Graphics g, double dx, double dy, boolean b)
    {
        setColor( state == 0 ? Color.white : Color.cyan);
        super.paint(g,dx,dy,b);
    }
}
