package jpsgcs.viewgraph;

import java.awt.Color;
import java.awt.Graphics;

public class Blob implements VertexRepresentation
{
	public boolean contains(double a, double b)
	{
		if (a < -h || a > h)
			return false;
		if (b < -h || b > h)
			return false;
		return true;
	}
	
	public void paint(Graphics g, double dx, double dy, boolean b)
	{
		int x = (int)(dx-w);
		int y = (int)(dy-h);
		g.setColor( col );
/*
		g.fillOval( x-2, y-2, 2*w+4, 2*h+4 );
		g.setColor(b? border0: border1);
		g.drawOval( x-2, y-2, 2*w+4, 2*h+4 );
*/
		if (shape == 1)
		{
			if (!transp)
				g.fillRect( x, y, 2*w, 2*h );
			g.setColor(b? border0: border1);
			g.drawRect( x, y, 2*w, 2*h );
		}
		else
		{
			if (!transp)
				g.fillOval( x, y, 2*w, 2*h );
			g.setColor(b? border0: border1);
			g.drawOval( x, y, 2*w, 2*h );
		}
	}

	public void setColor(Color c)
	{
		col = c;
	}

	public void setBorderColors(Color c, Color d)
	{
		border0 = c;
		border1 = d;
	}

	public void setSize(int i, int j)
	{
		w = i;
		h = j;
	}

	public void setShape(int i)
	{
		shape = i;
	}

	public void setTransparent(boolean b)
	{
		transp = b;
	}

// Private data.

	private Color col = Color.yellow;
	private Color border0 = Color.black;
	private Color border1 = Color.red;
	protected int w = 8;
	protected int h = 8;
	private int shape = 0;
	private boolean transp = false;
}
