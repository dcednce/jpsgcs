package jpsgcs.genio;

public interface ParameterData
{
	public String locusName(int i);
	
/**
 The number of genetic loci for which data is available.
 The loci will be accessed by index from 0 to nLoci()-1
 and are assumed to be in corresponding physical order.
*/
	public int nLoci();

/**
 The number of alleles at the ith genetic locus.
*/
	public int nAlleles(int i);

/**
 The frequencies of the alleles at the ith genetic locus.
*/
	public double[] alleleFreqs(int i);

/**
 Returns the probability of a recombination between loci i and j in
 a single meiosis in females.
*/
	public double getFemaleRecomFrac(int i, int j);

/**
 Returns the probability of a recombination between loci i and j in
 a single meiosis in males.
*/
	public double getMaleRecomFrac(int i, int j);

/**
 Returns 0 or 1 if the regions is not or is, respectively, sex linked.
*/
	public boolean sexLinked();
}
