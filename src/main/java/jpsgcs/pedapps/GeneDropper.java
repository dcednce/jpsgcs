package jpsgcs.pedapps;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedmcmc.Recombination;
import jpsgcs.markov.Variable;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.MarkovRandomField;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Random;

public class GeneDropper
{
	protected GeneDropper()
	{
	}

	public GeneDropper(GeneticDataSource d, boolean linkfirst, Random r)
	{
		this(d,new LDModel(d),linkfirst,r);
	}

	@Deprecated
	public GeneDropper(GeneticDataSource d, LDModel ld, boolean linkfirst, Random r)
	{
		this(d,ld,r);

		System.err.println("Warning: Constructor GeneDropper(GeneticDataSource d, LDModel ld, boolean linkfirst)");
		System.err.println("will be discontinued. In future use");
		System.err.println(" GeneDropper(GeneticDataSource d, LDModel ld) instead.");
	}

	public GeneDropper(GeneticDataSource d, Random r)
	{
		this(d,new LDModel(d),r);
	}
		
	public GeneDropper(GeneticDataSource dsource, LDModel ld, Random rand)
	{
           	data = dsource;

		all = new Variable[data.nLoci()];
		for (int i=0; i<data.nLoci(); i++)
			all[i] = new Variable(data.nAlleles(i));

		Map<Variable,Variable> map = new LinkedHashMap<Variable,Variable>();
		Variable[] ldv = ld.getLocusVariables();
		for (int i=0; i<all.length; i++)
			map.put(ldv[i],all[i]);

		gall = new GraphicalModel(ld.replicate(map),rand,true);
		
		pat = new Variable[data.nLoci()];
		for (int i=0; i<pat.length; i++)
			pat[i] = new Variable(2);

		MarkovRandomField p = new MarkovRandomField();
		for (int i=1; i<data.nLoci(); i++)
			p.add(new Recombination(pat[i],pat[i-1], data.getMaleRecomFrac(i,i-1)));

		gpat = new GraphicalModel(p,rand,true);
		

		mat = new Variable[data.nLoci()];
		for (int i=0; i<mat.length; i++)
			mat[i] = new Variable(2);

		p = new MarkovRandomField();
		for (int i=1; i<data.nLoci(); i++)
			p.add(new Recombination(mat[i],mat[i-1], data.getFemaleRecomFrac(i,i-1)));

		gmat = new GraphicalModel(p,rand,true);

		gall.collect();
		gpat.collect();
		gmat.collect();

		alleles = new int[data.nLoci()][data.nIndividuals()][2];

		pa = new int[data.nIndividuals()];
		ma = new int[data.nIndividuals()];
		
		for (int i=0; i<pa.length; i++)
		{
			pa[i] = data.pa(i);
			ma[i] = data.ma(i);
		}

		ord = Pedigrees.canonicalOrder(data);
	}

	public void geneDrop()
	{
		geneDrop(false);
	}

	public void geneDrop(boolean allout)
	{
		sampleAlleles();

		for (int i=0; i<data.nLoci(); i++)
		{
			for (int j=0; j<data.nIndividuals(); j++)
			{
				int a0 = data.getAllele(i,j,0);
				if (allout || a0 >= 0)
					a0 = alleles[i][j][0];

				int a1 = data.getAllele(i,j,1);
				if (allout || a1 >= 0)
					a1 = alleles[i][j][1];

				data.setAlleles(i,j,a0,a1);
			}
		}
	}

// Private data.

	protected GeneticDataSource data = null;

	protected int[][][] alleles = null;
	protected int[] pa = null;
	protected int[] ma = null;
	protected int[] ord = null;
	
	protected Variable[] all = null;
	protected Variable[] pat = null;
	protected Variable[] mat = null;
	protected GraphicalModel gall = null;
	protected GraphicalModel gpat = null;
	protected GraphicalModel gmat = null;

	protected void sampleAlleles()
	{
		for (int jj=0; jj<ord.length; jj++)
		{
			int j = ord[jj];

			if (pa[j] < 0)
			{
				gall.drop();
				for (int i=0; i<alleles.length; i++)
					alleles[i][j][0] = all[i].getState();
			}
			else
			{
				gpat.drop();
				for (int i=0; i<alleles.length; i++)
					alleles[i][j][0] = alleles[i][pa[j]][pat[i].getState()];

			}

			if (ma[j] < 0)
			{
				gall.drop();
				for (int i=0; i<alleles.length; i++)
					alleles[i][j][1] = all[i].getState();
			}
			else
			{
				gmat.drop();
				for (int i=0; i<alleles.length; i++)
					alleles[i][j][1] = alleles[i][ma[j]][mat[i].getState()];
			}
		}
	}
}
