package jpsgcs.pedapps;

import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.Function;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.pedmcmc.Inheritance;
import jpsgcs.pedmcmc.Recombination;
import jpsgcs.pedmcmc.LocusProduct;
import java.util.Set;
import java.util.LinkedHashSet;

public class TwoLocusLodScores
{
	public TwoLocusLodScores(LocusProduct a, LocusProduct b, boolean sexed)
	{
		sexlinked = sexed;

		MarkovRandomField prod = new MarkovRandomField(null);

		for (Function f : a.getFunctions())
			prod.add(f);
		for (Function f : b.getFunctions())
			prod.add(f);
		
		Inheritance[][] ha = a.getInheritances();
		Inheritance[][] hb = b.getInheritances();

		if (ha.length != hb.length)
			throw new RuntimeException("Locus missmatch in TwoLocusPoduct constructor");

		mrec = new LinkedHashSet<Recombination>();
		frec = new LinkedHashSet<Recombination>();

		for (int j=0; j<ha.length; j++)
		{
			if ((ha[j][0] == null && hb[j][0] != null) || (ha[j][0] != null && hb[j][0] == null)) 
				throw new RuntimeException("Locus missmatch in TwoLocusPoduct constructor");

			if (ha[j][0] != null && hb[j][0] != null)
			{
				Recombination r = new Recombination(ha[j][0],hb[j][0], ( sexlinked ? 0 : 0.5 ) );
				prod.add(r);
				mrec.add(r);
			}

			if ((ha[j][1] == null && hb[j][1] != null) || (ha[j][1] != null && hb[j][1] == null)) 
				throw new RuntimeException("Locus missmatch in TwoLocusPoduct constructor");

			if (ha[j][1] != null && hb[j][1] != null)
			{
				Recombination r = new Recombination(ha[j][1],hb[j][1],0.5);
				prod.add(r);
				frec.add(r);
			}
		}

		g = new GraphicalModel(prod,null,false);

		lhalf = g.logPeel();
		log10 = Math.log(10);
	}

	public double evaluate(double theta)
	{
		if (!sexlinked)
			for (Recombination m : mrec)
				m.fix(theta);

		for (Recombination f :frec)
			f.fix(theta);

		return (g.logPeel() - lhalf) / log10 ;
	}

// Private data and methods.

	private double lhalf = 0;
	private double log10 = 0;
	private GraphicalModel g = null;
	private boolean sexlinked = false;
	private Set<Recombination> mrec = null;
	private Set<Recombination> frec = null;
}
