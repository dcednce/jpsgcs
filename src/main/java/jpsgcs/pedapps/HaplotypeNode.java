package jpsgcs.pedapps;

import jpsgcs.viewgraph.StringNode;
import jpsgcs.viewgraph.VertexRepresentation;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.Color;

public class HaplotypeNode extends StringNode
{
	public HaplotypeNode(String s, Color c, int h)
	{
		super(s,c,h);
	}

	public HaplotypeNode(StringNode s)
	{
		this(s,null,null,null);
	}

	public HaplotypeNode(StringNode s, int[] pat, int[] mat, Color[] colours)
	{
		this (s,pat,mat,colours,-1,-1);
	}

	public HaplotypeNode(StringNode s, int[] pat, int[] mat, Color[] colours, int affection, int liability)
	{
		super(s);
		p = pat;
		m = mat;
		map = colours;
		stat = affection;
		liab = liability;
	}

	public void setHaplotypes(int[] pat, int[] mat, Color[] col)
	{
		p = pat;
		m = mat;
		map = col;
	}

	public void setStatus(int a, int l)
	{
		stat = a;
		liab = l;
	}

	public void setHapHeight(int h)
	{
		hapH = h;
	}

	public void paint(Graphics g, double dx, double dy, boolean b)
	{
		super.paint(g,dx,dy,b);

		int first = 1;

		int gap = 0;

		g.setColor(Color.black);

		if (stat >= 0)
		{
			gap += (5*height())/2;
			if (stat > 0)
			{
				String s = stat+" "+liab;
				g.drawString(s,(int)(dx-g.getFontMetrics().stringWidth(s)/2),(int)dy+gap);
			}
		}

		if (p != null)
		{
			int x = (int) dx - hsp - hapW;
			int y = (int) (dy+height()) + vsp + gap;
	
			for (int i=first; i<p.length; i++)
			{
				g.setColor(p[i] >= 0 ? map[p[i]] : Color.white);
				g.fillRect(x,y,hapW,hapH);
				y = y+hapH;
			}
	
			g.setColor(Color.black);
			g.drawRect(x,y-hapH*(p.length-first),hapW,(p.length-first)*hapH);
		}

		if (m != null)
		{
			int x = (int) dx + hsp;
			int y = (int) (dy+height()) + vsp + gap;
	
			for (int i=first; i<m.length; i++)
			{
				g.setColor(m[i] >= 0 ? map[m[i]] : Color.white);
				g.fillRect(x,y,hapW,hapH);
				y = y+hapH;
			}

			g.setColor(Color.black);
			g.drawRect(x,y-hapH*(p.length-first),hapW,(p.length-first)*hapH);
		}
	}

	private int[] p = null;
	private int[] m = null;
	private int stat = 0;
	private int liab = 0;

	private int hsp = 2;
	private int vsp = -2;
	private int hapW = 6;
	private int hapH = 8;
	private Color[] map = null;
}
