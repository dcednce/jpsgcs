package jpsgcs.pedapps;

import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkageLocus;
import jpsgcs.genio.GeneticDataSource;

import jpsgcs.pedmcmc.Inheritance;

public class FullInfoLinkageData extends LinkageDataSet
{
	public FullInfoLinkageData(GeneticDataSource d, boolean linkfirst)
	{
		par = new LinkageParameterData(((LinkageInterface)d).raw().getParameterData());

		boolean sexlinked = par.isSexLinked();

		first = linkfirst ? 0 : 1;
		pa = Pedigrees.fathers(d);
		ma = Pedigrees.mothers(d);
		order = Pedigrees.canonicalOrder(d);

		int na = 1;

		if (sexlinked)
		{
			for (int j=0; j<d.nIndividuals(); j++)
			{
				if (d.isFemale(j))
					if (d.pa(j) < 0)
						na++;

				if (d.ma(j) < 0)
					na++;
			}
		}
		else
		{
			for (int j=0; j<d.nIndividuals(); j++)
			{
				if (d.pa(j) < 0)
					na++;
				if (d.ma(j) < 0)
					na++;
			}
		}

		LinkageLocus full = LinkageLocus.fullyInformativeLocus(na,sexlinked);

		LinkageLocus[] loc = par.getLoci();
		for (int i=first; i<loc.length; i++)
			loc[i] = full;

		ped = new LinkagePedigreeData(((LinkageInterface)d).raw().getPedigreeData());

		LinkageIndividual[] ind = ped.getIndividuals();
		for (int i=first; i<d.nLoci(); i++)
			for (int j=0; j<ind.length; j++)
				ind[j].pheno[i] = full.makePhenotype(ind[j]);

		na = 1;

		if (sexlinked)
		{
			for (int j=0; j<ind.length; j++)
			{
				int a1 = 0;
				int a2 = 0;
	
				if (d.isFemale(j))
					if (d.pa(j) < 0)
						a1 = na++;
					
				if (d.ma(j) < 0)
					a2 = na++;
					
				for (int i=first; i<d.nLoci(); i++)
					ind[j].pheno[i].setAlleles(a1,a2);
			}
		}
		else
		{
			for (int j=0; j<ind.length; j++)
			{
				int a1 = 0;
				int a2 = 0;
	
				if (d.pa(j) < 0)
					a1 = na++;
					
				if (d.ma(j) < 0)
					a2 = na++;
					
				for (int i=first; i<d.nLoci(); i++)
					ind[j].pheno[i].setAlleles(a1,a2);
			}
		}
	}

	public void set(Inheritance[][][] h)
	{
		LinkageIndividual[] ind = ped.getIndividuals();

		for (int jj=0; jj<order.length; jj++)
		{
			int j = order[jj];

			for (int i=first; i<ind[j].pheno.length; i++)
			{
				int a0 = ind[j].pheno[i].getAllele(0);
				int a1 = ind[j].pheno[i].getAllele(1);
				
				if (pa[j] >= 0)
					a0 = ind[pa[j]].pheno[i].getAllele(h[i][j][0].getState());

				if (ma[j] >= 0)
					a1 = ind[ma[j]].pheno[i].getAllele(h[i][j][1].getState());

				ind[j].pheno[i].setAlleles(a0,a1);
			}
		}
	}

// Private data and methods.

	private int first = 0;
	private int[] pa = null;
	private int[] ma = null;
	private int[] order = null;
}
