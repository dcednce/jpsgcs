package jpsgcs.pedapps;

import jpsgcs.markov.GraphicalModel;
import jpsgcs.genio.BasicGeneticData;
import jpsgcs.pedmcmc.Inheritance;
import jpsgcs.pedmcmc.LocusProduct;
import jpsgcs.pedmcmc.TwoSidedRecombination;

import java.util.Set;
import java.util.LinkedHashSet;

public class MultiPoints
{
	public MultiPoints(BasicGeneticData d, Inheritance[][][] inh, double[] pos)
	{
		sexlinked = d.sexLinked();

		h = inh;
		positions = LocInfo.mapPositions(d,pos,false);

		LocusProduct p = new LocusProduct(d,0);
		Inheritance[][] hh = p.getInheritances();
		right = new Inheritance[hh.length][2];
		left = new Inheritance[hh.length][2];

		mrec = new LinkedHashSet<TwoSidedRecombination>();
		frec = new LinkedHashSet<TwoSidedRecombination>();
		
		for (int j=0; j<hh.length; j++)
		{
			if (hh[j][0] != null)
			{
				left[j][0] = new Inheritance();
				right[j][0] = new Inheritance();

				TwoSidedRecombination m = null;
				if (sexlinked)
				{
					m = new TwoSidedRecombination(left[j][0],hh[j][0],right[j][0],0.0,0.0);
					left[j][0].setState( d.isMale(j) ? 0 : 1 );
					right[j][0].setState( d.isMale(j) ? 0 : 1 );
				}
				else
				{
					m = new TwoSidedRecombination(left[j][0],hh[j][0],right[j][0],0.5,0.5);
				}
				p.add(m);

				p.remove(left[j][0]);
				p.remove(right[j][0]);

				mrec.add(m);
			}

			if (hh[j][1] != null)
			{
				left[j][1] = new Inheritance();
				right[j][1] = new Inheritance();
			
				TwoSidedRecombination f = new TwoSidedRecombination(left[j][1],hh[j][1],right[j][1],0.5,0.5);
				p.add(f);
				
				p.remove(left[j][1]);
				p.remove(right[j][1]);

				frec.add(f);
			}
		}
		
		g = new GraphicalModel(p,null,true);
		g.reduceStates();
		loglhalf = g.logPeel();

		ratios = new double[positions.length];
	}

	public void update()
	{
		for (int i=0; i<ratios.length; i++)
		{
			LocInfo l = positions[i];

			for (int j=0; j<left.length; j++)
			{
				if (left[j][0] != null)
					left[j][0].setState(h[1+l.ileft][j][0].getState());
				if (left[j][1] != null)
					left[j][1].setState(h[1+l.ileft][j][1].getState());
			}

			for (int j=0; j<right.length; j++)
			{
				if (right[j][0] != null)
					right[j][0].setState(h[1+l.iright][j][0].getState());
				if (right[j][1] != null)
					right[j][1].setState(h[1+l.iright][j][1].getState());
			}

			if (!sexlinked)
				for (TwoSidedRecombination m : mrec)
					m.fix(l.tleft,l.tright);

			for (TwoSidedRecombination f : frec)
				f.fix(l.tleft,l.tright);

			ratios[i] += Math.exp( g.logPeel() - loglhalf );
		}

		count += 1;
	}

/** 
 Returns a matrix of doubles. The first index indicates the locus, the second indicates
 the value of theta. Each value is the total likelihood ratio
*/
	public double[] results()
	{
		double[] lods = new double[ratios.length];

		for (int i=0; i<lods.length; i++)
			lods[i] = Math.log10 ( ratios[i] /count );

		return lods;
	}

// Private data and methods.

	private boolean sexlinked = false;
	private Set<TwoSidedRecombination> mrec = null;
	private Set<TwoSidedRecombination> frec = null;

	private LocInfo[] positions = null;
	private double loglhalf = 0;
	private Inheritance[][][] h = null;
	private Inheritance[][] right = null;
	private Inheritance[][] left = null;
	private GraphicalModel g = null;
	private double[] ratios = null;
	private double count = 0;
}
