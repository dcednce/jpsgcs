package jpsgcs.pedapps;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LDModel;

public class PhaseKnownHaplotypes extends CompleteHaplotypes
{
	public PhaseKnownHaplotypes(GeneticDataSource x, int hold)
	{
		data = x;
		haps = new int[2*data.nIndividuals()][hold];
		offset = 0;

		for (int j=0; j<data.nIndividuals(); j++)
		{
			for (int i=0; i<haps[0].length; i++)
			{
				int a0 = data.getAllele(i,j,0);
				int a1 = data.getAllele(i,j,1);

				if (a0 < 0)
				{
					System.err.println("Warning: Missing allele data in phase known genotypes.");
					System.err.println("Setting to 0");
					a0 = 0;
				}
			
				if (a1 < 0)
				{
					System.err.println("Warning: Missing allele data in phase known genotypes.");
					System.err.println("Setting to 0");
					a1 = 0;
				}

				haps[2*j][i] = a0;
				haps[2*j+1][i] = a1;
			}
		}
	}

	public void simulate() { }

	public void maximize() { }

	public void update(boolean random)
	{
	}

	public void shift(int n)
	{
		int m = n;
		if (offset + haps[0].length + m > data.nLoci())
			m = data.nLoci() - haps[0].length - offset;
		
		offset += m;


		for (int j=0; j<data.nIndividuals(); j++)
		{
			for (int i=0; i<haps[0].length-m; i++)
			{
				haps[2*j][i] = haps[2*j][i+m];
				haps[2*j+1][i] = haps[2*j+1][i+m];
			}

			for (int i=haps[0].length-m; i<haps[0].length; i++)
			{
				int a0 = data.getAllele(i+offset,j,0);
				int a1 = data.getAllele(i+offset,j,1);

				if (a0 < 0)
				{
					System.err.println("Warning: Missing allele data in phase known genotypes.");
					System.err.println("Setting to 0");
					a0 = 0;
				}
			
				if (a1 < 0)
				{
					System.err.println("Warning: Missing allele data in phase known genotypes.");
					System.err.println("Setting to 0");
					a1 = 0;
				}

				haps[2*j][i] = a0;
				haps[2*j+1][i] = a1;
			}
		}
	}

	public void setModel(LDModel ld)
	{
	}
}
