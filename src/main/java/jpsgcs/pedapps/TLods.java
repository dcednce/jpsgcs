package jpsgcs.pedapps;

import jpsgcs.markov.GraphicalModel;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.Inheritance;
import jpsgcs.pedmcmc.LocusProduct;
import jpsgcs.pedmcmc.Recombination;

import java.util.Set;
import java.util.LinkedHashSet;

public class TLods
{
	public TLods(GeneticDataSource d, Inheritance[][][] inh, double[] thetas)
	{
		this(d,inh,thetas,false);
	}

	public TLods(GeneticDataSource d, Inheritance[][][] inh, double[] thetas, boolean veryverbose)
	{
		veryverb = veryverbose;

		sexlinked = d.sexLinked();

		h = inh;
		theta = thetas;

		LocusProduct p = new LocusProduct(d,0);
		Inheritance[][] hh = p.getInheritances();
		t = new Inheritance[hh.length][2];

		mrec = new LinkedHashSet<Recombination>();
		frec = new LinkedHashSet<Recombination>();

		for (int j=0; j<hh.length; j++)
		{
			if (hh[j][0] != null)
			{
				t[j][0] = new Inheritance();

				Recombination m = null;
				if (sexlinked)
				{
					m = new Recombination(hh[j][0],t[j][0],0.0) ;
					t[j][0].setState( d.isMale(j) ? 0 : 1 );
				}
				else
				{
					m = new Recombination(hh[j][0],t[j][0],0.5) ;
				}
				
				p.add(m);
				p.remove(t[j][0]);
				mrec.add(m);
			}

			if (hh[j][1] != null)
			{
				t[j][1] = new Inheritance();
				Recombination f = new Recombination(hh[j][1],t[j][1],0.5);
				p.add(f);
				p.remove(t[j][1]);
				frec.add(f);
			}
		}
		
		g = new GraphicalModel(p,null,true);
		g.reduceStates();
		loglhalf = g.logPeel();

		ratios = new double[h.length][theta.length];
	}

	public void update()
	{
		for (int i=1; i<h.length; i++)
		{
			for (int j=0; j<t.length; j++)
			{
				if (t[j][0] != null)
					t[j][0].setState(h[i][j][0].getState());

				if (t[j][1] != null)
					t[j][1].setState(h[i][j][1].getState());
			}

			for (int j=0; j<theta.length; j++)
			{
				if (!sexlinked)
					for (Recombination m : mrec)
						m.fix(theta[j]);

				for (Recombination f : frec)
					f.fix(theta[j]);

				double res = g.logPeel() - loglhalf;
				ratios[i][j] += Math.exp( res );

				if (veryverb)
					System.out.println(res);
			}
		}

		count++;
	}

/** 
 Returns a matrix of doubles. The first index indicates the locus, the second indicates
 the value of theta. Each value is the total likelihood ratio
*/
	public double[][] results()
	{
		double[][] lods = new double[h.length][theta.length];

		for (int i=1; i<lods.length; i++)
			for (int k=0; k<lods[i].length; k++)
				lods[i][k] = Math.log10( ratios[i][k] / count );

		return lods;
	}

// Private data and methods.

	private boolean sexlinked = false;
	private Set<Recombination> mrec = null;
	private Set<Recombination> frec = null;
	private double[] theta = null;

	private double loglhalf = 0;
	private Inheritance[][][] h = null;
	private Inheritance[][] t = null;
	private double[][] ratios = null;
	private GraphicalModel g = null;
	private int count = 0;
	private boolean veryverb = false;
}
