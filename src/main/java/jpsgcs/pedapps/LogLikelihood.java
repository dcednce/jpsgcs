package jpsgcs.pedapps;

import jpsgcs.genio.BasicGeneticData;
import jpsgcs.pedmcmc.Inheritance;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.pedmcmc.LocusProduct;
import jpsgcs.pedmcmc.ErrorLocusProduct;
import jpsgcs.pedmcmc.Recombination;

public class LogLikelihood
{
	public LogLikelihood(BasicGeneticData d, Inheritance[][][] inh, boolean linkfirst, double error, int maxerr)
	{
		int first = linkfirst ? 0 : 1;

		h = inh;

		g = new GraphicalModel[h.length];

		for (int i=first; i<g.length; i++)
		{
			LocusProduct p = null;

			if (error > 0 && maxerr > 1 && d.nAlleles(i) <= maxerr)
			{
				p = new ErrorLocusProduct(d,i,error,true,true,h[i]);
			}
			else
			{
				p = new LocusProduct(d,i,true,true,h[i]);
			}

			for (int j=0; j<h[i].length; j++)
				for (int k=0; k<h[i][j].length; k++)
					if (h[i][j][k] != null)
						p.remove(h[i][j][k]);

			g[i] = new GraphicalModel(p,null,false);
		}

		m = new MarkovRandomField[h[first].length][h[first][0].length];
		
		for (int j=0; j<m.length; j++)
		{
			if (h[first][j][0] != null)
			{
				m[j][0] = new MarkovRandomField();
				for (int i=1+first; i<h.length; i++)
					m[j][0].add(new Recombination(h[i-1][j][0],h[i][j][0],d.getMaleRecomFrac(i-1,i)));
			}

			if (h[first][j][1] != null)
			{
				m[j][1] = new MarkovRandomField();
				for (int i=1+first; i<h.length; i++)
					m[j][1].add(new Recombination(h[i-1][j][1],h[i][j][1],d.getFemaleRecomFrac(i-1,i)));
			}
		}
			
	}
	
	public double logLikelihood()
	{
		double x = 0;

		for (int i=0; i<g.length; i++)
			if (g[i] != null)
				x += g[i].logPeel();

		for (int i=0; i<m.length; i++)
			for (int j=0; j<m[i].length; j++)
				if (m[i][j] != null)
					x += m[i][j].logValue();

		return x;
	}

// Private data and methods.

	Inheritance[][][] h = null;
	GraphicalModel[] g = null;
	MarkovRandomField[][] m = null;
}
