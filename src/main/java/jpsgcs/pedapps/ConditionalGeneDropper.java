package jpsgcs.pedapps;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedmcmc.Recombination;
import jpsgcs.pedmcmc.LocusProduct;
import jpsgcs.markov.Variable;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.MarkovRandomField;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Random;

public class ConditionalGeneDropper
{
	public ConditionalGeneDropper(GeneticDataSource d, int where, Random r)
	{
		this(d,new LDModel(d),where,r);
	}

	public ConditionalGeneDropper(GeneticDataSource d, LDModel ld, int where, Random rand)
	{
		if (where <= 0 || where >= d.nLoci()-1)
			throw new RuntimeException("Specified position of disease locus out of range");

		data = d;

		// Make the ld graphical model.

		all = new Variable[d.nLoci()];
		for (int i=0; i<d.nLoci(); i++)
			all[i] = new Variable(d.nAlleles(i));

		Map<Variable,Variable> map = new LinkedHashMap<Variable,Variable>();
		Variable[] ldv = ld.getLocusVariables();
		for (int i=0; i<all.length; i++)
			map.put(ldv[i],all[i]);

		gall = new GraphicalModel(ld.replicate(map),rand,true);
		gall.collect();

		// Make the paternal inheritance model.

		pat = new Variable[d.nLoci()];
		for (int i=0; i<pat.length; i++)
			pat[i] = new Variable(2);

		MarkovRandomField p = new MarkovRandomField();
		for (int i=1; i<=where; i++)
			p.add(new Recombination(pat[i],pat[i-1],d.getMaleRecomFrac(i,i-1)));

		double th  = d.getMaleRecomFrac(where+1,where);
		th = GeneticMap.thetaTocMKosambi(th);
		th = th/2.0;
		th = GeneticMap.cMToThetaKosambi(th);

		p.add(new Recombination(pat[where],pat[0],th));
		p.add(new Recombination(pat[0],pat[where+1],th));

		for (int i=where+2; i<d.nLoci(); i++)
			p.add(new Recombination(pat[i],pat[i-1],d.getMaleRecomFrac(i,i-1)));
	
		gpat = new GraphicalModel(p,rand,false);
		
		// Make the maternal inheritance model.

		mat = new Variable[d.nLoci()];
		for (int i=0; i<mat.length; i++)
			mat[i] = new Variable(2);

		p = new MarkovRandomField();
		for (int i=1; i<=where; i++)
			p.add(new Recombination(mat[i],mat[i-1],d.getFemaleRecomFrac(i,i-1)));

		th  = d.getFemaleRecomFrac(where+1,where);
		th = GeneticMap.thetaTocMKosambi(th);
		th = th/2.0;
		th = GeneticMap.cMToThetaKosambi(th);

		p.add(new Recombination(mat[where],mat[0],th));
		p.add(new Recombination(mat[0],mat[where+1],th));

		for (int i=where+2; i<d.nLoci(); i++)
			p.add(new Recombination(mat[i],mat[i-1],d.getFemaleRecomFrac(i,i-1)));

		gmat = new GraphicalModel(p,rand,false);

		// Make the disease locus inheritance model.

		LocusProduct disp = new LocusProduct(data,0);
		gdis = new GraphicalModel(disp,rand,true);
		gdis.collect();

		// Get the disease inheritance states.

		disinh = disp.getInheritances();

		// Allocate the allele matrix.

		alleles = new int[d.nLoci()][d.nIndividuals()][2];

		pa = new int[d.nIndividuals()];
		ma = new int[d.nIndividuals()];
		
		for (int i=0; i<pa.length; i++)
		{
			pa[i] = d.pa(i);
			ma[i] = d.ma(i);
		}

		ord = Pedigrees.canonicalOrder(d);
	}

	public void geneDrop(boolean allout)
	{
		sampleAlleles();

		for (int i=1; i<data.nLoci(); i++)
		{
			for (int j=0; j<data.nIndividuals(); j++)
			{
				int a0 = data.getAllele(i,j,0);
				if (allout || a0 >= 0)
					a0 = alleles[i][j][0];

				int a1 = data.getAllele(i,j,1);
				if (allout || a1 >= 0)
					a1 = alleles[i][j][1];

				data.setAlleles(i,j,a0,a1);
			}
		}
	}

// Private data.

	private GeneticDataSource data = null;

	private int[][][] alleles = null;
	private int[] pa = null;
	private int[] ma = null;
	private int[] ord = null;
	
	private Variable[] all = null;
	private Variable[] pat = null;
	private Variable[] mat = null;
	private GraphicalModel gall = null;
	private GraphicalModel gpat = null;
	private GraphicalModel gmat = null;
	private GraphicalModel gdis = null;
	private Variable[][] disinh = null;

	private void sampleAlleles()
	{
		gdis.drop();

		for (int jj=0; jj<ord.length; jj++)
		{
			int j = ord[jj];

			if (pa[j] < 0)
			{
				gall.drop();
				for (int i=0; i<alleles.length; i++)
					alleles[i][j][0] = all[i].getState();
			}
			else
			{
				int[] sss = {disinh[j][0].getState()};
//System.out.print(disinh[j][0].getState());
				pat[0].setStates(sss);
				pat[0].setState(sss[0]);
//System.out.print(pat[0].getState());
				gpat.collect();
				gpat.drop();
				for (int i=0; i<alleles.length; i++)
					alleles[i][j][0] = alleles[i][pa[j]][pat[i].getState()];
//System.out.print(pat[1].getState());
			}

			if (ma[j] < 0)
			{
				gall.drop();
				for (int i=0; i<alleles.length; i++)
					alleles[i][j][1] = all[i].getState();
			}
			else
			{
				int[] sss = {disinh[j][1].getState()};
//System.out.print(disinh[j][1].getState());
				mat[0].setStates(sss);
				mat[0].setState(sss[0]);
//System.out.print(mat[0].getState());
				gmat.collect();
				gmat.drop();
				for (int i=0; i<alleles.length; i++)
					alleles[i][j][1] = alleles[i][ma[j]][mat[i].getState()];
//System.out.print(mat[1].getState());
			}
		}
//System.out.println();
	}
}
