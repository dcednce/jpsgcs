package jpsgcs.pedapps;

import jpsgcs.genio.BasicGeneticData;
import jpsgcs.genio.GeneticDataSource;

public class AlleleSharing
{
	public static double[] weightedPairedHGS(BasicGeneticData x)
	{
		return weightedPaired(x,2);
	}

	public static double[] weightedPairedHomSGS(BasicGeneticData x)
	{
		return weightedPaired(x,1);
	}

	public static double[] weightedPairedSGS(BasicGeneticData x)
	{
		return weightedPaired(x,0);
/*
		int[] probs = probands(x);
		int[] rank = Pedigrees.canonicalRank(x);
		double[] t = new double[x.nLoci()];
		double tw = 0;
		double minvlog2 = -1/Math.log(2.0);
		
		for (int i=0; i<probs.length; i++)
			for (int j=i+1; j<probs.length; j++)
			{
				int[] s = runs(hetSharing(x,probs[i],probs[j]),2);
				double w = minvlog2 * Math.log(Pedigrees.kinship(rank,x,probs[i],probs[j]));
				for (int k=0; k<t.length; k++)
					t[k] += w * s[k];
				tw += w;
			}

		for (int k=0; k<t.length; k++)
			t[k] /= tw;
		
		return t;
*/
	}

	public static double[] weightedPaired(BasicGeneticData x, int option)
	{
		int[] probs = probands(x);
		int[] rank = Pedigrees.canonicalRank(x);
		double[] t = new double[x.nLoci()];
		double tw = 0;
		double minvlog2 = -1/Math.log(2.0);
		
		for (int i=0; i<probs.length; i++)
			for (int j=i+1; j<probs.length; j++)
			{
				int[] s = null;
				switch(option)
				{
				case 0: s = hetSharing(x,probs[i],probs[j]);
					break;
				case 1: s = homSharing(x,probs[i],probs[j]);
					break;
				case 2: s = homozygotes(x,probs[i],probs[j]);
					break;
				}
				s = runs(s,2);

				double w = minvlog2 * Math.log(Pedigrees.kinship(rank,x,probs[i],probs[j]));
				for (int k=0; k<t.length; k++)
					t[k] += w * s[k];
				tw += w;
			}

		for (int k=0; k<t.length; k++)
			t[k] /= tw;

		return t;
	}

	public static int[] pairedHomSGS(BasicGeneticData x)
	{
		return paired(x,1);
	}

	public static int[] pairedHGS(BasicGeneticData x)
	{
		return paired(x,2);
	}

	public static int[] pairedSGS(BasicGeneticData x)
	{
		return paired(x,0);

/*
		int[] probs = probands(x);
		int[] t = new int[x.nLoci()];

		for (int i=0; i<probs.length; i++)
			for (int j=i+1; j<probs.length; j++)
			{
				int[] s = sharing(x,probs[i],probs[j]);
				s = runs(s,2);
				for (int k=0; k<t.length; k++)
					t[k] += s[k];
			}
		
		return t;
*/
	}

	public static int[] paired(BasicGeneticData x, int option)
	{
		int[] probs = probands(x);
		int[] t = new int[x.nLoci()];

		for (int i=0; i<probs.length; i++)
			for (int j=i+1; j<probs.length; j++)
			{
				int[] s = null;
				switch(option)
				{
				case 0: s = hetSharing(x,probs[i],probs[j]);
					break;
				case 1: s = homSharing(x,probs[i],probs[j]);
					break;
				case 2: s = homozygotes(x,probs[i],probs[j]);
					break;
				}
				s = runs(s,2);

				for (int k=0; k<t.length; k++)
					t[k] += s[k];
			}

		return t;
	}

	public static int[] hetSharing(BasicGeneticData x)
	{
		return hetSharing(x,probands(x));
	}

	private static int[] hetSharing(BasicGeneticData x, int a, int b)
	{
		int[] pair = {a,b};
		return hetSharing(x,pair);
	}
			
	private static int[] hetSharing(BasicGeneticData x, int[] probs)
	{
		int[] s = new int[x.nLoci()];

		for (int i=0; i<s.length; i++)
		{
			int[] c = new int[x.nAlleles(i)];
			
			for (int j=0; j<probs.length; j++)
			{
				int a = x.getAllele(i,probs[j],0);
				int b = x.getAllele(i,probs[j],1);
				
				if (a < 0 || b < 0)
					for (int k=0; k<c.length; k++)
						c[k]++;
				else if (a == b)
					c[a]++;
				else
				{
					c[a]++;
					c[b]++;
				}
			}

			s[i] = 0;
			for (int k=0; k<c.length; k++)
				if (s[i] < c[k])
					s[i] = c[k];
		}

		return s;
	}

	public static int[] homSharing(BasicGeneticData x, int a, int b)
	{
		int[] pair = {a,b};
		return homSharing(x,pair);
	}

	public static int[] homSharing(BasicGeneticData x)
	{
		return homSharing(x,probands(x));
	}

	public static int[] homSharing(BasicGeneticData x, int[] probs)
	{
		int[] s = new int[x.nLoci()];
			
		for (int i=0; i<s.length; i++)
		{
			int[] c = new int[x.nAlleles(i)];
			
			for (int j=0; j<probs.length; j++)
			{
				int a = x.getAllele(i,probs[j],0);
				int b = x.getAllele(i,probs[j],1);
				
				if (a < 0 || b < 0)
					for (int k=0; k<c.length; k++)
						c[k]++;
				else if (a == b)
					c[a]++;
			}

			s[i] = 0;
			for (int k=0; k<c.length; k++)
				if (s[i] < c[k])
					s[i] = c[k];
		}

		return s;
	}

	public static int[] homozygotes(BasicGeneticData x, int a, int b)
	{
		int[] pair = {a,b};
		return homozygotes(x,pair);
	}

	public static int[] homozygotes(BasicGeneticData x)
	{
		return homozygotes(x,probands(x));
	}

	public static int[] homozygotes(BasicGeneticData x, int[] probs)
	{
		int[] s = new int[x.nLoci()];

		for (int i=0; i<s.length; i++)
		{
			for (int j=0; j<probs.length; j++)
			{
				int a = x.getAllele(i,probs[j],0);
				int b = x.getAllele(i,probs[j],1);
				if (a < 0 || b < 0 || a == b)
					s[i]++;
			}
		}

		return s;
	}

	private static int[] probands(BasicGeneticData x)
	{
		int[] probs = new int[x.nProbands()];
		int np = 0;
		for (int i=0; i<x.nIndividuals(); i++)
			if (x.proband(i) == 1)
				probs[np++] = i;

		return probs;
	}
			
	public static int[] runs(int[] s, int n)
	{
		int[] u = new int[s.length];
		if (s[0] >= n)
			u[0] = 1;
		for (int i=1; i<s.length; i++)
			if (s[i] >= n)
				u[i] = u[i-1] + 1;

		int[] v = new int[s.length];
		if (s[s.length-1] >= n)
			v[s.length-1] = 1;
		for (int i=s.length-2; i>=0; i--)
			if (s[i] >= n)
				v[i] = v[i+1] +1;

		for (int i=0; i<s.length; i++)
		{
			u[i] = u[i] +v[i]-1;
			if (u[i] < 0)
				u[i] = 0;
		}

		return u;
	}

	public static double max(double[] x)
	{
		double m = 0;
		for (int i=0; i<x.length; i++)
			if (m < x[i])
				m = x[i];
		return m;
	}

	public static int max(int[] x)
	{
		int m = 0;
		for (int i=0; i<x.length; i++)
			if (m < x[i])
				m = x[i];
		return m;
	}
}
