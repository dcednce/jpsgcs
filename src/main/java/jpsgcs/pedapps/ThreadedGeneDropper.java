package jpsgcs.pedapps;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedmcmc.Recombination;
import jpsgcs.markov.Variable;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.MarkovRandomField;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Random;

public class ThreadedGeneDropper extends GeneDropper
{
	public ThreadedGeneDropper(GeneticDataSource d, LDModel ld, Random r, int ngms)
	{
		// Copy address of data structure.
		data = d;

		// Set up multiple random number generators for non-founder haplotypes.
		rand = new Random[d.nIndividuals()][2];
		for (int j=0; j<d.nIndividuals(); j++)
		{
			rand[j][0] = new Random(r.nextLong());
			rand[j][1] = new Random(r.nextLong());
		}
		

		// Find order for genedrops and copy parent information.
		ord = Pedigrees.canonicalOrder(d);
		pa = new int[d.nIndividuals()];
		ma = new int[d.nIndividuals()];
		for (int j=0; j<d.nIndividuals(); j++)
		{
			pa[j] = d.pa(j);
			ma[j] = d.ma(j);
		}


		// Set up ngms graphical models that can be used in parallel for 
		// founder haplotypes.
		hapvar = new Variable[ngms][];
		gmhaps = new GraphicalModel[ngms];

		for (int k=0; k<ngms; k++)
		{
			hapvar[k] = new Variable[d.nLoci()];
			Map<Variable,Variable> map = new LinkedHashMap<Variable,Variable>();
			Variable[] ldv = ld.getLocusVariables();
			map.clear();
			for (int i=0; i<d.nLoci(); i++)
			{
				hapvar[k][i] = new Variable(d.nAlleles(i));
				map.put(ldv[i],hapvar[k][i]);
			}
			gmhaps[k] = new GraphicalModel(ld.replicate(map),new Random(r.nextLong()),true);
			gmhaps[k].collect();
		}


		// Copy the male and female recombination fractions for non-founder inheritances.
		femrec = new double[d.nLoci()];
		malrec = new double[d.nLoci()];
		femrec[0] = 0.5;
		malrec[0] = 0.5;
		for (int i=1; i<d.nLoci(); i++)
		{
			femrec[i] = d.getFemaleRecomFrac(i,i-1);
			malrec[i] = d.getMaleRecomFrac(i,i-1);
		}


		// Make big matrix to store haplotypes.
		alleles = new int[d.nIndividuals()][2][d.nLoci()];
	}

	
	private int gmcount = 0;

	private void setFounderAlleles(int j, int h)
	{
		int k ;

		synchronized(this)
		{
			k = (gmcount++) % gmhaps.length;
		}

		synchronized(gmhaps[k])
		{
			gmhaps[k].drop();
			for (int i=0; i<data.nLoci(); i++)
				alleles[j][h][i] = hapvar[k][i].getState();
		}
	}

	private void setNonFounderAlleles(int j, int h)
	{
		double[] recomb = null;
		int[][] parhap = null;

		if (h == 0)
		{
			recomb = malrec;
			parhap = alleles[pa[j]];
		}
		else
		{
			recomb = femrec;
			parhap = alleles[ma[j]];
		}


		for (int i=0, s=0; i<data.nLoci(); i++)
		{
			s = ( rand[j][h].nextDouble() < recomb[i] ? 1-s : s );
			alleles[j][h][i] = parhap[s][i];
		}
	}

	public void dropToIndividual(int j, boolean allout)
	{
		if (pa[j] < 0)
			setFounderAlleles(j,0);
		else
			setNonFounderAlleles(j,0);

		if (ma[j] < 0)
			setFounderAlleles(j,1);
		else
			setNonFounderAlleles(j,1);

		for (int i=0; i<data.nLoci(); i++)
		{
			int a0 = data.getAllele(i,j,0);
			int a1 = data.getAllele(i,j,1);

			if (allout || a0 >= 0)
				a0 = alleles[j][0][i];
			if (allout || a1 >= 0)
				a1 = alleles[j][1][i];

			data.setAlleles(i,j,a0,a1);
		}
	}

	// Delegate work of gene dropping to threads.
	public void geneDrop(boolean allout)
	{
		try
		{
			DropperThread[] t = new DropperThread[ord.length];
		
			// Go through in canonical order because each thread needs pointers
			// to the threads of its parents.
			// Also, to join to its parents threads, the parent threads need to
			// have been started.

			for (int j : ord)
			{
				t[j] = new DropperThread(this,j,allout,
					(pa[j] < 0 ? null : t[pa[j]]), 
					(ma[j] < 0 ? null : t[ma[j]]));

				t[j].start();

				// Replace the above line with this line to make it sequential for testing.
				// t[j].run();
			}

			// Can't join as you make the threads because this one will wait
			// for each thread to complete before making and starting the next.
			for (int j : ord)
				t[j].join();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	public void geneDrop()
	{
		geneDrop(false);
	}

// Private data.

	private double[] femrec;
	private double[] malrec;
	private Random[][] rand;

	private Variable[][] hapvar = null;
	private GraphicalModel gmhaps[] = null;
}

class DropperThread extends Thread
{
	private ThreadedGeneDropper g = null;
	private Thread pa = null;
	private Thread ma = null;
	private int j = 0;
	private boolean allout = false;

	public DropperThread(ThreadedGeneDropper gd, int ind, boolean ao, Thread path, Thread math)
	{
		g = gd;
		j = ind;
		allout = ao;
		pa = path;
		ma = math;
	}

	public void run() 
	{
		try
		{
			if (pa != null)
				pa.join();
			if (ma != null)
				ma.join();
	
			g.dropToIndividual(j,allout);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
