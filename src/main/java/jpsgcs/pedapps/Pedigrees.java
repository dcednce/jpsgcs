package jpsgcs.pedapps;

import jpsgcs.genio.PedigreeData;

import java.util.LinkedHashSet;

public class Pedigrees
{
	public static int[] canonicalOrder(PedigreeData d)
	{
		LinkedHashSet<Integer> x = new LinkedHashSet<Integer>();
		
		for (int i=0; i<d.nIndividuals(); i++)
			add(i,d,x);

		int[] ord = new int[x.size()];
		int j = 0;
		for (Integer i : x)
			ord[j++] = i.intValue();

		return ord;
	}

	public static int[] canonicalRank(PedigreeData d)
	{
		int[] ord = canonicalOrder(d);
		int[] dob = new int[ord.length];
		for (int k=0; k<ord.length; k++)
			dob[ord[k]] = k;
		return dob;
	}

	public static int[] generation(PedigreeData d)
	{
		int[] ord = canonicalOrder(d);
		int[] gen = new int[ord.length];
		
		for (int jj=0; jj<ord.length; jj++)
		{
			int j = ord[jj];
			gen[j] = 0;

			if (d.pa(j) >= 0 && gen[j] < gen[d.pa(j)] + 1)
				gen[j] = gen[d.pa(j)] + 1;
		
			if (d.ma(j) >= 0 && gen[j] < gen[d.ma(j)] + 1)
				gen[j] = gen[d.ma(j)] + 1;
		}

		for (int jj=ord.length-1; jj>=0; jj--)
		{
			int j = ord[jj];

			int[] kids = d.kids(j);
			if (kids == null || kids.length == 0)
				continue;

			int min = ord.length;
			for (int k=0; k<kids.length; k++)
				if (min > gen[kids[k]])
					min = gen[kids[k]];

			gen[j] = min-1;
		}

		return gen;
	}

	public static void add(int i, PedigreeData d, LinkedHashSet<Integer> x)
	{
		if (i < 0 || x.contains(i))
			return;

		add(d.pa(i),d,x);
		add(d.ma(i),d,x);
		
		x.add(i);
	}
		
	public static int[][] parents(PedigreeData d)
	{
		int[][] par = new int[d.nIndividuals()][2];

		for (int i=0; i<par.length; i++)
		{
			par[i][0] = d.pa(i);
			par[i][1] = d.ma(i);
		}

		return par;
	}

	public static int[] fathers(PedigreeData d)
	{
		int[] pa = new int[d.nIndividuals()];
		for (int i=0; i<pa.length; i++)
			pa[i] = d.pa(i);
		return pa;
	}

	public static int[] mothers(PedigreeData d)
	{
		int[] ma = new int[d.nIndividuals()];
		for (int i=0; i<ma.length; i++)
			ma[i] = d.ma(i);
		return ma;
	}

	public static double kinship(PedigreeData d, int i, int j)
	{
		int[] ord = canonicalOrder(d);
		int[] dob = new int[ord.length];
		for (int k=0; k<ord.length; k++)
			dob[ord[k]] = k;
		
		return kinship(canonicalRank(d),d,i,j);
	}

	public static double inbreeding(int[] dob, PedigreeData d, int i, int j)
	{
		return kinship(dob,d,d.pa(i),d.ma(i));
	}

	public static double kinship(int[] dob, PedigreeData d, int i, int j)
	{
		if (i < 0 || j < 0)
			return 0;
		if (i == j)
			return 0.5 + 0.5*kinship(dob,d,d.pa(i),d.ma(i));

		if (dob[i] < dob[j])
			return (kinship(dob,d,i,d.pa(j)) + kinship(dob,d,i,d.ma(j))) * 0.5;
		
		if (dob[j] < dob[i])
			return (kinship(dob,d,j,d.pa(i)) + kinship(dob,d,j,d.ma(i))) * 0.5;

		return -1000;
	}
}
