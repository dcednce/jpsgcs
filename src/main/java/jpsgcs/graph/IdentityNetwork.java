package jpsgcs.graph;

import java.util.Map;
import java.util.IdentityHashMap;

/** 
    This implementation of a Network uses an IdentityHashMap and 
    a Set backed by IdentityHashMap to get a graph implementation
    that uses identity rather than equality to manage its elements.
    This is useful, for example, when the vertices of a graph are
    themselves Sets. Using this implemntation lets you change the 
    contents of the Sets without disturbing their relationship
    to eachother in the graph.
*/
public class IdentityNetwork<V,E> extends Network<V,E>
{
    /**
       Creates a new, empty undirected IdentityNetwork.
    */
    public IdentityNetwork()
        {
            this(false);
        }

    /**
       Creates an empty directed or undirected IdentityNetwork as specified
       by the given boolean.
    */
    public IdentityNetwork(boolean directed)
        {
            super(directed,true);
        }
}
