package jpsgcs.graph;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Collection;

public class DAG<V,E> extends Network<V,E>
{
    public DAG()
        {
            this(false);
        }

    public DAG(boolean identity)
        {
            super(true,identity);
        }

    public Set<V> parents(V x)
    {
        return inNeighbours(x);
    }

    public Set<V> children(V x)
    {
        return outNeighbours(x);
    }

    public Set<V> ancestors(V x)
    {
        Set<V> anc = new LinkedHashSet<V>();
        collectAncestors(x,anc);
        return anc;
    }

    public void collectAncestors(V x, Set<V> anc)
    {
        for (V v : parents(x))
            {
                if (anc.contains(v))
                    continue;
                anc.add(v);
                collectAncestors(v,anc);
            }
    } 

    public Set<V> descendants(V x)
    {
        Set<V> dec = new LinkedHashSet<V>();
        collectDescendants(x,dec);
        return dec;
    }

    public void collectDescendants(V x, Set<V> dec)
    {
        for (V v : children(x))
            {
                if (dec.contains(v))
                    continue;
                dec.add(v);
                collectDescendants(v,dec);
            }
    }

    public List<V> dagOrder()
    {
        return dagOrder(getVertices());
    }

    public List<V> dagOrder(Collection<V> x)
    {
        Set<V> done = new LinkedHashSet<V>();
        for (V v : x)
            collectInOrder(v,done,x);
        return new ArrayList<V>(done);
    }

    public void collectInOrder(V v, Set<V> done, Collection<V> x)
    {
        if (!x.contains(v))
            return;
        if (done.contains(v))
            return;
        for (V u : parents(v))
            {
                collectInOrder(u,done,x);
            }
        done.add(v);
    }

    public String toString()
    {
        StringBuffer s = new StringBuffer();
        for (V x : dagOrder())
            {
                s.append(x);
                for (V p : parents(x))
                    s.append(" "+p);
                s.append("\n");
            }
        s.deleteCharAt(s.length()-1);
        return s.toString();
    }
}
