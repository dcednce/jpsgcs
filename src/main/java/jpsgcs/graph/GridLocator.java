package jpsgcs.graph;

import jpsgcs.markov.Parameter;
import java.util.Collection;

public class GridLocator<V extends Coordinated,E> extends GraphLocator<V,E>
{
    public GridLocator(double xx, double yy)
        {
            x = xx;
            y = yy;
            par = new Parameter[0];
        }

    public double move(LocatedGraph<V,E> g)
    {
        return 0;
    }

    public void set(LocatedGraph<V,E> g)
    {
        for (V v : g.getVertices())
            {
                Coord p = g.getCoord(v);
                p.x = x * v.x();
                p.y = y * v.y();
            }
    }

    private double x = 0;
    private double y = 0;
}
