package jpsgcs.graph;

import jpsgcs.viewgraph.*;
import jpsgcs.animate.*;
import java.util.*;
import java.awt.*;

public class IteratingGraphDemo
{
    public static void main(String[] args)
    {
        try
            {
                int n = 10;

                Collection<Integer> x = new ArrayList<Integer>();
                for (int i=0; i<n; i++)
                    x.add(i);
        
                IteratingGraph<Integer,Object> g = new IteratingGraph<Integer,Object>(x);
                g.init();

                PaintableGraph<Integer,Object> pg = new PaintableGraph<Integer,Object>(g);
                GraphLocator<Integer,Object> loc = new RootedLocalLocator<Integer,Object>();
                Panel p = new GraphPanel<Integer,Object>(pg,loc);

                Frame fr = new Frame();
                fr.addWindowListener(new FrameQuitter());
                fr.add(p);
                fr.pack();
                fr.setVisible(true);

                while (true)
                    {
                        Thread.sleep(50);
                        do
                            {
                                g.next();
                            }
                        while(!jpsgcs.jtree.JTrees.isTriangulated(g));
                    }
            }
        catch (Exception e)
            {
                System.err.println("Caught in IteratingGraphDemo:main()");
                e.printStackTrace();
            }
    }
}
