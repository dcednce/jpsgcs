package jpsgcs.graph;

import jpsgcs.markov.Parameter;
import java.util.Collection;
import java.util.LinkedHashSet;

public class GlobalLocator<V,E> extends GraphLocator<V,E>
{
    public GlobalLocator()
        {
            Parameter[] p = {new Parameter("Repulsion",0,500,100)};
            par = p;
        }

    public double move(LocatedGraph<V,E> g)
    {
        double delta = 0;
        double beta = par[0].getValue();
        beta = 16 * beta * beta * beta * beta;
                
        Collection<V> vertices = new LinkedHashSet<V>(g.getVertices());

        for (V a : vertices)
            {
                Coord pa = g.getCoord(a);
                if (!pa.m)
                    continue;

                Derivatives D = new Derivatives();
                        
                if (beta > Double.MIN_VALUE)
                    addDerivatives(D, inverseSquareRepulsions(pa, g.getCoords()), beta);

                addDerivatives(D, squaredAttractions(pa, g.getCoords(g.getNeighbours(a))), 1);
        
                delta += update(pa,D);
            }

        return delta;
    }
}
