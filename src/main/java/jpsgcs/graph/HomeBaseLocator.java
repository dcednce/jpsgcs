package jpsgcs.graph;

import jpsgcs.markov.Parameter;
import jpsgcs.util.RadixPlaneSorter;
import java.util.Collection;
import java.util.LinkedHashSet;

public class HomeBaseLocator<V,E> extends GraphLocator<V,E>
{
    public HomeBaseLocator()
        {
            par= new Parameter[3];
            par[0] = new Parameter("Repulsion",0,500,100);
            par[1] = new Parameter("Home sickness",0,1,0);
            par[2] = new Parameter("Stretch",0,200,50);
        }

    public double move(LocatedGraph<V,E> g)
    {
        double delta = 0;
        double d = par[0].getValue();
        double gamma = d*d;
        double alpha = 2*d;
        double theta = par[1].getValue();
        double sigma = par[2].getValue();
                
        Collection<V> vertices = new LinkedHashSet<V>(g.getVertices());
        RadixPlaneSorter<Coord> p = null;
        if (alpha > Double.MIN_VALUE)
            {
                p = new RadixPlaneSorter<Coord>(Math.sqrt(gamma),Math.sqrt(gamma),40);
                for (V a : vertices)
                    p.add(g.getCoord(a));
            }   
                        
        for (V a : vertices)
            {
                Coord pa = g.getCoord(a);
                if (!pa.m)
                    continue;

                Derivatives D = new Derivatives();
                        
                addDerivatives(D, squaredAttractions(pa,g.getCoords(g.getNeighbours(a))), 1);

                if (theta > 0)
                    addDerivatives(D, homeCoordAttraction(pa,sigma), theta);

                if (p != null)
                    {
                        p.remove(pa);
                        addDerivatives(D, rootedLocalRepulsions(pa,p.getLocal(pa,Math.sqrt(gamma)),gamma), alpha);
                    }

                delta += update(pa,D);

                if (p != null)
                    p.add(pa);
            }

        return delta;
    }
}
