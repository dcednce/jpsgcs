package jpsgcs.graph;

import jpsgcs.util.Point;

public class Coord extends jpsgcs.util.Point
{
	public boolean m = true;
	public double x0 = 0;
	public double y0 = 0;

	public Coord()
	{
		super();
	}

	public Coord(double x, double y)
	{
		super(x,y);
	}
}
