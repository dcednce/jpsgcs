package jpsgcs.graph;

/**
   This program generates a random directed acyclic graph with a given number of vertices and
   average number of parents for any vertex.

   <ul>
   <li>Usage: <b> java RandomDAGraph [v] [p] > output.graph </b>
   </li>
   </ul>

   Where
   <ul>
   <li> <b> v </b> is the number of vertices. Default is 1000. </li>
   <li> <b> p </b> is the mean number of parents. Default is 2. </li>
   </ul>
   The output from this program can be immediately input to the
   graph viewing program ViewDAG.
*/
        
public class RandomDAG
{
    public RandomDAG(int nn, double rr)
    {
        n = nn;
        r = rr;
    }

    public Network<String,String> next()
    {
        Network<String,String> g = new Network<String,String>(true);

        String[] s = new String[n];

        for (int i=0; i<s.length; i++)
            {
                s[i] = new String(""+i);
                g.add(s[i]);

                for (int j=0; j<i; j++)
                    if (Math.random() < r/i)
                        g.connect(s[j],s[i]);
            }

        return g;
    }
                

    // Private data and methods.

    private int n = 0;
    private double r = 0;

    public static void main(String[] args)
    {
        try
            {
                int v = 1000;
                double p = 2;

                switch(args.length)
                    {
                    case 2: p = Double.parseDouble(args[1]);
                    case 1: v = Integer.parseInt(args[0]);
                    }

                System.out.println(new RandomDAG(v,p).next());
                        
            }
        catch (Exception e)
            {
                System.err.println("Caught in RandomDAGraph.main()");
                e.printStackTrace();
                System.exit(1);
            }
    }
}
