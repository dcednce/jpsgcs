package jpsgcs.cusp;

import java.util.Comparator;

public class Ping implements Comparable<Ping>
{
    public Ping(int me, int you, int time)
        {
            x = me;
            y = you;
            t = time;
        }

    public int compareTo(Ping p)
    {
        if (t < p.t)
            return -1;
        if (p.t < t)
            return 1;

        if (x < p.x)
            return -1;
        if (p.x < x)
            return 1;
                
        if (y < p.y)
            return -1;
        if (p.y < y)
            return 1;

        return 0;
    }

    public String toString()
    {
        return t+"\t"+x+"\t"+y;
    }

    // Private data.
        
    public int x = 0;
    public int y = 0;
    public int t = 0;
}
