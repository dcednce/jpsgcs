package jpsgcs.random;

public class NormalRV extends GaussianRV implements LogConcave 
                                                    //public class NormalRV extends GaussianRV implements DifferentiableLogConcave
{
    static { className = "NormalRV"; }

    public NormalRV()
    {
        super(0,1);
        //double[] x = {-4,-3,-2,-1,-0.5,0,0.5,1,2,3,4};
        double[] x = {-3,0,3};
        //setMethod(new ExponentialSwitching(this,x));
        setMethod(new AdaptiveRejection(this,x));
    }

    public double densityFunction(double x)
    {
        return Math.exp(-x*x/2);
    }

    public double logDensityFunction(double x)
    {
        return -x*x/2;
    }

    public double lowerBound()
    {
        return Double.NEGATIVE_INFINITY;
    }

    public double upperBound()
    {
        return Double.POSITIVE_INFINITY;
    }

    public double derivativeLogDensity(double x)
    {
        return -x;
    }
}
