package jpsgcs.random;

/**
   Implements the rejection method of generation.
*/
public class Rejection extends GenerationMethod
{
    /**
       Creates a new rejection scheme for the given target using the given
       envelope and multiplier.
       The density function of the target must be less than the density function
       of the envelope times the multiplier everywhere.
    */
    public Rejection(DensityFunction target, Envelope envelope, double multiplier)
    {
        X = target;
        Y = envelope;
        m = multiplier;
    }

    /**
       Applies the method and returns the generated value.
    */
    public double apply()
    {
        Point p = null;
        do 
            {
                p = Y.nextPoint();
            }
        while (U() > X.densityFunction(p.x)/p.y/m);
        return p.x;
    }

    /**
       Returns the target density.
    */
    public DensityFunction getTarget()
    {
        return X;
    }

    /**
       Returns the envelope density.
    */
    public Envelope getEnvelope()
    {
        return Y;
    }

    /**
       Sets the current envelope.
    */
    public void setEnvelope(Envelope d)
    {
        Y = d;
    }

    private DensityFunction X = null;
    private Envelope Y = null;
    private double m = 1;
}
