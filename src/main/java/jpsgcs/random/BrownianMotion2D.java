package jpsgcs.random;

/**
   This class simulates a two dimensional Brownian motion.
*/
public class BrownianMotion2D extends StochasticProcess
{
    static { className = "BrownianMotion2D"; }

    /**
       Creates a new 2d Brownian motion with variance parameters 1.
    */
    public BrownianMotion2D()
    {
        this(1,1);
    }

    /**
       Creates a new 2d Brownian motion with the given variance parameters.
    */
    public BrownianMotion2D(double xvar, double yvar)
    {
        X = new GaussianRV(0,1);
        Y = new GaussianRV(0,1);
        set(xvar,yvar);
    }

    /**
       Sets the variance parameters for the x and y dimensions.
    */
    public void set(double xvar, double yvar)
    {
        X.set(0,xvar);
        Y.set(0,yvar);
    }

    /**
       Returns the current state.
       The runtime type of the state is Locus2D.
    */
    public StochasticState getState()
    {
        return new Locus2D(time,x,y);
    }

    /**
       Sets the current state of the process.
       The state must be a Locus2D.
    */
    public void setState(StochasticState s)
    {
        Locus2D l = (Locus2D)s;
        time = l.time;
        x = l.x;
        y = l.y;
    }

    /**
       Advances the process t time units.
    */
    public void advance(double t)
    {
        time += t;
        x += X.next() * Math.sqrt(t);
        y += Y.next() * Math.sqrt(t);
    }
        
    private GaussianRV X = null;
    private GaussianRV Y = null;
    private double time = 0;
    private double x = 0;
    private double y = 0;
}
