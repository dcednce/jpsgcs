package jpsgcs.random;

/**
   This interface defines what a random variable must provide in 
   order that the ratio of uniforms method can be used to 
   generate it.
*/
public interface RatioRegion
{
    /**
       Returns true iff the given point is contained in the appropriate region
    */
    public boolean containsInRatioRegion(double x, double y);
}
