package jpsgcs.random;

/**
   This class provides integers uniformly generated in
   a given range.
   The method used is a simple linear transformation of 
   the output of the random engine, forced to be an integer.
*/
public class UniformIntegerRV extends IntegerValuedRV 
{
    static { className = "UniformIntegerRV"; }

    /**
       Creates a Uniform Integer random variable returning values
       between -1 and 1, inclusive.
    */
    public UniformIntegerRV()
    {
        this(-1,1);
    }

    /**
       Creates a new Uniform Integer random variable returning values
       between the specified maximum and minimum, inclusive.
    */

    public UniformIntegerRV(int minimum, int maximum)
    {
        set(minimum, maximum);
    }

    /**
       Sets the minimum and maximum values as specified.
    */
    public void set(int minimum, int maximum)
    { 
        if (minimum > maximum)
            throw new ParameterException("Uniform Integer random variable minimum must be <= maximum");
        min = minimum;
        range = 1+maximum-minimum;
    }

    /**
       Returns the next realisation.
    */
    public int nextI()
    {
        return min + (int)(range*U());
    }

    private int min = 0;
    private int range = 0;
}
