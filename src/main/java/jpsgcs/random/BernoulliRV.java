package jpsgcs.random;

/**
   This class provides replicates from the Bernoulli distribution.
   The method used is simply to compare a standard Uniform with
   the required probability and return 1 if it is less.
*/
public class BernoulliRV extends IntegerValuedRV
{
    static { className = "BernoulliRV"; }

    /** 
        Creates a Bernoulli random variable with success probability 0.5.
    */
    public BernoulliRV()
    {
        this(0.5);
    }

    /**
       Creates a Bernoulli random variable with the specified probabiliy
       of success.
       Success = 1, failure = 0.
    */
    public BernoulliRV(double prob)
    {
        set(prob);
    }

    /**
       Set the success probability to the given value.
    */
    public void set(double prob)
    {
        if (p < 0 || p > 1)
            throw new ParameterException("Bernoulli success probability must be in [0,1]");
        p = prob;
    }

    /**
       Return the next value. P(1) = p.
    */
    public int nextI()
    {
        return nextB() ? 1 : 0;
    }
        
    /**
       Return the next value as boolean. P(true) = p.
    */
    public boolean nextB()
    {
        return U() <= p;
    }

    private double p = 0;
}
