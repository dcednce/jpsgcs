package jpsgcs.random;

/**
   This class implements Metropolis's method of proposal
   and accptance updates for a Markov chain.
*/
public class Metropolis extends GenerationMethod
{
    /**
       Creates a new Metropolis update scheme for the given
       Markov chain which must implement the Metropolitan interface.
    */
    public Metropolis(Metropolitan Z)
    {
        X = (MarkovChain) Z;
        Y = Z;
    }
        
    /**
       Apply the method.
       Returns 0.
    */
    public double apply()
    {
        int t = Y.proposal();
        if (U() < Y.acceptance(t))
            X.state = t;
        return 0;
    }
        
    private MarkovChain X = null;
    private Metropolitan Y = null;
}
