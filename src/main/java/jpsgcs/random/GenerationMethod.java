package jpsgcs.random;

/**
   This is the base class for the general methods for transforming
   uniform random variates into other distribution.
*/
abstract public class GenerationMethod extends Random
{
    /**
       Applies the method and returns the resulting value.
    */
    abstract public double apply();
}
