package jpsgcs.random;

import java.util.Set;
import java.util.Iterator;

/**
   This class is used to sample elements with replacement 
   from java structures.
*/

public class Sampler extends Random
{
    public Object sample(Set s)
    {
        Iterator i = s.iterator();
        for(int n = (int) (s.size() * U()); n>0; n--)
            i.next();
        return i.next();
    }
}
