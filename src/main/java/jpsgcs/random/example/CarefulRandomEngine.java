package jpsgcs.random.example;

abstract public class CarefulRandomEngine
{
    abstract protected double nextOne();
        
    public double next()
    {
        double d = nextOne();
        if (d >= 0.0 && d <= 1.0)
            return d;
        else
            throw new Error("Generator out of range");
    }
}
