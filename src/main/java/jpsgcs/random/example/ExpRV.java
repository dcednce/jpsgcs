package jpsgcs.random.example;

public class ExpRV
{
    public ExpRV(CongGen gen)
    {
        g = gen;
    }

    public double next()
    {
        return -Math.log(g.next());
    }

    private CongGen g = null;

    public static void main(String[] args)
    {
        CongGen g = new CongGen(1,45,1,2048);
        UnifRV U = new UnifRV(g);
        ExpRV E = new ExpRV(g);
        for (int i=0; i<20; i++)
            System.out.println(U.next());
        for (int i=0; i<20; i++)
            System.out.println(E.next());
    }
}
