package jpsgcs.random.example;

public class CongGen
{
    public CongGen(long seed,long mult,long shift,long mod)
    {
        s = seed;
        m = mult;
        h = shift;
        d = mod;
    }

    public double next()
    {
        s = ( s * m + h) % d;
        return s / (double) d;
    }

    private long s, m, h, d;
}
