package jpsgcs.random;

/**
   This class provides replicates from the Gaussian or Normal distribution.
   The method used is ratio of uniforms.
*/
public class GaussianRV extends RandomVariable implements RatioRegion
{
    static { className = "GaussianRV"; }

    /**
       Creates a new Gaussian random variable with mean 0 and variance 1.
    */
    public GaussianRV()
    {
        this(0,1);
    }

    /**
       Creates a new Gaussian random variable with given mean and variance.
    */
    public GaussianRV(double mean, double variance)
    {
        set(mean,variance);
        setMethod(new RatioOfUniforms(this,new UniformRV(),new UniformRV(-C1,C1)));
    }

    /**
       Sets the mean and variance parameters.
    */
    public void set(double mean, double variance)
    {
        if (variance < 0)
            throw new ParameterException("Gaussian variance must be positive");
        m = mean;
        s = Math.sqrt(variance);
    }

    /**
       Provides the next replicate.
    */
    public double next()
    {
        return m + s*getMethod().apply();
    }

    /**
       Returns true iff the given point is contained in the appropriate region.
       See Ripley's Stochastic Simulation, page 82.
    */
    public boolean containsInRatioRegion(double x, double y)
    {
        double b = y/x*y/x/4;
                
        if (b < 1-x)
            return true;
        else if (b > C2/x - C3)
            return false;
        else if (b > -Math.log(x))
            return false;
        else
            return true;
    }

    private static final double C1 = Math.sqrt(2/Math.E);
    private static final double C2 = Math.exp(-1.35);
    private static final double C3 = 1+Math.log(C2);
    private double m = 0;
    private double s = 1;
}
