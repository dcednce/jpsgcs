package jpsgcs.random;

/**
   This class provides simulation from the Gamma distribution.
   The methods used are Ahrens Dieter when the shape parameter 
   is less than or equal to 1, and Cheng Feast when it is
   greater than 1.
   See Ripley's Stochastic Simulation page 88.
*/
public class GammaRV extends RandomVariable
{
    static { className = "GammaRV"; }

    /**
       Creates a new Gamma random variable with shape parameter 1
       and rate parameter 1.
    */
    public GammaRV()
    {
        this(1.0);
    }

    /**
       Creates a new Gamma random variable with given shape parameter
       and rate parameter 1.
    */
    public GammaRV(double shape)
    {
        this(shape, 1);
    }

    /**
       Creates a new Gamma random variable with the given shape and
       rate parameters.
    */
    public GammaRV(double shape, double rate)
    {
        set(shape,rate);
    }

    /**
       Set the shape and rate parameters of the distribution.
    */
    public void set(double shape, double rate)
    {
        if (shape <= 0)
            throw new ParameterException("Gamma shape parameter must be positive");
        if (rate <= 0)
            throw new ParameterException("Gamma rate parameter must be positive");
        a = shape;
        b = 1.0/rate;

        if (a <= 1.0)
            setMethod(new AhrensDieter(a));
        else
            setMethod(new ChengFeast(a));
    }
                
    /**
       Provides the next replicate.
    */
    public double next()
    {
        return getMethod().apply()*b;
    }

    private double a = 1;
    private double b = 1;
}
