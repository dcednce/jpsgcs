package jpsgcs.random;

/**
   This class creates a mixture of general exponential random variables.
   Each random variable is chosen with probability proportional to the 
   area under the curve.
*/
public class ExponentialMixture extends Mixture implements Envelope
{
    public ExponentialMixture(GeneralExponentialRV[] vars)
    {
        super(areasUnder(vars),vars);
    }

    /**
       Sets the mixture of general exponentials.
    */
    public void set(GeneralExponentialRV[] vars)
    {
        super.set(areasUnder(vars),vars);
    } 

    /**
       Return the next generated value and the value of the density corresponding.
    */
    public Point nextPoint()
    {
        return ((GeneralExponentialRV)X[A.applyI()]).nextPoint();
    }

    /**
       Returns the array of areas under the component exponentials.
    */
    private static double[] areasUnder(GeneralExponentialRV[] vars)
    {
        double[] p = new double[vars.length];
        for (int i=0; i<vars.length; i++)
            p[i] = vars[i].areaUnder();
        return p;
    }
}
