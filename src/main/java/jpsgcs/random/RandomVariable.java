package jpsgcs.random;

/**
   The class from which all random variables descend.
   By definition a random variable is a real number which
   is the result of some random process.
*/
abstract public class RandomVariable extends Random
{
    static String className = "RandomVariable";

    /**
       Return the next replicate of this random variable.
       By default this is done by delegating to a standard 
       generating method.
    */
    public double next()
    {
        return m.apply();
    }

    /**
       Set the method used by this random variable to generate
       the next instance.
    */
    public final void setMethod(GenerationMethod g)
    {
        m = g;
    }

    //-
    /** 
        Get the method used to generate the next instance.
    */
    public final GenerationMethod getMethod()
    {
        return m;
    }
        
    private GenerationMethod m = null;
    //:
        
    /**
       This main method can be used by any subclasses that overwrite
       the "className" string to be their own class name, and provide
       a default constructor.
       As a quick check on the programs the mean and variance of 1000
       replicates from the distribution is printed.
    */
 
    public static void main(String[] args)
    {
        //.
        try
            {
                String s = "jpsgcs.random."+className;
                System.out.println("Default Main Method: "+s);
                RandomVariable X = (RandomVariable)(Class.forName(s).newInstance()); 

                double m = 0;
                double t = 0;
                int n = 1000;
                if (args.length > 0)
                    n = Integer.parseInt(args[0]);
                for (int i=0; i<n; i++)
                    {
                        double x = X.next();
                        m += x;
                        t += x*x;
                    }

                System.out.println("Number\t\t=\t"+n);
                System.out.println("Mean\t\t= \t"+m/n);
                System.out.println("Variance\t=\t"+(t-m*m/n)/n);
            }
        catch (Exception e)
            {
                System.out.println("Caught in RandomVariable:main()");
                e.printStackTrace();
            }
        //:
    }
}
