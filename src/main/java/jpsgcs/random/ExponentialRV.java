package jpsgcs.random;

/**
   This class provides variates from the exponential random variable
   with specified rate.
   The method used is inversion.
*/
public class ExponentialRV extends RandomVariable implements InverseDistribution
{
    static { className = "ExponentialRV"; }

    /**
       Create an Exponential random variable with rate 1.
    */
    public ExponentialRV()
    {
        this(1.0);
    }

    /**
       Create an Exponential random variable with the given rate.
       Rate is 1/mean.
    */
    public ExponentialRV(double rate)
    {
        set(rate);
        setMethod(new Inversion(this));
    }

    /**
       Resets the rate parameter of this random variable.
    */
    public void set(double rate)
    {
        if (rate <= 0)
            throw new ParameterException("Exponential paramter must be positive");
        l = rate;
    }

    /**
       Provides the inverse of the distribution function as required by the inversion method.
    */
    public double inverseDistribution(double u)
    {
        return -Math.log(u)/l;
    }

    private double l = 1.0;
}
