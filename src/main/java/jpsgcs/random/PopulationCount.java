package jpsgcs.random;

/**
   This counts the number 
   of births and deaths in a birth and death process.
*/
public class PopulationCount extends Count
{
    /**
       Counts the number of births.
    */
    public int births = 0;
    /**
       Counts the the number of deaths.
    */
    public int deaths = 0;

    /**
       Creates a new counter with the given time, number of births 
       and number of deaths.
    */
    public PopulationCount(double t, int b, int d)
    {
        super(t,b+d);
        births = b;
        deaths = d;
    }

    /**
       Returns a string representation of the state.
    */
    public String toString()
    {
        return "{"+time+","+count+","+births+","+deaths+"}";
    }
}
