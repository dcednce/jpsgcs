package jpsgcs.random;

/** 
    This is the abstract superclass for processes that generate 
    random objects, rather than random variables.
*/
abstract public class RandomObject extends Random
{
    /**
       Return the next object generated.
    */
    abstract public Object next();
}
