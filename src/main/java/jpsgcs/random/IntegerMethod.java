package jpsgcs.random;

/**
   This interface defines which generation methods can return
   integer values.
*/
public interface IntegerMethod
{
    /**
       Returns the result of applying the method as an integer.
    */
    public int applyI();
}
