package jpsgcs.random;

/**
   This is a generation method for use specifically 
   for the Gamma distribution with shape parameter in (0,1)
   and with rate parameter 1.
*/
public class AhrensDieter extends GenerationMethod
{
    /**
       Creates a new Ahrens-Dieter scheme.
    */
    public AhrensDieter(double alpha)
    {
        a = alpha;
    }

    /**
       Applies the method and returns the result.
    */
    public double apply()
    {
        while(true)
            {
                double u = U();
        
                if (u <= e/(a+e))
                    {
                        double x = Math.pow(((a+e)*u/e),1/a);
                        if (U() <= Math.exp(-x))
                            return x;
                    }
                else
                    {
                        double x = -Math.log((a+e)*(1-u)/a/e);
                        if (U() <= Math.pow(x,a-1))
                            return x;
                    }
            }
    }

    private static final double e = Math.E;
    private double a = 0;
}
