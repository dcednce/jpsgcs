package jpsgcs.random;

/**
   This is the base class from which stochastic processes are
   derived.
*/
abstract public class StochasticProcess extends Random
{
    static String className = "StochasticProcess";

    /**
       Returns the current state of the process.
    */
    abstract public StochasticState getState();
    /**
       Sets the state of the process.
    */
    abstract public void setState(StochasticState s);
    /**
       Advances the process the given time.
    */
    abstract public void advance(double time);

    /*
      Avances the process to the next event.
    */
    //abstract public void next();

    public static void main(String[] args)
    {
        try
            {
                String s = "jpsgcs.random."+className;
                System.out.println("\nDefault Main Method: "+s);
                StochasticProcess S = (StochasticProcess)(Class.forName(s).newInstance());
                for (int i=0; i<10; i++)
                    {
                        S.advance(10.0);
                        System.out.println(S.getState());
                    }
            }
        catch (Exception e)
            {
                System.out.println("Caught in StochasticProcess:main()");
                e.printStackTrace();
            }
    }
}
