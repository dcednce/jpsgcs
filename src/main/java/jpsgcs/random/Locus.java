package jpsgcs.random;

/**
   This class represents the state of a stochatic process
   in one dimensional space.
*/
public class Locus extends StochasticState
{
    /**
       The point in space of the state.
    */
    public double x;

    /**
       Creates a new state with the given time and position.
    */
    public Locus(double t, double p)
    {
        super(t);
        x = p;
    }

    /**
       Returns a string representation of the state.
    */
    public String toString()
    {
        return "{"+time+","+x+"}";
    }
}
