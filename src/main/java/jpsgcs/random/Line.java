package jpsgcs.random;

class Line
{
    public Line(Point p, double slope)
    {
        a = slope;
        b = p.y - a*p.x;
    }

    public Line(Point p, Point q)
    {
        a = (q.y - p.y)/(q.x - p.x);
        b = p.y - a*p.x;
    }

    public double a = 0;
    public double b = 0;

    public Point intersection(Line y)
    {
        Line x = this;
        return new Point((y.b-x.b)/(x.a-y.a),(y.b*x.a-x.b*y.a)/(x.a-y.a));
    }
}
