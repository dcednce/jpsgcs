package jpsgcs.random;

import java.util.Vector;

/**
   This class implements exponential switching but with the envelope
   changing and getting tighter as new values of the target density
   give more information about its shape.
*/
public class AdaptiveRejection extends ExponentialSwitching
{
    /**
       Creates a new adaptive rejection scheme.
    */
    public AdaptiveRejection(LogConcave R, double[] a)
    {
        super(R,a);
    }

    /**
       Applies the rejection method and returns the result.
    */
    public double apply()
    {
        Point p = null;
        do 
            {
                p = ((ExponentialMixture)getEnvelope()).nextPoint();
                        
                if (stillAdapting)
                    {
                        v.addElement(new Point(p.x,((LogConcave)getTarget()).logDensityFunction(p.x)));
                        if (++increase >= jump)
                            {
                                updateEnvelope();
                                increase = 0;
                            }
                    }

                ntried++;
            }
        while (U() > getTarget().densityFunction(p.x)/p.y);

        naccepted++;
        return p.x;
    }

    /**
       Uses the new points collected to update the exponential mixture
       envelope.
    */
    private void updateEnvelope()
    {
        Point[] p = new Point[v.size()];
        p = (Point[])v.toArray(p);

        for (int i=1; i<p.length; i++)
            for (int j=i; j>0; j--)
                if (p[j].x < p[j-1].x)
                    {
                        Point t = p[j];
                        p[j] = p[j-1];
                        p[j-1] = t;
                    }

        setEnvelope(makeEnvelope((LogConcave)getTarget(),p));
        stillAdapting = ntried < 20 ||  naccepted <= thresh * ntried;
    }

    private boolean stillAdapting = true;
    private double thresh = 0.95;
    private int jump = 10;
    private int increase = 0;
    private int naccepted = 0;
    private int ntried = 0;
}

