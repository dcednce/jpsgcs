package jpsgcs.random;

/**
   This degenerate random variable always yeilds the same fixed
   value. This is just to allow us to use a constant when a random
   variable is called for.
*/
public class DegenerateRV extends RandomVariable
{
    static { className = "DegenerateRV"; }

    /**
       Default constructor sets the return value to zero.
    */
    public DegenerateRV()
    {
        this(0);
    }
        
    /**
       Creates a new degenerate random variable that always return the
       specified value.
    */
    public DegenerateRV(double d)
    {
        set(d);
    }

    /**
       Sets the return value.
    */
    public void set(double d)
    {
        x = d;
    }

    /**
       Returns the next value.
    */
    public double next()
    {
        return x;
    }

    private double x = 0;
}
