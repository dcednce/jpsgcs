package jpsgcs.random;

/**
   This interface defines what it is necessary for a 
   distribution to implement in order to be used 
   as an envelope in the rejection method.
*/
public interface Envelope
{
    /**
       Returns a point where the x coodinate is the next 
       simulated value and the y coordinate is the density function
       at that value.
    */
    public Point nextPoint();
}
