package jpsgcs.random;

/**
   This class implements the ANSII C rand  generator.
   There are lots better.
*/
public final class Rand extends CongruentialGenerator
{
    static { className = "Rand"; }

    /**
       Creates a new rand generator.
    */
    public Rand()
    {
        super(1103515245,12345,0x7FFFFFFF);
    }
}
