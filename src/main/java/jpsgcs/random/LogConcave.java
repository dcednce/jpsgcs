package jpsgcs.random;

/**
   This interface defines what is needed to in order to use
   Exponential Switching as a method of generation.
   Any random variable that implements this inteface will be
   assumed to have a density function the log of which 
   is a concave function throughout its range.
*/
public interface LogConcave extends DensityFunction
{
    /**
       Return the log of a function proportional to the density function.
    */
    public double logDensityFunction(double x);
    /**
       Return the lower bound of the range of the random variable.
       May be negative infinity.
    */
    public double lowerBound();
    /**
       Return the lower bound of the range of the random variable.
       May be positive infinity.
    */
    public double upperBound();
}
