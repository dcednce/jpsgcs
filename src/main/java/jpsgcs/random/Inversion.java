package jpsgcs.random;

/**
   This class implements the general inversion method.
   The random variable using it has to provide the inverse
   distribution function.
*/
public class Inversion extends GenerationMethod implements IntegerMethod
{
    /** 
        Creates a new inversion scheme.
    */
    public Inversion(InverseDistribution i)
    {
        x = i;
    }

    /**
       Applies the method and returns the result.
    */
    public double apply()
    {
        return x.inverseDistribution(U());
    }

    /**
       Applies the method and returns the result as an integer.
    */
    public int applyI()
    {
        return (int) (apply() + 0.5);
    }

    private InverseDistribution x = null;
}
