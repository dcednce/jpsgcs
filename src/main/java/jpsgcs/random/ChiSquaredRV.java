package jpsgcs.random;

/**
   This class provides replicates from the Chi Squared distribution.
   It is implemented as a special case of the Gamma.
*/
public class ChiSquaredRV extends GammaRV
{
    static { className = "ChiSquaredRV"; }

    /**
       Creates a new Chi Squared random variable with 1 degree of freedom.
    */
    public ChiSquaredRV()
    {
        this(1);
    }

    /**
       Creates a new Chi Squared random variable with the specified
       degrees of freedom.
    */
    public ChiSquaredRV(double df)
    {
        super(df*0.5,0.5);
    }
}
