package jpsgcs.random;

/**
   This class provides a link between this package and the
   standard java random number generator. Just a wrapper.
*/
public class BuiltInGenerator extends PseudoRandomEngine
{
    static { className = "BuiltInGenerator"; }

    /**
       Constructs a new generator that delegates to the java.util.Random class.
    */
 
    public BuiltInGenerator()
    {
        e = new java.util.Random();
    }

    /**
       Sets the seed for the generator.
    */
    public final synchronized void seed(long s)
    {
        e.setSeed(s);
    }

    /**
       Returns the next pseudo random variate.
    */
    public final synchronized double next()
    {
        return e.nextDouble();
    }

    private java.util.Random e = null;
}
