package jpsgcs.random;

/**
   This class combines random variables into mixtures.
   A distribution is chosen with a certain probability 
   and a relisation is generated from it. Successive calls
   choose distributions independently.
   Alias rejection is used to choose the random variable.
*/
public class Mixture extends RandomVariable
{
    /**
       Generate a new mixture of the given random variables chosen
       with probabilities proportional to the given double values.
    */
    public Mixture(double[] props, RandomVariable[] vars)
    {
        set(props,vars);
    }

    /**
       Set the mixture probabilities and the random variables.
    */
    public void set(double[] props, RandomVariable[] vars)
    {
        if (props.length != vars.length)
            throw new ParameterException("Mixture proportion and random variable array lengths must match.");
        if (props.length <1)
            throw new ParameterException("Mixtures must have at least one element");
        for (int i=0; i<props.length; i++)
            if (props[i] < 0)
                throw new ParameterException("Mixture proportions must be positive");
        X = vars;
        A = new AliasRejection(props);
    }

    /**
       Choose a random variable at random and return the next realisation
       from it.
    */
    public double next()
    {
        last = A.applyI();
        return X[last].next();
    }

    public int lastI()
    {
        return last;
    }

    public double next(int i)
    {
        return X[i].next();
    } 

    protected RandomVariable[] X = null;
    protected AliasRejection A = null;

    private int last = 0;
}
