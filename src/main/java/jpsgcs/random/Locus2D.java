package jpsgcs.random;

/**
   This class represents the state of two dimensional
   stochastic process.
*/
public class Locus2D extends Locus
{
    /**
       The y coordinate in space of the state.
    */
    public double y = 0;

    /**
       Creates a new stochastic state with time and location set
       by the given parameters.
    */
    public Locus2D(double time, double x, double y)
    {
        super(time,x);
        this.y = y;
    }

    /**
       Returns a string representation of the state.
    */
    public String toString()
    {
        return "{"+time+","+x+","+y+"}";
    }
}
