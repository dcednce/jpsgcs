package jpsgcs.random;

/**
   This interface defines what is needed of a random variable 
   in order to use the rejection method.
*/
public interface DensityFunction
{
    /**
       Returns a value proportional to the density funtion for 
       the given value in the range.
    */
    public double densityFunction(double x);
}
