package jpsgcs.random;

/**
   This class represents the state of a renewal process.
*/
public class RenewalCount extends Count
{
    /**
       This parameter is the time of the last event.
    */
    public double last = 0;

    /**
       Creates a new state with the given time and count.
    */
    public RenewalCount(double t, double l, int c)
    {
        super(t,c);
        last = l;
    }

    /**
       Returns a string representation of the process.
    */
    public String toString()
    {
        return "{"+time+","+last+","+count+"}";
    }
}
