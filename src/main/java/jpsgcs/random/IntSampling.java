package jpsgcs.random;

/**
   Abstract class from which sampling with and without replacement
   are derived.
*/
abstract public class IntSampling extends IntegerValuedRV
{
    /**
       Returns an array of the next integers sampled.
    */
    abstract public int[] nextI(int n);

    /**
       Restarts the sampling process.
    */
    abstract public void restart();

    /**
       Test main.
    */
    public static void main(String[] args)
    {
        try
            {
                String s = "jpsgcs.random."+className;
                System.out.println("Default Main Method: "+s);
                IntSampling X = (IntSampling)(Class.forName(s).newInstance()); 

                for (int i=0; i<10; i++)
                    {
                        X.restart();
                        int[] y = X.nextI(12);
                        for (int j=0; j<y.length; j++)
                            System.out.print(" "+y[j]);
                        System.out.println();
                    }
                for (int i=0; i<10; i++)
                    {
                        X.restart();
                        int[] y = X.nextI(5);
                        for (int j=0; j<y.length; j++)
                            System.out.print(" "+y[j]);
                        System.out.println();
                    }
            }
        catch (Exception e)
            {
                System.out.println("Caught in IntSampling:main()");
                e.printStackTrace();
            }
    }
}
