package jpsgcs.random;

/**
   A runtime exception that is thrown if parameter values given
   to a random variable are not appropriate.
   All random variable classes should check the parameters in
   the "set()" method.
*/
public class ParameterException extends RuntimeException
{
    /**
       Constructs an exception with default message string.
    */
    public ParameterException()
    {
        this("Random variable parameters not acceptable.");
    }

    /**
       Construct an exception with the given message string.
    */
    public ParameterException(String s)
    {
        super(s);
    } 
}
