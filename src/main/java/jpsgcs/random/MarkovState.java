package jpsgcs.random;

/**
   This class represents the states of a Markov chain.
*/
public class MarkovState extends Count
{
    /**
       The current state of the chain.
    */
    public int state = 0;

    /**
       Create a new Markov state which is in state s after
       c transitions.
    */
    public MarkovState(int c, int s)
    {
        super(c,c);
        state = s;
    }

    /**
       Returns a string representation of the state.
    */
    public String toString()
    {
        return "{"+count+","+state+"}";
    }
}
