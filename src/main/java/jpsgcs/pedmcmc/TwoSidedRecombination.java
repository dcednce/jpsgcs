
package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;
import jpsgcs.pedapps.GeneticMap;

public class TwoSidedRecombination extends GenepiFunction
{
	public TwoSidedRecombination(Inheritance x, Inheritance y, Inheritance z, double lt, double rt)
	{
		v = new Variable[3];
		v[0] = x;
		v[1] = y;
		v[2] = z;

		fix(lt,rt);
	}

	public void fix(double lt, double rt)
	{
		left = lt;
		right = rt;
		all = GeneticMap.sumTheta(lt,rt);
	}

	public double getValue()
	{
		double x = v[0].getState() == v[1].getState() ? 1-left : left;
		x *= v[1].getState() == v[2].getState() ? 1-right : right;
		x /= v[0].getState() == v[2].getState() ? 1-all : all;

		return x;
	}

	public String toString()
	{
		return "REC"+super.toString();
	}

// Private data.

	private double left = 0;
	private double right = 0;
	private double all = 0;
}
