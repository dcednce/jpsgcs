package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class AlleleErrorPenetrance extends AllelePenetrance
{
	public AlleleErrorPenetrance(Allele paternal, Allele maternal, Error err)
	{
		this(paternal,maternal,err,null);
	}

	public AlleleErrorPenetrance(Allele paternal, Allele maternal, Error err, double[][] penet)
	{
		v = new Variable[3];
		v[0] = paternal;
		v[1] = maternal;
		v[2] = err;
		fix(penet);
	}

	public double getValue()
	{
		return v[2].getState() == 1 ? q : p[v[0].getState()][v[1].getState()] ;
	}

	public String toString()
	{
		return "EPEN"+super.toString();
	}

	public void fix(double[][] penet)
	{
		p = penet;
		if (p != null)
			q = 1.0 / (p.length*p.length);
	}

// Private data.

	private double q = 0;
}
