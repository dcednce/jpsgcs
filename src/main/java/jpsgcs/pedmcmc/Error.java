package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class Error extends Variable
{
	public Error()
	{
		super(2);
	}

	public String toString()
	{
		return "ERR("+super.toString()+")";
	}
}
