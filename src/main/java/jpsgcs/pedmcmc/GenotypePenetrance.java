package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class GenotypePenetrance extends GenepiFunction
{
	protected GenotypePenetrance()
	{
	}

	public GenotypePenetrance(Genotype g)
	{
		this(g,null);
	}

	public GenotypePenetrance(Genotype g, double[][] penet)
	{
		v = new Variable[1];
		v[0] = g;
		fix(penet);
	}

	public double getValue()
	{
		return p == null ? 1 :  p[((Genotype)v[0]).pat()][((Genotype)v[0]).mat()];
	}

	public String toString()
	{
		return "GPEN"+super.toString();
	}

	public void fix(double[][] penet)
	{
		p = penet;
	}

// Private data.

	protected double[][] p = null;
}
