package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

/**
 A variable representing the ordered genotype for an
 individual as some genetic locus.
*/
public class Genotype extends Variable
{
	public Genotype(int n)
	{
		super(n*n);
		na = n;
	}

/**
 Returns the state of the paternal allele for this genotype.
*/
	public int pat()
	{
		return getState()/na;
	}

/**
 Return the state of the maternal allele for this genotype.
*/
	public int mat()
	{
		return getState()%na;
	}

	public String toString()
	{
		return "G("+super.toString()+")";
	}

// Private data.

	protected int na = 0;
}
