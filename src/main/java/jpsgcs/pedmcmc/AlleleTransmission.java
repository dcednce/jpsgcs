package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;
import jpsgcs.markov.MultiVariable;
import jpsgcs.markov.Iterable;

public class AlleleTransmission extends GenepiFunction implements Iterable
{
	public AlleleTransmission(Allele pat, Allele mat, Inheritance inh, Allele mine)
	{
		Variable[] u = {pat, mat, inh, mine};
		v = u;
		
		Variable[] w = {pat, mat, inh};
		m = new MultiVariable(w);
	}

	public void init()
	{
		m.init();
	}

	public boolean next()
	{
		while(true)
		{
			if (!m.next())
				return false;
			
			int s = v[2].getState() == 0 ? v[0].getState() : v[1].getState();

			if (v[3].hasState(s))
			{
				v[3].setState(s);
				return true;
			}
		}
	}

	public int getNStates()
	{
		return m.getNStates();
	}

	public double getValue()
	{
		return v[3].getState() == v[ v[2].getState() ].getState() ? 1 : 0 ;
	}

	public String toString()
	{
		return "AT"+super.toString();
	}

// Private data.

	private MultiVariable m = null;
}
