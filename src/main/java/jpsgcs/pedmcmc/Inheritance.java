package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;
import java.util.Set;
import java.util.LinkedHashSet;

public class Inheritance extends Variable
{
	public Inheritance()
	{
		super(2);
	}

	public void setStates(int[] x)
	{
	}
	
	public void flip()
	{
		state = 1-state;
	}

	public void remember()
	{
		memo = state;
	}

	public void recall()
	{
		state = memo;
	}

	public String toString()
	{
		return "I("+super.toString()+")"+"["+state+"]";
	}

	private int memo = 0;
}
