package jpsgcs.pedmcmc;

import jpsgcs.genio.BasicGeneticData;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.Function;
import jpsgcs.markov.Functions;
import jpsgcs.markov.Variable;
import jpsgcs.markov.FillIn;

import java.util.Random;

public class LocusProduct extends MarkovRandomField
{
	public LocusProduct(BasicGeneticData d, int i)
	{
		this(d,i,true,true);
	}

	public LocusProduct(BasicGeneticData d, int i, boolean withpenetrances, boolean withpriors)
	{
		this(d,i,withpenetrances,withpriors,allocInheritances(d));
	}

	public LocusProduct(BasicGeneticData d, int i, boolean withpenetrances, boolean withpriors, Inheritance[][] inher)
	{
		super();

		inh = inher;

		all = new Allele[d.nIndividuals()][2];
		
		for (int j=0; j<all.length; j++)
		{
			all[j][0] = new Allele(d.nAlleles(i));
			all[j][1] = new Allele(d.nAlleles(i));

			add(all[j][0]);
			add(all[j][1]);
		}

		if (withpenetrances)
		{
			for (int j=0; j<all.length; j++)
			{
				if (d.penetrance(i,j) != null)
				{
					Function f = new AllelePenetrance(all[j][0],all[j][1],d.penetrance(i,j));
					Functions.reduceStates(f);
					add(f);
				}
			}
		}

		for (int j=0; j<inh.length; j++)
		{
			if (inh[j][0] != null)
			{
				Function f = new AlleleTransmission(all[d.pa(j)][0], all[d.pa(j)][1], inh[j][0], all[j][0]);
				if (withpenetrances)
					Functions.reduceStates(f);
				add(f);
			}
			else if (withpriors)
			{
				Function f = new AllelePrior(all[j][0],d.alleleFreqs(i));
				if (withpenetrances)
					Functions.reduceStates(f);
				add(f);
			}
				
			if (inh[j][1] != null)
			{
				Function f = new AlleleTransmission(all[d.ma(j)][0], all[d.ma(j)][1], inh[j][1], all[j][1]);
				if (withpenetrances)
					Functions.reduceStates(f);
				add(f);
			}
			else if (withpriors)
			{
				Function f = new AllelePrior(all[j][1],d.alleleFreqs(i));
				if (withpenetrances)
					Functions.reduceStates(f);
				add(f);
			}
		}
	}
	
	public Inheritance[][] getInheritances()
	{
		return inh;
	}

	public Allele[][] getAlleles()
	{
		return all;
	}

// Private data and methods.

	protected Allele[][] all = null;
	protected Inheritance[][] inh = null;

	protected static Inheritance[][] allocInheritances(BasicGeneticData d)
	{
		Inheritance[][] h = new Inheritance[d.nIndividuals()][2];

		for (int j=0; j<d.nIndividuals(); j++)
		{
			if (d.pa(j) >=0)
				h[j][0] = new Inheritance();
			if (d.ma(j) >=0)
				h[j][1] = new Inheritance();
		}

		return h;
	}

}
