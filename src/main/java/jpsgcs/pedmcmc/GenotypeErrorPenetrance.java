package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class GenotypeErrorPenetrance extends GenotypePenetrance
{
	public GenotypeErrorPenetrance(Genotype g, Error e)
	{
		this(g,e,null);
	}

	public GenotypeErrorPenetrance(Genotype g, Error e, double[][] penet)
	{
		v = new Variable[2];
		v[0] = g;
		v[1] = e;
		fix(penet);
	}

	public double getValue()
	{
		if (p == null)
			return 1;

		if (v[1].getState() == 1)
			return q;

		 return p[((Genotype)v[0]).pat()][((Genotype)v[0]).mat()];
	}

	public String toString()
	{
		return "GEPEN"+super.toString();
	}

	public void fix(double[][] penet)
	{
		p = penet;
		if (p != null)
			q = 1.0 / (p.length * p.length);
	}

// Private data.

	private double q = 0;
}
