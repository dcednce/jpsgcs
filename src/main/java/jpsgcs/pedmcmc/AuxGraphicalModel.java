package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;
import jpsgcs.markov.Function;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.MarkovRandomField;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Random;

public class AuxGraphicalModel extends GraphicalModel
{
	public AuxGraphicalModel(MarkovRandomField m, Random r)
	{
		super(m,r,false);

		Set<AuxVariable> s = new LinkedHashSet<AuxVariable>();
		for (Object x : m.getElements())
			if (x instanceof Variable)
				s.add((AuxVariable)x);
		v = (AuxVariable[]) s.toArray(new AuxVariable[0]);

		Set<AuxLocusFunction> t = new LinkedHashSet<AuxLocusFunction>();
		for (Object x : m.getElements())
			if (x instanceof AuxLocusFunction)
				t.add((AuxLocusFunction)x);
		f = (AuxLocusFunction[]) t.toArray(new AuxLocusFunction[0]);
	}

	public void simulate(boolean usepost)
	{
		for (int i=0; i<v.length; i++)
			v[i].prepare();
		for (int i=0; i<f.length; i++)
			f[i].prepare();
		super.simulate(usepost);
	}

	public void maximize(boolean usepost)
	{
		for (int i=0; i<v.length; i++)
			v[i].prepare();
		for (int i=0; i<f.length; i++)
			f[i].prepare();
		super.maximize(usepost);
	}


// Private data.

	private AuxVariable[] v = null;
	private AuxLocusFunction[] f = null;
}
