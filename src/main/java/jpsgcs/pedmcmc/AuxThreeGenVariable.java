package jpsgcs.pedmcmc;

public class AuxThreeGenVariable extends AuxVariable
{
	public AuxThreeGenVariable(Inheritance[] hp, Inheritance[] hm, Inheritance[] lp, Inheritance[] lm)
	{
		super(2);

		hip = hp;
		him = hm;
		lop = lp;
		lom = lm;

		p = new Inheritance[hip.length + lop.length];
		m = new Inheritance[him.length + lom.length];

		for (int i=0, j=0; i<hip.length; i++, j++)
		{
			p[j] = hip[i];
			m[j] = him[i];
		}

		for (int i=0, j=hip.length; i < lop.length; i++, j++)
			p[j] = lop[i];

		for (int i=0, j=him.length; i < lom.length; i++, j++)
			m[j] = lom[i];
	}

	public void setState(int s)
	{
		super.setState(s);

		for (Inheritance i : p)
			i.recall();
		for (Inheritance i : m)
			i.recall();

		switch(s)
		{
		case 0:	break;

		case 1:	for (Inheritance i : lop)
				i.flip();
			for (Inheritance i : lom)
				i.flip();

			for (int i=0; i<hip.length; i++)
			{
				int t = hip[i].getState();
				hip[i].setState(him[i].getState());
				him[i].setState(t);
			}

			break;
		}
	}

	public boolean next()
	{
		if (!super.next())
			return false;
		setState(getState());
		return true;
	}

// Private data.

	private Inheritance[] hip = null;
	private Inheritance[] him = null;
	private Inheritance[] lop = null;
	private Inheritance[] lom = null;
}
