package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;
import jpsgcs.markov.MultiVariable;
import jpsgcs.markov.Iterable;
import jpsgcs.markov.Trivial;

/** This only iterates over possible values. It doesn't give proper probabilities.
 It needs to be used with an AlleleTransmission, or similar.
*/
public class AlleleSegregation extends GenepiFunction implements Iterable, Trivial 
{
	public AlleleSegregation(Allele pat, Allele mat, Allele mine)
	{
		Variable[] u = {pat, mat, mine};
		v = u;
		
		Variable[] w = {pat, mat};
		m = new MultiVariable(w);
	}

	public void init()
	{
		m.init();
		gotone = false;
	}

	public boolean next()
	{
		while (true)
		{
			if (!gotone)
			{
				if (!m.next())
					return false;

				int s = v[0].getState();

				if (s != v[1].getState())
					gotone = true;
				
				if (v[2].hasState(s))
				{
					v[2].setState(s);
					return true;
				}
			}
			else
			{
				gotone = false;

				int s = v[1].getState();
				if (v[2].hasState(s))
				{
					v[2].setState(s);
					return true;
				}
			}
		}
	}

	public int getNStates()
	{
		return 2*m.getNStates();
	}

	public double getValue()
	{
		return 1;

/*
If this was not just an iterator over possible values, it would need
to return the following.
		int count = 0;
		int s = v[2].getState();
		if (s == v[0].getState())
			count++;
		if (s == v[1].getState())
			count++;
		return count;
*/
	}

	public String toString()
	{
		return "AS"+super.toString();
	}

// Private data.

	private MultiVariable m = null;
	private boolean gotone = false;
}
