package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;
import jpsgcs.markov.Function;
import java.util.Set;
import java.util.LinkedHashSet;

abstract public class GenepiFunction extends Function
{
	public Variable[] getVariables()
	{
		return v;
	}

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		s.append("(");
		for (Variable u : v)
			s.append(u+",");
		if (v.length > 0)
			s.deleteCharAt(s.length()-1);
		s.append(")");
		return s.toString();
	}

	protected Variable[] v = null;
}
