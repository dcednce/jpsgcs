package jpsgcs.pedmcmc;

public class AuxOneGenVariable extends AuxVariable
{
	public AuxOneGenVariable(Inheritance pat, Inheritance mat)
	{
		this(array(pat),array(mat));
	}

	public AuxOneGenVariable(Inheritance[] pat, Inheritance[] mat)
	{
		super(4);
		p = pat;
		m = mat;
	}

	public void setState(int s)
	{
		super.setState(s);

		for (Inheritance i : p)
			i.recall();
		for (Inheritance i : m)
			i.recall();

		switch(s)
		{
		case 0:	break;

		case 1:	for (Inheritance i : p)
				i.flip();
			break;

		case 2:	for (Inheritance i : m)
				i.flip();
			break;

		case 3:	for (Inheritance i : p)
				i.flip();
			for (Inheritance i : m)
				i.flip();
			break;
		}
	}

	public boolean next()
	{
		if (!super.next())
			return false;
		setState(getState());
		return true;
	}
}
