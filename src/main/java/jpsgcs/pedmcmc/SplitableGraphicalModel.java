package jpsgcs.pedmcmc;

import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.Variable;
import jpsgcs.markov.Potential;
import jpsgcs.markov.Table;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Random;

public class SplitableGraphicalModel extends GraphicalModel
{
	public SplitableGraphicalModel(MarkovRandomField p, Random r, boolean compit)
	{
		super(p,r,compit);
	}

	public Potential[][] splitByOutvol(Set<Variable> s)
	{
		Set<Potential> a = new LinkedHashSet<Potential>();
		Set<Potential> b = new LinkedHashSet<Potential>();

		for (int i=0; i<c.length; i++)
		{
			if (b.contains(c[i]))
				continue;

			Set<Variable> out = c[i].outvol();
			out.retainAll(s);

			if (out.isEmpty())
			{
				a.add(c[i]);
			}
			else
			{
				for (Potential p = c[i]; p != null && !b.contains(p); p = p.next)
					b.add(p);
			}
		}

		Potential[][] res = new Potential[2][];
		res[0] = new Potential[a.size()];
		res[1] = new Potential[b.size()];
		for (int i=0, j=0, k=0; i<c.length; i++)
		{
			if (a.contains(c[i]))
				res[0][j++] = c[i];
			else if (b.contains(c[i]))
				res[1][k++] = c[i];
		}
		
		return res;
	}

	public double preLogPeel(Potential[] p, boolean usepost)
	{
		double result = 0;
		
		for (int i=0; i<p.length; i++)
		{
			p[i].output().allocate();
			p[i].allocate();
			p[i].collect(usepost);
			p[i].free();
			for (Table t : p[i].inputs())
				t.free();

			double x = p[i].output().sum();
			if (x > 0)
			{
				p[i].output().scale(1.0/x);
				result += Math.log(x);
			}
			else
			{
				p[i].output().free();
				return Double.NEGATIVE_INFINITY;
			}
		}

		return result;
	}

	public double postLogPeel(Potential[] p, boolean usepost)
	{
		double result = 0;
		
		for (int i=0; i<p.length; i++)
		{
			p[i].output().allocate();
			p[i].allocate();
			p[i].collect(usepost);
			p[i].free();
			
			double x = p[i].output().sum();
			if (x > 0)
			{
				p[i].output().scale(1.0/x);
				result += Math.log(x);
			}
			else
			{
				p[i].output().free();
				return Double.NEGATIVE_INFINITY;
			}
		}

		return result;
	}

	public void cleanUpOutputs()
	{
		for (int i=0; i<c.length; i++)
			c[i].output().free();
	}
}
