package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class GenotypePrior extends GenepiFunction 
{
	public GenotypePrior(Genotype g, double[] freqs)
	{
		v = new Variable[1];
		v[0] = g;
		f = freqs;
	}

	public double getValue()
	{
		return f[((Genotype)v[0]).pat()] * f[((Genotype)v[0]).mat()];
	}

	public String toString()
	{
		return "GPRI"+super.toString();
	}

// Private data.

	private double[] f = null;
}
