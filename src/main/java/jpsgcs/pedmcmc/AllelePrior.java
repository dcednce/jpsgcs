package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class AllelePrior extends GenepiFunction 
{
	public AllelePrior(Variable al, double[] allelefreqs)
	{
		v = new Variable[1];
		v[0] = al;
		f = allelefreqs;
	}

	public double getValue()
	{
		return f[v[0].getState()];
	}

	public String toString()
	{
		return "PRI"+super.toString();
	}

// Private data.

	private double[] f = null;
}
