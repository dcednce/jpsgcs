package jpsgcs.pedmcmc;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.markov.Variable;
import jpsgcs.markov.Function;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.MarkovRandomField;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Random;

public class TrueMultiLocusSampler extends LocusSampler
{
	protected TrueMultiLocusSampler(Random r)
	{
		super(r);
	}

	public TrueMultiLocusSampler(GeneticDataSource d, boolean linkfirst, Random r)
	{
		this(d,linkfirst,false,r);
	}

	public TrueMultiLocusSampler(GeneticDataSource d, boolean linkfirst, boolean usepotentials, Random r)
	{
		this(r);

		LocusProduct[] p = new LocusProduct[d.nLoci()];
		Allele[][][] all = new Allele[p.length][][];
		h = new Inheritance[p.length][][];

		MarkovRandomField prod = new MarkovRandomField();

		for (int i=0; i<p.length; i++)
		{
			p[i] = new LocusProduct(d,i);
			all[i] = p[i].all;
			h[i] = p[i].inh;
			GraphicalModel gm = new GraphicalModel(p[i],random(),false);
			gm.reduceStates();
			for (Function f : p[i].getFunctions())
				prod.add(f);
		}

		for (int j=0; j<h[0].length; j++)
		{
			if (h[0][j][0] != null)
				for (int i = (linkfirst ? 1 : 2); i<p.length; i++)
					prod.add(new Recombination(h[i-1][j][0],h[i][j][0],d.getMaleRecomFrac(i-1,i)));

			if (h[0][j][1] != null)
				for (int i = (linkfirst ? 1 : 2); i<p.length; i++)
					prod.add(new Recombination(h[i-1][j][1],h[i][j][1],d.getFemaleRecomFrac(i-1,i)));
		}

		gg = new GraphicalModel(prod,random(),usepotentials);

		max = false;
	}

	public void initialize()
	{
		gg.collect(true,false);
		gg.drop();
		max = false;
	}

	public void sample(boolean report)
	{
		if (max)
		{
			gg.collect(true,false);
			max = false;
		}

		gg.drop();
		if (report)
			System.err.print("-");
	}

	public void maximize(boolean report)
	{
		if (!max)
		{
			gg.collect(true,true);
			max = true;
		}

		gg.drop();
		if (report)
			System.err.print("-");
	}

// Private data.

	protected GraphicalModel gg = null;
	protected boolean report = false;
	protected boolean max = false;
}
