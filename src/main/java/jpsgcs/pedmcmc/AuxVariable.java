package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

abstract public class AuxVariable extends Variable
{
	public AuxVariable(int n)
	{
		super(n);
	}

	public void prepare()
	{
		for (Inheritance i : p)
			i.remember();
		for (Inheritance i : m)
			i.remember();
	}

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		s.append(super.toString()+":");
		s.append("AV(");
		for (Inheritance i : p)
			s.append(i);
		s.append("+");
		for (Inheritance i : m)
			s.append(i);
		s.append(")");
		return s.toString();
	}

// Private data and methods.

	protected Inheritance[] p = null;
	protected Inheritance[] m = null;

	protected static Inheritance[] array(Inheritance i)
	{
		Inheritance[] j = new Inheritance[1];
		j[0]= i;
		return j;
	}
}
