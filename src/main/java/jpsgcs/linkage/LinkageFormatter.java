package jpsgcs.linkage;

import jpsgcs.util.InputFormatter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class LinkageFormatter extends InputFormatter
{
    public LinkageFormatter() throws IOException {
        this(new BufferedReader(new InputStreamReader(System.in)),"System.in");
    }

    public LinkageFormatter(String name) throws IOException {
        this (new BufferedReader(new FileReader(name)),name);
    }

    public LinkageFormatter(BufferedReader br, String f) throws IOException {
        super(br);
        file = f;
    }

    public void readLine() throws IOException {
        if (!newLine())
            crash("is missing");
    }
    
    public String readString (String name, int def, boolean used, boolean crash) {
    	if (newToken() && nextIsInt())
            return getString();
        else
            return getString();
    }

    public int readInt(String name, int def, boolean used, boolean crash) {
        if (newToken() && nextIsInt())
            return getInt();
		
        if (crash) {
            crash("Can't read integer "+name+" from string \""+getString()+"\".");
        }
        else {
            warn("Can't read integer "+name+
                 " from string \""+getString()+"\"."+
                 "\n\tAssumed to be "+def+"."
                 + (!used ? "\n\tThis parameter is not used by these programs" : "")
                 );
        }
        return def;
    }

    public double readDouble(String name, double def, boolean used, boolean crash) {
        if (newToken() && nextIsDouble())
            return getDouble();
		
        if (crash) {
            crash("Can't read double "+name+" from string \""+getString()+"\".");
        }
        else {
            warn("Can't read double "+name+
                 " from string \""+getString()+"\"."+
                 "\n\tAssumed to be "+def+"."
                 + (!used ? "\n\tThis parameter is not used by these programs" : "")
                 );
        }
        return def;
    }

    public void crash(String s) {
        System.err.println("\n"+file+", line "+lastLine()+", item "+lastToken()+":\n\t"+s);
        System.err.println("\tCan't continue");
        System.exit(1);
    }

    public void warn(String s) {
        System.err.println("\n"+file+", line "+lastLine()+", item "+lastToken()+":\n\t"+s);
    }

    public String fileName() {
        return file;
    }

    private String file = null;
}
