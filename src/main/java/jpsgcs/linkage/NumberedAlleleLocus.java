package jpsgcs.linkage;

import jpsgcs.util.StringFormatter;
import jpsgcs.util.IntArray;
import java.io.IOException;
import java.util.Map;
import java.util.LinkedHashMap;

/**
 This class represents the a locus given
 in the linkage numbered locus format.
*/
public class NumberedAlleleLocus extends LinkageLocus
{
	private static Map<IntArray,double[][]> cache = null;

	public NumberedAlleleLocus()
	{
		type = NUMBERED_ALLELES;
	
		line1comment = "";
		line2comment = "";

		if (cache == null)
			cache = new LinkedHashMap<IntArray,double[][]>();
	}

	public NumberedAlleleLocus(int na)
	{
		this();

		freq = new double[na];
		for (int i=0; i<freq.length; i++)
			freq[i] = 1;
	}

	public LinkagePhenotype makePhenotype(LinkageIndividual a)
	{
		return new NumberedAllelePhenotype(this);
	}

	public void read(LinkageFormatter b) throws IOException
	{
		int na = b.readInt("number of alleles",0,true,true);
		line1comment = b.restOfLine();
		
		freq = new double[na];
		b.readLine();
		for (int i=0; i<freq.length; i++)
		{
			freq[i] = b.readDouble("allele frequency "+i,0,true,true);
			if (freq[i] < 0)
				b.crash("Negative allele frequency "+freq[i]);
		}

		checkAndSetAlleleFrequencies(freq,b);

		line2comment = b.restOfLine();
	}

	public LinkageLocus copy()
	{
		NumberedAlleleLocus l = new NumberedAlleleLocus();

		l.freq = new double[freq.length];
		for (int i=0; i<l.freq.length; i++)
			l.freq[i] = freq[i];

		l.line1comment = line1comment;
		l.line2comment = line2comment;

		return l;
	}

	public int[] downCode(LinkageIndividual[] ind, int i, int hard)
	{
		int[] c = new int[nAlleles()+1];

		for (int j=0; j<ind.length; j++)
		{
			c[ind[j].pheno[i].getAllele(0)+1]++;
			c[ind[j].pheno[i].getAllele(1)+1]++;
		}

		c[0] = 0;
		int k = 0;
		for (int j=1; j<c.length; j++)
			if (c[j] > 0)
				c[j] = ++k;

		if (k == 0)
		{
			for (int j=0; j<c.length; j++)
				c[j] = 1;
			return c;
		}

		if (k == c.length-1)
			return c;

		if (hard == 0 || (hard == 1 && k == 1) )
		{
			++k;
			for (int j=1; j<c.length; j++)
				if (c[j] == 0)
					c[j] = k;
		}

		reCode(c);
		for (int j=0; j<ind.length; j++)
			ind[j].pheno[i].reCode(c);

		return c;
	}

	public void countAlleleFreqs(LinkageIndividual[] ind, int i)
	{
		double[] c = new double[nAlleles()];
		int n = 0;
		int al = -1;

		for (int j=0; j<ind.length; j++)
		{
			al = ind[j].pheno[i].getAllele(0);
			if (al >= 0)
			{
				c[al] += 1;
				n++;
			}
			al = ind[j].pheno[i].getAllele(1);
			if (al >= 0)
			{
				c[al] += 1;
				n++;
			}
		}

		for (int k=0; k<c.length; k++)
			c[k] /= n;

		setAlleleFrequencies(c);
	}

	public void reCode(int[] c)
	{
		int n = 0;
		for (int i=0; i<c.length; i++)
			if (n < c[i]) 
				n = c[i];

		double[] newf = new double[n];
		double tot = 0;
		for (int i=1; i<c.length; i++)
			if (c[i] > 0)
			{
				newf[c[i]-1] += freq[i-1];
				tot += freq[i-1];
			}

		for (int i=0; i<newf.length; i++)
			newf[i] /= tot;
	
		freq = newf;
	}

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		
		s.append(type+" "+freq.length+" "+line1comment+"\n");
		
		for (int i=0; i<freq.length; i++)
		{
			s.append(StringFormatter.format(freq[i],2,6));
			s.append(" ");
		}
		s.append(line2comment+"\n");

		return s.toString();
	}

	public double[][] penetrance(int a0, int a1)
	{
		if (a0 < 0 && a1 < 0)
			return null;

		IntArray a = new IntArray(nAlleles(),a0,a1);

		double[][] x = cache.get(a);

		if (x == null)
		{
			x = makePen(nAlleles(),a0,a1);
			cache.put(a,x);
		}

		return x;
	}

// Private data.

	protected String line2comment;

	public static double[][] makePen(int na, int p, int q)
	{
		double[][] x = new double[na][na];

		if (p < 0)
		{
			if (q < 0)
			{
/*
				for (int i=0; i<x.length; i++)
					for (int j=0; j<x[i].length; j++)
						x[i][j] = 1;
*/
				System.err.println("Can't get here");
				return null;
			}
			else
			{
				for (int i=0; i<x.length; i++)
					x[i][q] = x[q][i] = 1;
			}
		}
		else
		{
			if (q < 0)
				for (int i=0; i<x.length; i++)
					x[i][p] = x[p][i] = 1;
			else
				x[p][q] = x[q][p] = 1;
		}

		return x;
	}
}
