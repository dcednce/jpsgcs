package jpsgcs.linkage;

import jpsgcs.util.StringFormatter;
import java.io.IOException;

public class SexLinkedAffStatLocus extends AffectionStatusLocus implements LinkConstants
{
	public SexLinkedAffStatLocus()
	{
		super();
	}

	public LinkagePhenotype makePhenotype(LinkageIndividual a)
	{
		return new SexLinkedAffStatPhenotype(this,a.sex);
	}

	public void read(LinkageFormatter b) throws IOException
	{
		int na = b.readInt("number of alleles",0,true,true);
		freq = new double[na+1];
		line1comment = b.restOfLine();

		freq[0] = 1;

		b.readLine();
		for (int i=1; i<freq.length; i++)
		{
			freq[i] = b.readDouble("frequency "+(i+1),0,true,true);
			if (freq[i] < 0)
				b.crash("Negative allele frequency "+freq[i]);
		}

		checkAndSetAlleleFrequencies(freq,b);

		line2comment = b.restOfLine();

		b.readLine();

		nmliab = b.readInt("number of male liability classes",0,true,true);
		if (nmliab < 1)
			b.crash("Number of male liability classes must be positive "+nmliab);

		nfliab = b.readInt("number of female liability classes",0,true,true);
		if (nfliab < 1)
			b.crash("Number of female liability classes must be positive "+nfliab);

		if (nfliab == 1 && nmliab == 1)
			b.warn("Note that there is only 1 liabilty class for each sex. " +
				"\n\t There must be no liabilty column in the pedigree data file.");

		if (nfliab == 1 && nmliab > 1)
			b.warn("Note that there is only 1 liabilty class for females, but "+nmliab+
				" for males. \n\t There must be a liabilty column in the pedigree data file for each sex.");
		if (nmliab == 1 && nfliab > 1)
			b.warn("Note that there is only 1 liabilty class for males, but "+nfliab+
				" for females. \n\t There must be a liabilty column in the pedigree data file for each sex.");

		int nliab = nfliab + nmliab;

		liab = new double[nliab][];
		comment = new String[liab.length];
		line3comment = b.restOfLine();
		
		int ng = na*(na+1)/2;
		int i = 0;
		for (; i<nmliab; i++)
		{
			b.readLine();
			liab[i] = new double[na];
			for (int j=0; j<liab[i].length; j++)
				liab[i][j] = b.readDouble("penetrance male "+(i+1)+" "+(j+1),0,true,true);
			comment[i] = b.restOfLine();
		}

		for (; i<nliab; i++)
		{
			b.readLine();
			liab[i] = new double[ng];
			for (int j=0; j<liab[i].length; j++)
				liab[i][j] = b.readDouble("penetrance female "+(i+1)+" "+(j+1),0,true,true);
			comment[i] = b.restOfLine();
		}
	}

	public String toString()
	{
		StringBuffer s = new StringBuffer();

		s.append(type+" "+(freq.length-1)+" "+line1comment+"\n");

		double tot = 0;
		for (int i=1; i<freq.length; i++)
			tot += freq[i];

		for (int i=1; i<freq.length; i++)
			s.append(StringFormatter.format(freq[i]/tot,2,6)+" ");

		s.append(line2comment+"\n");

		s.append(nmliab+" "+nfliab+" "+line3comment+"\n");

		for (int i=0; i<liab.length; i++)
		{
			for (int j=0; j<liab[i].length; j++)
				s.append(StringFormatter.format(liab[i][j],2,6)+" ");
			s.append(comment[i]+"\n");
		}

		return s.toString();
	}

	public LinkageLocus copy()
	{
		SexLinkedAffStatLocus l = new SexLinkedAffStatLocus();

                l.freq = new double[freq.length];
                for (int i=0; i<l.freq.length; i++)
                        l.freq[i] = freq[i];
                l.line1comment = line1comment;

		l.liab = new double[liab.length][];
		for (int i=0; i<l.liab.length; i++)
		{
			l.liab[i] = new double[liab[i].length];
			for (int j=0; j<l.liab[i].length; j++)
				l.liab[i][j] = liab[i][j];
		}
		
		l.comment = new String[comment.length];
		for (int i=0; i<l.comment.length; i++)
			l.comment[i] = comment[i];
		
		l.line2comment = line2comment;
		l.line3comment = line3comment;

		l.nfliab = nfliab;
		l.nmliab = nmliab;

		return l;
	}

	public double[][] penetrance(int status, int liability, int sex)
	{
		int code = status * (nmliab + nfliab) + ( sex == MALE ? liability : nmliab+liability );

		double[][] res = cache.get(code);
		if (res == null)
		{
			res = xyTable(status,liability,sex);
			cache.put(code,res);
		}
	
		return res;
	}

// Private data.

	private int nmliab = 0;
	private int nfliab = 0;

	private double[][] xyTable(int status, int liability, int sex)
	{
		double[][] x = new double[nAlleles()][nAlleles()];
		
		if (sex == MALE)
		{
			if (status == UNKNOWN)
			{
				for (int j=1; j<x[0].length; j++)
					x[0][j] = 1;
			}
			else
			{
				double[] f = liab[liability];
				if (status == AFFECTED)
				{
					for (int j=1; j<x[0].length; j++)
						x[0][j] = f[j-1];
				}
				else
				{
					for (int j=1; j<x[0].length; j++)
						x[0][j] = 1 - f[j-1];
				}
			}
		}
		else
		{
			if (status == UNKNOWN)
			{
				for (int i=1; i<x.length; i++)
					for (int j=1; j<x[i].length; j++)
						x[i][j] = 1;
			}
			else
			{
				double[] f = liab[nmliab+liability];
				if (status == AFFECTED)
				{
					for (int i=1; i<x.length; i++)
						for (int j=i; j<x[i].length; j++)
						{
							int g = allelesToGenotype(i-1,j-1,nAlleles()-1);
							x[i][j] = x[j][i] = f[g];
						}
				}
				else
				{
					for (int i=1; i<x.length; i++)
						for (int j=i; j<x[i].length; j++)
						{
							int g = allelesToGenotype(i-1,j-1,nAlleles()-1);
							x[i][j] = x[j][i] = 1-f[g];
						}
				}
			}
		}

		return x;
	}
}
