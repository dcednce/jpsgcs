package jpsgcs.linkage;

import java.io.IOException;

public class SexLinkedAffStatPhenotype extends AffectionStatusPhenotype implements LinkConstants
{
	public SexLinkedAffStatPhenotype(SexLinkedAffStatLocus l, int s)
	{
		super(l);
		sex = s;
	}

	public void read(LinkageFormatter f)
	{
		int s = f.readInt("disease status",0,true,false);

		switch(s)
		{
		case UNKNOWN:
		case UNAFFECTED:
		case AFFECTED:
			break;
		default:
			f.warn("disease status "+s+" is out of range "+0+" "+2+"\n\tSetting to 0");
			s = 0;
		}

		int l = 1;
		if (nLiab() > 2)
		{
			l = f.readInt("liability class",0,true,false);
			if (s != 0)
			{
				if (l < 1 || l > nLiab())
				{
					f.warn("liability class "+l+" is out of range "+1+" "+nLiab()+
							"\n\tSetting disease status to 0 and libility class to 0");
					s = 0;
					l = 0;
				}
			}
			else
			{
				l = 0;
			}
		}

		status = s;
		liability = l;
	}

	public String toString()
	{
		if (nLiab() > 2)
			return f.format(status,2)+" "+f.format(liability,2);
		else
			return f.format(status,2);
	}

	public LinkagePhenotype nullCopy()
	{
		return new SexLinkedAffStatPhenotype((SexLinkedAffStatLocus)getLocus(),sex);
	}

	public double[][] penetrance()
	{
		return ((SexLinkedAffStatLocus) getLocus()).penetrance(status,liability,sex);
	}

// Private data.

	private int sex = 0;
}
