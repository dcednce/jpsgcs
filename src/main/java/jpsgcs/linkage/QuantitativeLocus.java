package jpsgcs.linkage;

import jpsgcs.util.StringFormatter;
import java.io.IOException;

public class QuantitativeLocus extends LinkageLocus
{
	public QuantitativeLocus()
	{
		type = QUANTITATIVE_VARIABLE;
	}

	public LinkagePhenotype makePhenotype(LinkageIndividual a)
	{
		return new QuantitativePhenotype(this,mean.length);
	}

	public void read(LinkageFormatter b) throws IOException
	{
		int na = b.readInt("number of alleles",0,true,true);

		freq = new double[na];
		line1comment = b.restOfLine();

		b.readLine();
		for (int i=0; i<freq.length; i++)
		{
			freq[i] = b.readDouble("frequency "+(i+1),0,true,true);
			if (freq[i] < 0)
				b.crash("Negative allele frequency "+freq[i]);
		}
		checkAndSetAlleleFrequencies(freq,b);

		line2comment = b.restOfLine();

		b.readLine();
		int nvars = b.readInt("number of quantitative variables",0,true,true);
		line3comment = b.restOfLine();

		int ng = freq.length;
		ng = (ng*(ng+1))/2;

		comment = new String[nvars];
		mean = new double[ng][nvars];
		covar = new double[nvars][nvars];
		
		for (int i=0; i<nvars; i++)
		{
			b.readLine();
			for (int j=0; j<ng; j++)
				mean[j][i] = b.readDouble("genotype mean "+(i+1)+" "+(j+1),0,true,true);
			comment[i] = b.restOfLine();
		}

		b.readLine();
		for (int i=0; i<nvars; i++)
		{
			for(int j=i; j<nvars; j++)
			{
				covar[i][j] = b.readDouble("variance/covariance "+(i+1)+" "+(j+1),0,true,true);
				covar[j][i] = covar[i][j];
			}
		}
		line4comment = b.restOfLine();

		b.readLine();
		mult = b.readDouble("hetrozygous var/covar multiplier",1,true,false);
		line5comment = b.restOfLine();

		makeInverse(b);
	}

	public LinkageLocus copy()
	{
		QuantitativeLocus l = new QuantitativeLocus();

                l.freq = new double[freq.length];
                for (int i=0; i<l.freq.length; i++)
                        l.freq[i] = freq[i];

                l.line1comment = line1comment;

		l.mean = new double[mean.length][mean[0].length];
		for (int i=0; i<l.mean.length; i++)
			for (int j=0; j<l.mean[i].length; j++)
				l.mean[i][j] = mean[i][j];

		l.covar = new double[covar.length][covar[0].length];
		for (int i=0; i<l.covar.length; i++)
			for (int j=0; j<l.covar[i].length; j++)
				l.covar[i][j] = covar[i][j];

		l.inver = new double[inver.length][inver[0].length];
		for (int i=0; i<l.inver.length; i++)
			for (int j=0; j<l.inver[i].length; j++)
				l.inver[i][j] = inver[i][j];

		l.determinant = determinant;
		l.mult = mult;

		l.line2comment = line2comment;
		l.line3comment = line3comment;
		l.line4comment = line4comment;
		l.line5comment = line5comment;
		l.comment = new String[comment.length];
		for (int i=0; i<l.comment.length; i++)
			l.comment[i] = comment[i];

		return l;
	}

	public int[] downCode(LinkageIndividual[] ind, int j, int hard)
	{
		int[] c = new int[nAlleles()];
		for (int i=0; i<c.length; i++)
			c[i] = i;
		return c;
	}

	public void countAlleleFreqs(LinkageIndividual[] ind, int j)
	{
		return;
	}

	public void reCode(int[] c)
	{
		return;
	}

	public String toString()
	{
		StringBuffer s = new StringBuffer();

		s.append(type+" "+freq.length+" "+line1comment+"\n");

		for (int i=0; i<freq.length; i++)
			s.append(StringFormatter.format(freq[i],2,6)+" ");
		s.append(line2comment+"\n");

		s.append(mean[0].length+" "+line3comment+"\n");
		for (int i=0; i<mean[0].length; i++)
		{
			for (int j=0; j<mean.length; j++)
				s.append(StringFormatter.format(mean[j][i],4,6)+" ");
			s.append(comment[i]+"\n");
		}
		
		for (int i=0; i<covar.length; i++)
			for (int j=i; j<covar[i].length; j++)
				s.append(StringFormatter.format(covar[i][j],4,6)+" ");
		s.append(line4comment+"\n");
		s.append(mult+" "+line5comment+"\n");

		return s.toString();
	}

	public double[][] penetrance(double[] v)
	{
		for (int i=0; i<v.length; i++)
			if (Math.abs(v[i]) < 0.000000001)
				return null;

		int nvar = mean[0].length;
		double con = Math.pow(2*Math.PI,nvar/2.0) * Math.sqrt(Math.abs(determinant)) ;

		double[][] q = new double[freq.length][freq.length];
		int k = 0;
		for (int i=0; i<q.length; i++)
			for (int j=i; j<q[i].length; j++)
			{
				double[] mu = mean[k];
				double mul = ( i == j ? 1 : mult );

				double x = 0;
				for (int a=0; a<nvar; a++)
					for (int b=0; b<nvar; b++)
						x += (v[a]-mu[a]) * covar[a][b] * (v[b]-mu[b]);

				q[i][j] =  Math.exp(- x / 2 * mul) / con / Math.pow(mul,nvar/2.0);
				q[j][i] = q[i][j];
				k++;
			}

		double z = q[0][0];
		for (int i=0; i<q.length; i++)
			for (int j=0; j<q[i].length; j++)
				q[i][j] /= z;

		return q;
	}

// Private data and methods.

	private String line2comment = null;
	private String line3comment = null;
	private String line4comment = null;
	private String line5comment = null;
	private String comment[] = null;

	private double[][] mean = null;
	private double[][] covar = null;
	private double[][] inver = null;
	private double determinant = 0;
	private double mult = 0;

	private void makeInverse(LinkageFormatter b)
	{
		double[][] x = new double[covar.length][2*covar.length];
		for (int i=0; i<x.length; i++)
		{
			for (int j=0; j<covar.length; j++)
				x[i][j] = covar[i][j];
			x[i][i+covar.length] = 1;
		}

		for (int i=0; i<x.length; i++)
		{
			if (x[i][i] == 0)
				b.crash("Covariance matrix is not invertible");

			for (int j=0; j<x.length; j++)
				if (j != i)
				{
					double z = x[j][i]/x[i][i];
					for (int k=0; k<x[j].length; k++)
						x[j][k] -= x[i][k]*z;
				}
		}

		determinant = 1;
		for (int i=0; i<x.length; i++)
		{
			double z = x[i][i];
			for (int j=0; j<x[i].length; j++)
				x[i][j] /= z;
			determinant *= z;
		}

		inver = new double[covar.length][covar.length];
		for (int i=0; i<inver.length; i++)
			for (int j=0; j<inver[i].length; j++)
				inver[i][j] = x[i][j+inver.length];
	}
}
