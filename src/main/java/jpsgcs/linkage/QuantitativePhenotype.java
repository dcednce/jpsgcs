package jpsgcs.linkage;

public class QuantitativePhenotype extends LinkagePhenotype
{
	public QuantitativePhenotype(QuantitativeLocus l, int dim)
	{
		setLocus(l);
		v = new double[dim];
		for (int i=0; i<v.length; i++)
			v[i] = 0;
	}

	public void read(LinkageFormatter f)
	{
		for (int i=0; i<v.length; i++)
			v[i] = f.readDouble("quantitative variable",0,true,false);
	}

	public String toString()
	{
		StringBuffer b = new StringBuffer();
		for (int i=0; i<v.length; i++)
			b.append(v[i]+" ");
		if (v.length > 0)
			b.deleteCharAt(b.length()-1);
		return b.toString();
	}

	public LinkagePhenotype nullCopy()
	{
		return new QuantitativePhenotype((QuantitativeLocus)getLocus(),v.length);
	}

	public boolean informative()
	{
		double x = 0;
		for (int i=0; i<v.length; i++)
			x += v[i]*v[i];
		return x > 0.00000001;
	}

	public void setUninformative()
	{
		for (int i=0; i<v.length; i++)
			v[i] = 0;
	}

	public int getAllele(int k)
	{
		return -1;
	}

	public boolean setAlleles(int a1, int a2)
	{
		return false;
	}

	public int getAffectedStatus()
	{
		return -1;
	}

	public int getLiability()
	{
		return -1;
	}

	public void reCode(int[] c)
	{
		return;
	}

	public double[][] penetrance()
	{
		return ((QuantitativeLocus)getLocus()).penetrance(v);
	}

// Private data.

	public double[] v = null;
}
