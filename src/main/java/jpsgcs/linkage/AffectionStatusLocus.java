package jpsgcs.linkage;

import jpsgcs.util.StringFormatter;
import java.io.IOException;
import java.util.Map;
import java.util.LinkedHashMap;

/**
 This class represents a locus as specified in the
 linkage affectation status format.
*/
public class AffectionStatusLocus extends LinkageLocus implements LinkConstants
{
/**
 Creates a new locus with data read from the given input formatter.
*/

	public AffectionStatusLocus()
	{
		type = AFFECTION_STATUS;
		cache = new LinkedHashMap<Integer,double[][]>();
	}

	public LinkagePhenotype makePhenotype(LinkageIndividual a)
	{
		return new AffectionStatusPhenotype(this);
	}

	public void read(LinkageFormatter b) throws IOException
	{
		int na = b.readInt("number of alleles",0,true,true);
		freq = new double[na];
		line1comment = b.restOfLine();

		b.readLine();
		for (int i=0; i<freq.length; i++)
		{
			freq[i] = b.readDouble("frequency "+(i+1),0,true,true);
			if (freq[i] < 0)
				b.crash("Negative allele frequency "+freq[i]);
		}

		checkAndSetAlleleFrequencies(freq,b);

		line2comment = b.restOfLine();

		b.readLine();
		int nliab = b.readInt("number of liability classes",0,true,true);
	
		if (nliab == 1)
			b.warn("Note that there is only 1 liabilty class for this trait. There should be no liabilty column in the pedigree data file");
		if (nliab < 1)
			b.crash("Number of liability classes must be positive "+nliab);

		liab = new double[nliab][];
		comment = new String[liab.length];
		line3comment = b.restOfLine();
		
		int ng = freq.length;
		ng = (ng*(ng+1))/2;
		for (int i=0; i<liab.length; i++)
		{
			b.readLine();
			liab[i] = new double[ng];
			for (int j=0; j<liab[i].length; j++)
				liab[i][j] = b.readDouble("penetrance "+(i+1)+" "+(j+1),0,true,true);
			comment[i] = b.restOfLine();
		}
	}

	public LinkageLocus copy()
	{
		AffectionStatusLocus l = new AffectionStatusLocus();

                l.freq = new double[freq.length];
                for (int i=0; i<l.freq.length; i++)
                        l.freq[i] = freq[i];
                l.line1comment = line1comment;

		l.liab = new double[liab.length][];
		for (int i=0; i<l.liab.length; i++)
		{
			l.liab[i] = new double[liab[i].length];
			for (int j=0; j<l.liab[i].length; j++)
				l.liab[i][j] = liab[i][j];
		}
		
		l.comment = new String[comment.length];
		for (int i=0; i<l.comment.length; i++)
			l.comment[i] = comment[i];
		
		l.line2comment = line2comment;
		l.line3comment = line3comment;

		return l;
	}

	public int[] downCode(LinkageIndividual[] ind, int j, int hard)
	{
		int[] c = new int[nAlleles()];
		for (int i=0; i<c.length; i++)
			c[i] = i;
		return c;
	}

	public void countAlleleFreqs(LinkageIndividual[] ind, int j)
	{
		return;
	}

	public void reCode(int[] c)
	{
		return;
	}

	public String toString()
	{
		StringBuffer s = new StringBuffer();

		s.append(type+" "+freq.length+" "+line1comment+"\n");

		for (int i=0; i<freq.length; i++)
			s.append(StringFormatter.format(freq[i],2,6)+" ");
		s.append(line2comment+"\n");

		s.append(liab.length+" "+line3comment+"\n");

		for (int i=0; i<liab.length; i++)
		{
			for (int j=0; j<liab[i].length; j++)
				s.append(StringFormatter.format(liab[i][j],2,6)+" ");
			s.append(comment[i]+"\n");
		}

		return s.toString();
	}

	public double[][] penetrance(int status, int liability)
	{
		if (status == 0)
			return null;

		int code = (status) * liab.length + liability;

		double[][] res = cache.get(code);

		if (res == null)
		{
			res = affTable(status,liability);
			cache.put(code,res);
		}

		return res;
	}

	public int nLiab()
	{
		return liab.length;
	}
	
// Private data.

	protected double[][] liab = null;
	protected String comment[] = null;
	protected String line2comment = null;
	protected String line3comment = null;
	protected Map<Integer,double[][]> cache = null;

        private double[][] affTable(int stat, int liability)
        {
		double[] f = liab[liability-1];

		int na = nAlleles();

                if (stat == 0)
                        return null;

                double[][] x = new double[na][];
                for (int i=0; i<x.length; i++)
                        x[i] = new double[na];

                switch (stat)
                {
                case 0:
                        for (int i=0; i<x.length; i++)
                                for (int j=0; j<x[i].length; j++)
                                        x[i][j] = 1;
                        break;
                case 1:
                        for (int i=0; i<x.length; i++)
                                for (int j=0; j<x[i].length; j++)
                                {
                                        int k = allelesToGenotype(i,j,na);
                                        x[i][j] = 1-f[k];
                                }
                        break;
                case 2:
                        for (int i=0; i<x.length; i++)
                                for (int j=0; j<x[i].length; j++)
                                {
                                        int k = allelesToGenotype(i,j,na);
                                        x[i][j] = f[k];
                                }
                        break;
                }

                return x;
        }

        protected int allelesToGenotype(int ii, int jj, int n)
        {
                int i = ii;
                int j = jj;
                if (i > j)
                {
                        int k = i;
                        i = j;
                        j = k;
                }

                return ( i * (2*n-1-i) ) / 2 + j;
        }
}
