package jpsgcs.linkage;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.genio.PedigreeData;
import jpsgcs.genio.ParameterData;
import java.io.IOException;
import java.io.PrintStream;

public class Linkage
{
	public static GeneticDataSource read(String par, String ped, boolean premake) throws IOException
	{
		LinkageDataSet p = new LinkageDataSet(par,ped,premake);
		return new LinkageInterface(p);
	}

	public static GeneticDataSource read(String par, String ped) throws IOException
	{
		return read(par,ped,false);
	}

	public static GeneticDataSource read(LinkageFormatter parin, LinkageFormatter pedin) throws IOException
	{
		LinkageDataSet p = new LinkageDataSet(parin,pedin);
		return new LinkageInterface(p);
	}

	public static GeneticDataSource[] readAndSplit(String par, String ped, boolean premake) throws IOException
	{
		LinkageDataSet p = new LinkageDataSet(par,ped,premake);
		LinkageDataSet[] s = p.splitByPedigree(); 
		GeneticDataSource[] g = new GeneticDataSource[s.length];

		for (int i = 0; i<s.length; i++)
			g[i] = new LinkageInterface(s[i]);

		return g;
	}

	public static GeneticDataSource[] readAndSplit(String par, String ped) throws IOException
	{
		return readAndSplit(par,ped,false);
	}

	public static ParameterData readParameterData(LinkageFormatter parin) throws IOException
	{
		LinkageParameterData p = new LinkageParameterData(parin);
		LinkageDataSet q = new LinkageDataSet(p,null);
		return new LinkageInterface(q);
	}
		
	public static GeneticDataSource readTriplets() throws IOException
	{
		LinkagePedigreeData p = new LinkagePedigreeData(new LinkageFormatter(),null,LinkageIndividual.TRIPLET);
		LinkageDataSet d = new LinkageDataSet(null,p);
		return new LinkageInterface(d);
	}

/*
	public static GeneticDataSource[] readAndSplit(LinkageFormatter parin, LinkageFormatter pedin) throws IOException
	{
		LinkageDataSet p = new LinkageDataSet(parin,pedin);
		LinkageDataSet[] s = p.splitByPedigree(); 
		GeneticDataSource[] g = new GeneticDataSource[s.length];

		for (int i = 0; i<s.length; i++)
			g[i] = new LinkageInterface(s[i]);

		return g;
	}

	private static GeneticDataSource[] split(LinkageDataSet p)
	{
		LinkageDataSet[] s = p.splitByPedigree(); 
		return split(p);
	}

	public static GeneticDataSource readTriplets(String ped) throws IOException
	{
		LinkagePedigreeData p = new LinkagePedigreeData(new LinkageFormatter(ped),null,LinkageIndividual.TRIPLET);
		LinkageDataSet d = new LinkageDataSet(null,p);
		return new LinkageInterface(d);
	}

	public static GeneticDataSource readPedigreeData() throws IOException
	{
		LinkagePedigreeData p = new LinkagePedigreeData(new LinkageFormatter(),null,false);
		LinkageDataSet d = new LinkageDataSet(null,p);
		return new LinkageInterface(d);
	}

	public static GeneticDataSource readPedigreeData(String ped) throws IOException
	{
		LinkagePedigreeData p = new LinkagePedigreeData(new LinkageFormatter(ped),null,false);
		LinkageDataSet d = new LinkageDataSet(null,p);
		return new LinkageInterface(d);
	}

	public static ParameterData readParameterData() throws IOException
	{
		LinkageParameterData p = new LinkageParameterData(new LinkageFormatter());
		LinkageDataSet d = new LinkageDataSet(p,null);
		return new LinkageInterface(d);
	}

	public static ParameterData readParameterData(String par) throws IOException
	{
		LinkageParameterData p = new LinkageParameterData(new LinkageFormatter(par));
		LinkageDataSet d = new LinkageDataSet(p,null);
		return new LinkageInterface(d);
	}
*/
}
