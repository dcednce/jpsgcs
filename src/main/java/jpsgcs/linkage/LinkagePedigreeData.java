package jpsgcs.linkage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.Vector;

import jpsgcs.genio.Pedigree;
import jpsgcs.graph.DAG;
import jpsgcs.util.Triplet;

/**
 * A structure representing the data from a linkage .ped file.
 */
public class LinkagePedigreeData implements LinkConstants {
    public boolean twosexes = true;

    public LinkagePedigreeData() {
    }
    
    public LinkagePedigreeData(LinkageFormatter b, LinkageParameterData par, int format, Set<String> disposables) throws IOException {
        Vector<LinkageIndividual> v = new Vector<LinkageIndividual>();
        for (; b.newLine();) {
            LinkageIndividual p = new LinkageIndividual(b, par, format);
            if (! disposables.contains(p.id.stringGetId())) {
                v.addElement(p);
	        }
        }
        set(v);
    }

    public LinkagePedigreeData(LinkageFormatter b, LinkageParameterData par, int format) throws IOException {
        this(b, par, format, new HashSet<String>());
    }

    public LinkagePedigreeData(LinkageFormatter b, LinkageParameterData par, boolean premake) throws IOException {
        this(b, par, (premake ? PREMAKE : STANDARD));
    }

    public LinkagePedigreeData(LinkageFormatter b, LinkageParameterData par, Set<String> disposables) throws IOException {
        this(b, par, STANDARD, disposables);
    }

    public LinkagePedigreeData(LinkageFormatter b, LinkageParameterData par) throws IOException {
        this(b, par, STANDARD, new HashSet<String>());
    }

    public LinkagePedigreeData(String s, LinkageParameterData par) throws IOException {
        this(new LinkageFormatter(s), par);
    }

    public LinkagePedigreeData(LinkagePedigreeData p, int[] x) {
        Vector<LinkageIndividual> v = new Vector<LinkageIndividual>();

        LinkageIndividual[] ind = p.getIndividuals();
        for (int i = 0; i < ind.length; i++) {
            v.add(new LinkageIndividual(ind[i], x));
        }
        set(v);
    }

    public LinkagePedigreeData(LinkagePedigreeData p) {
        Vector<LinkageIndividual> v = new Vector<LinkageIndividual>();

        LinkageIndividual[] ind = p.getIndividuals();
        for (int i = 0; i < ind.length; i++) {
            v.add(new LinkageIndividual(ind[i]));
        }

        set(v);
    }

    public LinkagePedigreeData(Collection<LinkageIndividual> v) {
        set(v);
    }

    public boolean isOutputPremake() {
        return outputpremake;
    }

    public void setOutputPremake(boolean premake) {
        outputpremake = premake;
    }

    public void quickSet(Collection<LinkageIndividual> v) {
        Collection<Triplet> t = new LinkedHashSet<>();

        for (LinkageIndividual cur : v) {
            LinkageIndividual pa = find(cur.paid);
            LinkageIndividual ma = find(cur.maid);
            t.add(new Triplet(cur, pa, ma));
        }

        ped = new Pedigree();
        ped.addTriplets(t);

        ind = v.toArray(new LinkageIndividual[0]);

        for (int i = 0; i < ind.length; i++) {
            ind[i].index = i;
        }
    }

    public void set(Collection<LinkageIndividual> v) {
        Map<String, LinkageIndividual> h = new LinkedHashMap<String, LinkageIndividual>();

        for (LinkageIndividual cur : v) {
            //System.out.println("test"+cur.pedid.stringGetId()+"::"+cur.id.stringGetId());
            String s = cur.pedid.stringGetId() + "::" + cur.id.stringGetId();
            // System.out.println("test"+s);
            if (h.get(s) != null) {
                System.err.println("Warning: Multiple input lines for " + cur.pedid.stringGetId() + " " + cur.id.stringGetId());
                System.err.println("Ignoring all but the first.");
            }
            else {
                h.put(s, cur);
            }
        }

        Collection<Triplet> t = new LinkedHashSet<>();
        for (LinkageIndividual cur : v) {
            LinkageIndividual pa = null;
            LinkageIndividual ma = null;
            if (cur.paid!= null) {
                String s = cur.pedid.stringGetId() + "::" + cur.paid.stringGetId();
                pa = h.get(s);
                if (pa == null) {
                    pa = new LinkageIndividual(cur,(String)(cur.paid.stringGetId()));
                    pa.sex = MALE;
                    h.put(s, pa);
                    //t.add(new Triplet<LinkageIndividual, LinkageIndividual, LinkageIndividual>(pa, null, null));
                    t.add(new Triplet(pa, null, null));
                }
            }

            if (cur.maid != null) {
                String s = cur.pedid.stringGetId()+ "::" + cur.maid.stringGetId();
                ma = h.get(s);
                if (ma == null) {
                    ma = new LinkageIndividual(cur,((String)cur.maid.stringGetId()));
                    ma.sex = FEMALE;
                    h.put(s, ma);
                    t.add(new Triplet(ma, null, null));
                }
            }

            if (twosexes && pa != null && ma != null && pa.equals(ma)) {
                System.err.println("Warning: Father and mother of " + cur.uniqueName() + " are the same person: "
                                   + pa.uniqueName());
                System.err.println("Unless selfing is possible, you should correct this");
            }
            else {
                if (pa != null) {
                    if (pa.sex != MALE) {
                        if (pa.sex == FEMALE) {
                            System.err.println("Warning: Father of " + cur.uniqueName()
                                               + " previously listed as female: " + pa.uniqueName() + " : " + pa.sex);
                            System.err.println("Setting father's sex to male");
                        }

                        pa.sex = MALE;
                    }
                }

                if (ma != null) {
                    if (ma.sex != FEMALE) {
                        if (ma.sex == MALE) {
                            System.err.println("Warning: Mother of " + cur.uniqueName()
                                               + " previously listed as male: " + ma.uniqueName() + " : " + ma.sex);
                            System.err.println("Setting mother's sex to female");
                        }
                        ma.sex = FEMALE;
                    }
                }
            }

            t.add(new Triplet(cur, pa, ma));
        }

        ped = new Pedigree();
        ped.addTriplets(t);

        Collection<LinkageIndividual> u = h.values();
        ind = u.toArray(new LinkageIndividual[u.size()]);
        for (int i = 0; i < ind.length; i++) {
        	indivIndexMap.put(ind[i].id, i);
            ind[i].index = i;
        }
    }

    public LinkageIndividual pa(LinkageIndividual x) {
        for (int i = 0; i < ind.length; i++) {
            LinkageId<?> ped = ind[i].pedid;
            LinkageId<?> xped = x.pedid;
            LinkageId<?> xpa = x.paid;

            if (ped == null || xped == null || xpa == null) {
                return null;
            }
            if (ped.stringGetId().equals(xped.stringGetId()) && ind[i].id.stringGetId().equals(xpa.stringGetId())) {
                return ind[i];
            }
        }
        return null;
    }

    public int getIndividualIndex(LinkageId<?> lid) {
    	Integer index = null;
    
    	index = indivIndexMap.get(lid);
    	if (index == null) {
    		for(int i = 0; i < ind.length; i++) {   			
    			if (ind[i].id.equals(lid)) {
    				indivIndexMap.put(ind[i].id, i);
    				return i;
    			}
    		}
    	}
    	
    	return index;
    	
    }
    public LinkageIndividual ma(LinkageIndividual x) {
        for (int i = 0; i < ind.length; i++) {
            LinkageId<?> ped = ind[i].pedid;
            LinkageId<?> xped = x.pedid;
            LinkageId<?> xma = x.maid;
            if (ped == null || xped == null || xma == null) {
                return null;
            }
            if (ped.stringGetId().equals(xped.stringGetId()) && ind[i].id.stringGetId().equals(xma.stringGetId())) {
                return ind[i];
            }
        }
        return null;
    }

    /**
     * Returns the array of individual data structures.
     */
    public LinkageIndividual[] getIndividuals() {
        return ind;
    }

    public int nIndividuals() {
        return ind.length;
    }

    public LinkageId<?> firstPedid() {
        if (ind == null || ind.length == 0) {
            return null;
        }
        else {
            return ind[0].pedid;
        }
    }

    /**
     * Returns a string representing the data for the given pedigrees in the
     * same format as it was read in from the linkage .ped file.
     */
    @Override
    public String toString() {
        StringBuffer s = new StringBuffer();
        if (outputpremake) {
            for (int i = 0; i < ind.length; i++) {
                s.append(ind[i].shortString()).append("\n");
            }
        }
        else {
            for (int i = 0; i < ind.length; i++) {
                s.append(ind[i].longString()).append("\n");
            }
        }

        if (ind.length > 0) {
            s.deleteCharAt(s.length() - 1);
        }
        return s.toString();
    }

    public void writeTo(PrintStream p) {
        if (outputpremake) {
            for (int i = 0; i < ind.length; i++) {
                if (i == ind.length - 1) {
                    p.print(ind[i].shortString());
                }
                else {
                    p.println(ind[i].shortString());
                }
            }
        }
        else {
            for (int i = 0; i < ind.length; i++) {
                ind[i].writeTo(p);
                // if (i != ind.length-1)
                p.println();
            }
        }

    }

    public void writeTranspose(PrintStream p) {
        p.println(ind.length);

        for (int i = 0; i < ind.length; i++) {
            p.println(ind[i].pedigreeDataString());
        }

        p.flush();

        int nloc = -1;
        for (int i = 0; i < ind.length; i++) {
            if (ind[i].pheno != null) {
                if (nloc < ind[i].pheno.length) {
                    nloc = ind[i].pheno.length;
                }
            }
        }

        for (int i = 0; i < nloc; i++) {
            for (int j = 0; j < ind.length; j++) {
                p.print(ind[j].pheno[i] + "  ");
            }
            p.println();
        }

        p.flush();
    }

    public LinkageIndividual find(LinkageId<?> person){
        if (person == null) {
            return null;
        }
        for(Object i : ped.individuals()) {
            LinkageIndividual member = (LinkageIndividual)i;
            if (member.id.stringGetId().equals(person.stringGetId())) {
                return member;
            }
        }
        return null;
    }
    /**
     * Creates a checked Pedigree structure from the list of individuals.
     */
    public Pedigree getPedigree() {
        return ped;
    }
    public Boolean integerCheck(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public LinkagePedigreeData[] splitByPedigree() {
        Map<String, Vector<LinkageIndividual>> h = new LinkedHashMap<String, Vector<LinkageIndividual>>();
        for (int i = 0; i < ind.length; i++) {
            LinkageId<?> x=null;
            if(integerCheck(ind[i].pedid.stringGetId()))
                x= new LinkageId<Integer>(Integer.parseInt((String)ind[i].pedid.stringGetId()));
            else
                x = new LinkageId<String>(ind[i].pedid.stringGetId());
            Vector<LinkageIndividual> v = h.get(x.stringGetId());
            if (v == null) {
                v = new Vector<LinkageIndividual>();
                h.put(x.stringGetId(), v);
            }
            v.add(ind[i]);
        }

        Vector<LinkageIndividual>[] u = h.values().toArray(new Vector[0]);
        LinkagePedigreeData[] lpd = new LinkagePedigreeData[u.length];
        for (int i = 0; i < lpd.length; i++) {
            lpd[i] = new LinkagePedigreeData(u[i]);
        }

        return lpd;
    }

    public void resetProbands(List<LinkageId<?>> idsToSet) {
        for (LinkageIndividual one : ind) {
            one.setProband(0);      
        }
        for (LinkageId<?> lid : idsToSet) {
            LinkageIndividual member = find(lid);
            if (member == null) {
                throw new RuntimeException("Cannot find individual to set as proband: " + lid.toString());
            }
            member.setProband(1);
        }
    }

    public int countMeioses() {
        DAG<LinkageIndividual, Object> peopledag = new DAG<LinkageIndividual, Object>();
        LinkagePedigreeData[] allPeds = splitByPedigree();
        List<LinkageIndividual>probands = new ArrayList<LinkageIndividual>();

        if (allPeds.length != 1) {
            throw new RuntimeException("we cannot count meioses for more than one pedigree");
        }
        for (LinkageIndividual lid : getIndividuals()) {
            peopledag.connect(pa(lid), lid);
            peopledag.connect(ma(lid), lid);
            if (lid.proband == 1) {
                probands.add(lid);
            }
        }
        LinkedHashSet<LinkageIndividual> idSet = new LinkedHashSet<LinkageIndividual>();
        return meiosesCount(peopledag, idSet);
    }

    private int meiosesCount(DAG<LinkageIndividual, Object> pDAG, Set<LinkageIndividual> s) {
        Set<LinkageIndividual> rel_anc = new LinkedHashSet<LinkageIndividual>();
        Set<LinkageIndividual> common_anc = new LinkedHashSet<LinkageIndividual>();
        common_anc.addAll(pDAG.getVertices());
        int num_probands = 0;
        CountHolder ch = new CountHolder();

        for (LinkageIndividual person : getIndividuals()) {
            if ((person.proband == 1) && !(s.contains(person))) {
                num_probands++; // common_anc=\bigcap{i\cup anc(i)}
                Set<LinkageIndividual> temp = new LinkedHashSet<LinkageIndividual>();
                temp.addAll(pDAG.ancestors(person));
                temp.add(person);
                rel_anc.addAll(temp);
                common_anc.retainAll(temp);
            }
        }
        if (num_probands == 0) {
            ch.hasProbands = false;
            return ch.descendants;
        }
        else {
            ch.hasProbands = true;
            common_anc.remove(null);
            Set<LinkageIndividual> rem_anc = new LinkedHashSet<LinkageIndividual>();

            for (LinkageIndividual i : common_anc) {
                Set<LinkageIndividual> temp = new LinkedHashSet<LinkageIndividual>();
                for (LinkageIndividual j : common_anc) {
                    temp.addAll(pDAG.ancestors(j));
                }
                if (temp.contains(i)) {
                    rem_anc.add(i);
                }
            }

            for (LinkageIndividual j : rem_anc) {
                rel_anc.remove(j);
                common_anc.remove(j);
            }

            int num_mrca = common_anc.size();
            if (num_mrca == 0) {
                ch.ancestors = 0;
            }
            else if (num_mrca > 2) {
                ch.countMRCA = num_mrca;
            }
            else if (num_mrca == 2) {
                Collection<LinkageIndividual> com_kids = new LinkedHashSet<LinkageIndividual>();
                com_kids.addAll(rel_anc);
                for (LinkageIndividual i : common_anc) {
                    com_kids.retainAll(pDAG.children(i));
                }
                if (com_kids.isEmpty()) {
                    ch.unmarriedMRCA = true;
                }
            }
            Collection<LinkageIndividual> desc = new LinkedHashSet<LinkageIndividual>();
            for (LinkageIndividual i : common_anc) {
                desc = pDAG.descendants(i);
                break;
            }
            desc.retainAll(rel_anc);
            ch.ancestors = common_anc.size();
            ch.descendants = desc.size();
        }
        if (! ch.isReportable() ) {
            //write something somewhere
            return 0;
        }
        else {
            return ch.descendants;
        }
    }

    // Private data.

    protected LinkageIndividual[] ind           = null;
    protected HashMap<LinkageId<?>, Integer> indivIndexMap = new HashMap<>();
    protected Pedigree            ped           = null;
    protected boolean             outputpremake = false;


    // Private class
    private class CountHolder {
        public int            ancestors     = 0;
        public int            descendants   = 0;
        public int            countMRCA     = 0;
        public boolean        unmarriedMRCA = false;
        public boolean        hasProbands   = false;
        List<LinkageId> ids           = new ArrayList<LinkageId>();

        public boolean isReportable() {
            return (!unmarriedMRCA) && countMRCA <= 2 && ancestors > 0 && hasProbands;
        }
    }

    /**
     * Main reads in a linkage parameter file and a linkage pedigree file which
     * is in short form and output the linkage pedigree file in long form.
     */

    public static void main(String[] args) {
        try {
            boolean premake = false;

            switch (args.length) {
            case 1:
                if (args[0].equals("-s")) {
                    premake = true;
                }
                break;
            }

            LinkageFormatter f = new LinkageFormatter(new BufferedReader(new InputStreamReader(System.in)), "Ped file");
            LinkagePedigreeData ped = new LinkagePedigreeData(f, null, premake);
            System.out.println(ped);
        }
        catch (Exception e) {
            System.err.println("Caught in LinkagePedigreeData:main()");
            e.printStackTrace();
        }
    }
}
