package jpsgcs.linkage;

import jpsgcs.util.StringFormatter;

import java.io.PrintStream;
import java.io.Serializable;

/**
 * A class that contains the information for an individual from the linkage .ped
 * file.
 */
public class LinkageIndividual implements LinkConstants, Serializable {
    public LinkageIndividual() {
    }

    public LinkageIndividual(LinkageIndividual l, int[] x) {
        pedid = l.pedid;
        id = l.id;
        paid = l.paid;
        maid = l.maid;
        firstbornid = l.firstbornid;
        pasnextid = l.pasnextid;
        masnextid = l.masnextid;
        sex = l.sex;
        proband = l.proband;
        comment = l.comment;
        pheno = new LinkagePhenotype[x.length];
        for (int i = 0; i < pheno.length; i++)
            pheno[i] = l.pheno[x[i]];
    }

    public LinkageIndividual(LinkageIndividual l) {
        pedid = l.pedid;
        id = l.id;
        paid = l.paid;
        maid = l.maid;
        firstbornid = l.firstbornid;
        pasnextid = l.pasnextid;
        masnextid = l.masnextid;
        sex = l.sex;
        proband = l.proband;
        comment = l.comment;
        if (l.pheno != null) {
            pheno = new LinkagePhenotype[l.pheno.length];
            for (int i = 0; i < pheno.length; i++)
                pheno[i] = l.pheno[i];
        }
    }

    public LinkageIndividual(LinkageIndividual l, String temp) {
        if (integerCheck(temp))
            id = new LinkageId<Integer>(Integer.parseInt(temp));
        else
            id = new LinkageId<String>(temp);
        pedid = l.pedid;
        if (l.pheno != null) {
            pheno = new LinkagePhenotype[l.pheno.length];
            for (int i = 0; i < pheno.length; i++)
                pheno[i] = l.pheno[i].nullCopy();
        }
    }

    public Boolean integerCheck(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public LinkageIndividual(LinkageFormatter b, LinkageParameterData par, int format) {
        this(b, par, format, false);
    }
    
    public LinkageIndividual(LinkageFormatter b, LinkageParameterData par, int format, boolean doSingleCheck) {
        String temp;
        if (format == TRIPLET) {
            pedid = new LinkageId<String>("unspecified");
            id = null;
            temp = b.readString("individual id", 0, true, true);
            if (integerCheck(temp)) {
                if(Integer.parseInt(temp)<=0) {
                    b.crash("individual id " + temp + " is not positive");
                }
                else
                    id = new LinkageId<Integer>(Integer.parseInt(temp));
            } else {
                id = new LinkageId<String>(temp);
            }

            temp = b.readString("father's id", 0, true, true);
            if (integerCheck(temp)) {
                if(Integer.parseInt(temp)==0)
                    paid = null;
                else if(Integer.parseInt(temp)<0)
                    {
                        paid = null;
                        b.warn("father's id " + id.stringGetId() + " is negative"
                               + "\n\tSetting to null");
					
                    }
                else
                    paid = new LinkageId<Integer>(Integer.parseInt(temp));
            } else {
                paid = new LinkageId<String>(temp);
            }

            temp = b.readString("mother's id", 0, true, false);
            if (integerCheck(temp)) {
                if(Integer.parseInt(temp)==0)
                    maid = null;
                else if(Integer.parseInt(temp)<0)
                    {
                        maid = null;
                        b.warn("mother's id " + id.stringGetId() + " is negative"
                               + "\n\tSetting to null");
					
                    }
                else
                    maid = new LinkageId<Integer>(Integer.parseInt(temp));
            } else {
                maid = new LinkageId<String>(temp);
            }

            if (doSingleCheck)
                singleParentCheck(b);

            comment = b.restOfLine().trim();
            return;
        }

        temp = b.readString("kindred number", 0, true, false);
        if (integerCheck(temp)) {
            pedid = new LinkageId<Integer>(Integer.parseInt(temp));
        } else {
            pedid = new LinkageId<String>(temp);
        }
        temp = b.readString("individual id", 0, true, true);
        if (integerCheck(temp)) {
            if(Integer.parseInt(temp)<=0) {
                b.crash("individual id " + temp + " is not positive");
            }
            else
                id = new LinkageId<Integer>(Integer.parseInt(temp));
        } else {
            id = new LinkageId<String>(temp);
        }

        temp = b.readString("father's id", 0, true, true);
        if (integerCheck(temp)) {
            if(Integer.parseInt(temp)==0)
                paid = null;
            else if(Integer.parseInt(temp)<0)
                {
                    paid = null;
                    b.warn("father's id " + id.stringGetId() + " is negative"
                           + "\n\tSetting to null");
				
                }
            else
                paid = new LinkageId<Integer>(Integer.parseInt(temp));
        } else {
            paid = new LinkageId<String>(temp);
        }

		
        temp = b.readString("mother's id", 0, true, false);
        if (integerCheck(temp)) {
            if(Integer.parseInt(temp)==0)
                maid = null;
            else if(Integer.parseInt(temp)<0)
                {
                    maid = null;
                    b.warn("mother's id " + id.stringGetId() + " is negative"
                           + "\n\tSetting to null");
				
                }
            else
                maid = new LinkageId<Integer>(Integer.parseInt(temp));
        } else {
            maid = new LinkageId<String>(temp);
        }
		
        if (doSingleCheck)
            singleParentCheck(b);

        if (format != PREMAKE) {
            // throw new RuntimeException("Premake format not supported");
            firstbornid = b.readInt("first child pointer", 0, false, false);
            pasnextid = b.readInt("father's next child pointer", 0, false,
                                  false);
            masnextid = b.readInt("mother's next child pointer", 0, false,
                                  false);
        }

        sex = b.readInt("sex", 0, true, false);

        if (par != null && par.isSexLinked() && sex == 0)
            b.crash("Sex must be specified as 1 (MALE) or 2 (FEMALE) for all individuals at sex linked loci.");

        if (sex < 0 || sex > 2)
            b.warn("sex indicator " + sex + " is out of range " + 0 + " " + 2
                   + "\n\tSetting to 0");

        if (format != PREMAKE)
            proband = b.readInt("proband status", 0, false, false);

        if (format != PEDONLY) {
            if (par != null) {
                LinkageLocus[] l = par.getLoci();
                pheno = new LinkagePhenotype[l.length];
                if (b.itemsLeftOnLine() == 0) {
                  // we're assuming there are no genotypes for this individual
                  for (int i = 0; i < pheno.length; i++) {
                    pheno[i] = new NumberedAllelePhenotype((NumberedAlleleLocus) l[i]);
                  }
                }
                else {
                  for (int i = 0; i < pheno.length; i++) {
                      pheno[i] = l[i].readPhenotype(b, this);
                    }
                }
            }
        }

        comment = b.restOfLine().trim();
    }

    /**
     * Returns one call from the array of linkage phonetypes for this individual.
     */
    public LinkagePhenotype getPhenotype(int i) {
        return pheno[i - offset];
    }

    public void setPhenotype(LinkageIndividual other) {
        //This maintains the shallow copy of the constructors
        pheno = other.pheno;
    }
    public int getProband() {
        return proband;
    }

    public void setProband(int zeroOne) {
        proband = zeroOne;
    }

    public int index = 0;

    /**
     * The individual's pedigree id number.
     */
    // public int pedid = 0;
    public LinkageId<?> pedid = null;
    /**
     * The individual's id number.
     */
    public LinkageId<?> id = null;
    /**
     * The individual's father's id number.
     */
    // public int paid = 0;
    public LinkageId<?> paid = null;
    /**
     * The individual's mother's id number.
     */
    // public int maid = 0;
    public LinkageId<?> maid = null;
    /**
     * The individual's first born child's id number.
     */
    public int firstbornid = 0;
    /**
     * The individual's father's next born child's id number.
     */
    public int pasnextid = 0;
    /**
     * The individual's mother's next born child's id number.
     */
    public int masnextid = 0;
    /**
     * The individual's sex coded as 1 for male and 2 for female.
     */
    public int sex = 0;
    /**
     * The individual's proband status coded as 1 for proband 0 for non-proband.
     */
    public int proband = 0;
    /**
     * A string representing anything that was left on this individual's input
     * line after all thee pedigree, sex, proband and locus informatio was read
     * in.
     */
    public String comment = null;
    /**
     * A list of phenotypes as read in from the input line. For co-dominant
     * markers these phenotypes will be the genotypes.
     */
    public LinkagePhenotype[] pheno = null;

    protected int offset = 0;

    /**
     * Returns a string representation of this individual's data in the same
     * format as the original linkage input .ped file.
     */
    public String longString() {
        StringBuffer s = new StringBuffer();

        s.append(pedigreeDataString(false));
        if (pheno != null)
            for (int i = 0; i < pheno.length; i++)
                s.append(pheno[i] + "  ");

        if (comment != null)
            s.append(comment);

        return s.toString();
    }

    /**
     * Returns a string representation of the individual's pedigree data. The
     * genotypes/phenotypes are omitted.
     */
    public String pedigreeDataString() {
        return pedigreeDataString(true);
    }

    public void writeTo(PrintStream p) {
        StringFormatter f = new StringFormatter();

        p.print(pedigreeDataString(false));
        if (pheno != null)
            for (int i = 0; i < pheno.length; i++)
                p.print(pheno[i] + "  ");

        if (comment != null)
            p.print(comment);
        p.flush();
    }

    /**
     * Returns a string representation of this individual's data in pre makeped
     * format.
     */
    public String shortString() {
        StringBuffer s = new StringBuffer();

        s.append(StringFormatter.format(pedid.stringGetId(), 2) + " ");
        s.append(" ");
        s.append(StringFormatter.format(id.stringGetId(), 3) + " ");
        s.append(StringFormatter.format(paid == null ? "0" : paid.stringGetId(), 3) + " ");
        s.append(StringFormatter.format(maid == null ? "0" : maid.stringGetId(), 3) + " ");
        s.append("   ");
        s.append(StringFormatter.format(sex, 1) + " ");
        s.append("  ");
        if (pheno != null)
            for (int i = 0; i < pheno.length; i++)
                s.append(pheno[i] + "  ");

        if (comment != null)
            s.append(comment);

        return s.toString();
    }

    public String toString() {
        return "" + id.stringGetId();
    }

    public String uniqueName() {
        return pedid.stringGetId() + " " + id.stringGetId();
    }
	
    public void singleParentCheck(LinkageFormatter formatter) {
        if ((paid == null && maid != null) || (paid != null && maid == null)) {
            formatter.warn(String.format("Individual %s has only one parent specified\n\tThis may crash some programs", id.stringGetId()));
        }
    }
	
    public String pedigreeDataString(boolean withComment) {
        StringBuffer s = new StringBuffer();

        s.append(String.format("%s %s %s %s %d %d %d", pedid.stringGetId(), id.stringGetId(), 
                               paid == null ? "0" : paid.stringGetId(), maid == null ? "0" : maid.stringGetId(), firstbornid, pasnextid, masnextid));
        s.append(String.format("   %d %d  ", sex, proband));

        if (withComment) {
            if (comment != null)
                s.append(comment);
        }
		
        return s.toString();		
    }
}
