package jpsgcs.linkage;

import jpsgcs.util.StringFormatter;

/**
 This is a base class from with the linkage phenotype classes
 are derived.
*/
abstract public class LinkagePhenotype
{
	abstract public void read(LinkageFormatter f);

	abstract public double[][] penetrance();

	abstract public LinkagePhenotype nullCopy();

	abstract public boolean informative();

	abstract public void setUninformative();

	abstract public int getAllele(int k);

	abstract public boolean setAlleles(int a1, int a2);

	abstract public int getAffectedStatus();

	abstract public int getLiability();

	abstract public void reCode(int[] c);

/**
 Sets the locus associated with this phenotype.
*/
	public void setLocus(LinkageLocus l)
	{
		this.l = l;
	}

/**
 Gets the locus associated with this phenotype.
*/
	public LinkageLocus getLocus()
	{
		return l;
	}

// Private data.

	private LinkageLocus l = null;
	protected static final StringFormatter f = new StringFormatter();
}
