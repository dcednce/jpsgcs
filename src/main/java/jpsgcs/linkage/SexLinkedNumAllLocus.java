package jpsgcs.linkage;

import jpsgcs.util.StringFormatter;
import jpsgcs.util.IntArray;
import java.io.IOException;
import java.util.Map;
import java.util.LinkedHashMap;

/**
 This class represents the a locus given
 in the linkage numbered locus format.
*/
public class SexLinkedNumAllLocus extends NumberedAlleleLocus implements LinkConstants
{
        private static Map<IntArray,double[][]> xycache = null;

	public SexLinkedNumAllLocus()
	{
		type = NUMBERED_ALLELES;
	
		line1comment = "";
		line2comment = "";

                if (xycache == null)
                        xycache = new LinkedHashMap<IntArray,double[][]>();
	}

	public SexLinkedNumAllLocus(int na)
	{
		this();
		
		freq = new double[na];
		for (int i=0; i<freq.length; i++)
			freq[i] = 1;
	}

	public LinkagePhenotype makePhenotype(LinkageIndividual a)
	{
		return new SexLinkedNumAllPhenotype(this,a.sex);
	}

	public void read(LinkageFormatter b) throws IOException
	{
		int na = b.readInt("number of alleles",0,true,true);
		line1comment = b.restOfLine();
		
		freq = new double[1+na];
		freq[0] = 1;  // this is for the null male allele. As long as the value is positive, it shouldn't matter.

		b.readLine();
		for (int i=1; i<freq.length; i++)
		{
			freq[i] = b.readDouble("allele frequency "+i,0,true,true);
			if (freq[i] < 0)
				b.crash("Negative allele frequency "+freq[i]);
		}

		checkAndSetAlleleFrequencies(freq,b);

		line2comment = b.restOfLine();
	}

	public LinkageLocus copy()
	{
		SexLinkedNumAllLocus l = new SexLinkedNumAllLocus();

                l.freq = new double[freq.length];
                for (int i=0; i<l.freq.length; i++)
                        l.freq[i] = freq[i];

                l.line1comment = line1comment;
		l.line2comment = line2comment;

		return l;
	}

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		
		s.append(type+" "+(freq.length-1)+" "+line1comment+"\n");
		
		double tot = 0;
		for (int i=1; i<freq.length; i++)
			tot += freq[i];
		
		for (int i=1; i<freq.length; i++)
		{
			s.append(StringFormatter.format(freq[i]/tot,2,6));
			s.append(" ");
		}
		s.append(line2comment+"\n");

		return s.toString();
	}

	public double[][] penetrance(int a0, int a1, int sex)
	{
		if (sex == MALE && a0 != 0)
			throw new RuntimeException("Male has 2 X chromosomes.");
		if (sex == FEMALE && a0 == 0)
			throw new RuntimeException("Female has Y chromosome.");

		IntArray a = new IntArray(nAlleles(),a0,a1);

		double[][] x = xycache.get(a);

		if (x == null)
		{
			x = makeXYPen(a0,a1,sex);
			xycache.put(a,x);
		}

		return x;
	}

	private double[][] makeXYPen(int a0, int a1, int sex)
	{
                double[][] x = new double[nAlleles()][nAlleles()];

                if (sex == MALE)
                {
			if (a1 > 0)
				x[0][a1] = 1;
			else
                        	for (int j=1; j<x[0].length; j++)
                                	x[0][j] = 1;
                }
                else
                {
			if (a0 > 0)
			{
				if (a1 > 0)
				{
					x[a0][a1] = 1;
					x[a1][a0] = 1;
				}
				else
				{
					for (int j=1; j<x[0].length; j++)
					{
						x[a0][j] = 1;
						x[j][a0] = 1;
					}
				}

			}
			else
			{
				if (a1 > 0)
				{
					for (int j=1; j<x[0].length; j++)
					{
						x[a1][j] = 1;
						x[j][a1] = 1;
					}
				}
				else
				{
                        		for (int i=1; i<x.length; i++)
                                		for (int j=1; j<x[i].length; j++)
                                        		x[i][j] = 1;
				}
			}
                }

                return x;
	}
}
