package jpsgcs.linkage;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

import jpsgcs.genio.Family;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedapps.Pedigrees;

public class LinkageInterface implements GeneticDataSource, LinkConstants {

    public LinkageInterface(LinkageDataSet l) {
        L = l;
    }

    // GeneticDataSource interface

    @Override
    public int proband(int j) {
        LinkageIndividual x = L.getPedigreeData().getIndividuals()[j];
        return x.proband;
    }

    public void setProbandsCounted(int n) {
        probandsCounted = n;
    }
    public int getProbandsCounted() {
        return nProbands();
    }
    
    @Override
    public int nProbands() {            
        if (probandsCounted < 0) {
            int n = 0;
            LinkageIndividual[] x = L.getPedigreeData().getIndividuals();
            for (int i = 0; i < x.length; i++) {
                if (x[i].proband == 1) {
                    n++;
                }
            }
            probandsCounted = n;
        }
        return probandsCounted;
    }
    public List<LinkageId<?>> getProbandIdSet() {
    	List<LinkageId<?>> pidset = new ArrayList<>();
    	
    	for (LinkageIndividual indiv : L.getPedigreeData().getIndividuals()) {
    		if (indiv.proband == 1) {
    			pidset.add(indiv.id);
    		}
    	}
    	return pidset;
    }

    public int[] hetSharing() {
        int[] s = new int[nLoci()];
        int[] probs = makeProbandList();
        for (int i = 0; i < s.length; i++) {
            int[] c = new int[nAlleles(i)];

            for (int j = 0; j < probs.length; j++) {
                int a = getAllele(i, probs[j], 0);
                int b = getAllele(i, probs[j], 1);

                if (a < 0 || b < 0) {
                    for (int k = 0; k < c.length; k++) {
                        c[k]++;
                    }
                }
                else if (a == b) {
                    c[a]++;
                }
                else {
                    c[a]++;
                    c[b]++;
                }
            }

            s[i] = 0;
            for (int k = 0; k < c.length; k++) {
                if (s[i] < c[k]) {
                    s[i] = c[k];
                }
            }
        }
        return s;
    }

    @Override
    public int getAllele(int i, int j, int k) {
        return L.getPedigreeData().getIndividuals()[j].getPhenotype(i).getAllele(k);
    }

    @Override
    public boolean setAlleles(int i, int j, int b0, int b1) {
        return L.getPedigreeData().getIndividuals()[j].getPhenotype(i).setAlleles(b0, b1);
    }

    @Override
    public void writeIndividual(int j, PrintStream p) {
        L.getPedigreeData().getIndividuals()[j].writeTo(p);
    }

    @Override
    public void writePedigree(PrintStream p) {
        L.getPedigreeData().writeTo(p);
    }

    @Override
    public void writeParameters(PrintStream p) {
        L.getParameterData().writeTo(p);
    }

    @Override
    public void downcodeAlleles() {
        L.downCode(false);
    }

    // BasicGeneticData interface.

    @Override
    public double[][] penetrance(int i, int j) {
        LinkagePhenotype pheno = L.getPedigreeData().getIndividuals()[j].getPhenotype(i);
        if (pheno != null) {
            return pheno.penetrance();
        }
        return null;
    }

    @Override
    public boolean sexLinked() {
        return L.getParameterData().isSexLinked();
    }

    // ParameterData interface

    @Override
    public String locusName(int i) {
        return L.getParameterData().getLoci()[i].locName();
    }

    @Override
    public int nLoci() {
        if (L.getParameterData() == null) {
            return 0;
        }
        return L.getParameterData().nLoci();
    }

    @Override
    public int nAlleles(int i) {
        return alleleFreqs(i).length;
    }

    @Override
    public double[] alleleFreqs(int i) {
        LinkageParameterData p = L.getParameterData();
        LinkageLocus[] locs = p.getLoci();
        LinkageLocus ll = locs[i];
        return ll.alleleFrequencies();
    }

    @Override
    public double getFemaleRecomFrac(int i, int j) {
        return L.getParameterData().femaleTheta(i, j);
    }

    @Override
    public double getMaleRecomFrac(int i, int j) {
        return L.getParameterData().maleTheta(i, j);
    }

    // PedigreeData interface.

    @Override
    public int[] getRank() {
        if (rank == null) {
            rank = Pedigrees.canonicalRank(this);
        }
        return rank;
    }

    @Override
    public double getKinshipCoeff(int pbi, int pbj) {
        Double retval = null;
        HashMap<LinkageIndividual, Double> h = kinshipCache.get(L.getPedigreeData().getIndividuals()[pbi]);
        if (h == null) {
            h = new HashMap<LinkageIndividual, Double>();
            retval = Pedigrees.kinship(getRank(), this, pbi, pbj);
            h.put(L.getPedigreeData().getIndividuals()[pbj], retval);
            kinshipCache.put(L.getPedigreeData().getIndividuals()[pbi], h);
        }
        else {
            retval = h.get(L.getPedigreeData().getIndividuals()[pbj]);
            if (retval == null) {
                retval = Pedigrees.kinship(getRank(), this, pbi, pbj);
                h.put(L.getPedigreeData().getIndividuals()[pbj], retval);
            }
        }
        return retval;
    }

    public void setRank() {

    }

    @Override
    public String pedigreeName(int j) {
        LinkageIndividual x = L.getPedigreeData().getIndividuals()[j];
        return x.pedid.stringGetId() + "";
    }

    @Override
    public String individualName(int j) {
        LinkageIndividual x = L.getPedigreeData().getIndividuals()[j];
        return "" + x.id.stringGetId();
    }

    @Override
    public boolean isMale(int j) {
        LinkageIndividual x = L.getPedigreeData().getIndividuals()[j];
        return x.sex == MALE;
    }

    @Override
    public boolean isFemale(int j) {
        LinkageIndividual x = L.getPedigreeData().getIndividuals()[j];
        return x.sex == FEMALE;
    }

    @Override
    public int nIndividuals() {
        return L.getPedigreeData().getIndividuals().length;
    }

    @Override
    public int pa(int j) {
        LinkageIndividual x = L.getPedigreeData().getIndividuals()[j];
        x = (LinkageIndividual) L.getPedigreeData().getPedigree().getTriplet(x).y;
        return x == null ? -1 : x.index;
    }

    @Override
    public int ma(int j) {
        LinkageIndividual x = L.getPedigreeData().getIndividuals()[j];
        x = (LinkageIndividual) L.getPedigreeData().getPedigree().getTriplet(x).z;
        return x == null ? -1 : x.index;
    }

    @Override
    public int[] kids(int j) {
        LinkageIndividual x = L.getPedigreeData().getIndividuals()[j];
        Object[] kk = L.getPedigreeData().getPedigree().kids(x);
        int[] kkk = new int[kk.length];
        for (int i = 0; i < kkk.length; i++) {
            kkk[i] = ((LinkageIndividual) kk[i]).index;
        }
        return kkk;
    }

    @Override
    public int[][] nuclearFamilies() {
        Family[] f = L.getPedigreeData().getPedigree().nuclearFamilies();
        int[][] n = new int[f.length][];
        for (int i = 0; i < n.length; i++) {
            LinkageIndividual pa = (LinkageIndividual) f[i].getPa();
            LinkageIndividual ma = (LinkageIndividual) f[i].getMa();
            Object[] k = f[i].getKids();
            n[i] = new int[k.length + 2];
            n[i][0] = pa == null ? -1 : pa.index;
            n[i][1] = ma == null ? -1 : ma.index;
            for (int j = 0; j < k.length; j++) {
                n[i][j + 2] = ((LinkageIndividual) k[j]).index;
            }
        }

        return n;
    }

    // Not required for an interface.

    public LinkageDataSet raw() {
        return L;
    }

    /*
     * From old Genetic data interface.
     */
    public String toPhase() {
        StringBuffer s = new StringBuffer();
        int nloc = nLoci();
        s.append(nIndividuals());
        s.append("\n");
        s.append(nloc);
        s.append("\n");
        for (int i = 0; i < nloc; i++) {
            s.append(nAlleles(i) == 2 ? "S" : "M");
        }
        s.append("\n");

        for (int i = 0; i < nIndividuals(); i++) {
            s.append("#" + i);
            s.append("\n");
            for (int k = 0; k < 2; k++) {
                for (int j = 0; j < nloc; j++) {
                    s.append(" ");
                    if (getAllele(j, i, k) == -1) {
                        s.append(nAlleles(j) == 2 ? "?" : "-1");
                    }
                    else {
                        s.append(getAllele(j, i, k));
                    }
                }
                s.append("\n");
            }
        }

        s.deleteCharAt(s.length() - 1);
        return s.toString();
    }

    public String toFastPhase() {
        String space = " ";
        StringBuffer s = new StringBuffer();
        int nloc = nLoci();
        s.append(nIndividuals());
        s.append("\n");
        s.append(nloc);
        s.append("\n");
        for (int i = 0; i < nloc; i++) {
            if (nAlleles(i) != 2) {
                System.err.println("Warning: Locus " + i + " is not diallelic. Allele numbers > 1 will be set to 1");
            }
            s.append("S");
        }
        s.append("\n");

        for (int i = 0; i < nIndividuals(); i++) {
            s.append("# id " + i);
            s.append("\n");
            for (int k = 0; k < 2; k++) {
                for (int j = 0; j < nloc; j++) {
                    int allele = getAllele(j, i, k);
                    s.append(space);
                    s.append(allele == -1 ? "?" : (allele > 1 ? 1 : allele));
                }
                s.append("\n");
            }
        }

        s.deleteCharAt(s.length() - 1);
        return s.toString();
    }

    private int[] makeProbandList() {
        int[] probs = new int[nProbands()];
        int np = 0;
        for (int i = 0; i < nIndividuals(); i++) {
            if (proband(i) == 1) {
                probs[np++] = i;
            }
        }
        return probs;
    }

    // Private data, a la AT
    private  LinkageDataSet  L                = null;
    private  int             probandsCounted  = -1;
    private  int[]           rank             = null;

    private final HashMap<LinkageIndividual, HashMap<LinkageIndividual, Double>> kinshipCache = new HashMap<>();
}
