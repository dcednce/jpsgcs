package jpsgcs.infect;

import jpsgcs.viewgraph.PaintableGraph;
import jpsgcs.viewgraph.VertexRepresentation;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.graph.Network;

import java.util.LinkedHashMap;
import java.util.Map;

public class InfectionFrame extends GraphFrame<Pady,Object>
{
    public InfectionFrame(InfectionModel m, int size)
        {
            super(getPaintableGraph(m,size),new InfectionLocator(4*size,2+2*size));
        }

    public static PaintableGraph<Pady,Object> getPaintableGraph(InfectionModel m, int size)
        {
            Network<Pady,Object> g = m.linkGraph();
            Map<Pady,VertexRepresentation> map = new LinkedHashMap<Pady,VertexRepresentation>();
            for (Pady v : g.getVertices())
                map.put(v,new PadyBlob(v,size));
                        
            return new PaintableGraph<Pady,Object>(g,map);
        }
}
