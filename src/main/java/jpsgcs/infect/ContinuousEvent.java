package jpsgcs.infect;

public class ContinuousEvent extends Event
{
	public ContinuousEvent(double time, int patient, int type)
	{
		super((int)(time),patient,type);
		ctime = time;
	}

	public double time()
	{
		return ctime;
	}

	public String toString()
	{
		return ctime+"\t"+pat()+"\t"+type();
	}

	private double ctime = 0;
}
