package jpsgcs.infect;

import jpsgcs.util.InputFormatter;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Collections;
import java.util.Collection;
import java.io.PrintStream;
import java.io.IOException;

public class EventHandler
{
    /*
      public static EventHandler read(InputFormatter f) throws IOException
      {
      ArrayList<Event> c = new ArrayList<Event>();
      while (f.newLine())
      c.add(Event.read(f));
      return new EventHandler(c);
      }
    */
                
    public static Collection<Event> check(Collection<Event> c)
    {
        return new EventHandler(c).a;
    }

    public EventHandler(Collection<Event> c) 
    {
        a = new ArrayList<Event>(c);

        // Count days.
        maxday = -1;
        minday = 1000000;
        for (Event e : a)
            {
                if (e.day() > maxday)
                    maxday = e.day();
                if (e.day() < minday)
                    minday = e.day();
            }

        daycounts = new int[maxday+1];

        // List patients.
        Collections.sort(a,new Event.ByType());
        Collections.sort(a,new Event.ByPatient());
        Collections.sort(a,new Event.ByDay());

        LinkedHashSet<Integer> h = new LinkedHashSet<Integer>();
        for (Event e : a)
            h.add(e.pat());
                
        int n = 0;
        pat = new int[h.size()];
        for (Integer i : h)
            pat[n++] = i.intValue();

        // Sort.
        Collections.sort(a,new Event.ByPatient());

        // Count patients each day.
        for (int i=0; i<pat.length; i++)
            {
                Event[] hist = patientHistory(i);
                for (int j=hist[0].day(); j<=hist[hist.length-1].day(); j++)
                    daycounts[j]++;
            }
    }

    public void writeTo(PrintStream p) throws IOException
    {
        for (Event e : a)
            p.println(e);
    }

    public int nDays()
    {
        return daycounts.length;
    }

    public int nPatients()
    {
        return pat.length;
    }

    public int name(int i)
    {
        return pat[i];
    }

    public int count(int i)
    {
        return daycounts[i];
    }

    public int maxCount()
    {
        int x = 0;
        for (int i=0; i<daycounts.length; i++)
            if (x < daycounts[i])
                x = daycounts[i];
        return x;
    }

    public Event[] patientHistory(int i)
    {
        int name = pat[i];

        int k = 0;
        for (; ; k++)
            {
                if (k == a.size())
                    return null;
                if (a.get(k).pat() == name)
                    break;
            }

        ArrayList<Event> x = new ArrayList<Event>();
        for (; ; )
            {
                x.add(a.get(k));
                k++;
                if (k == a.size())
                    break;
                if (a.get(k).pat() != name)
                    break;
            }
                        
        Event[] e = x.toArray(new Event[0]);
        boolean error = false;

        if (e[0].type() != 0)
            {
                error = true;
                System.err.println("Event error: first event is not entry.");
                a.add(new Event(minday,e[0].pat(),0));
            }

        if (e[e.length-1].type() != 3)
            {
                //      error = true;
                //                      System.err.println("Event warning: last event is not exit. Adding discharge event on last day.");
                a.add(new Event(maxday,e[0].pat(),3));
            }

        if (error)
            {
                for (Event b : e)
                    System.err.println(b);
                //                      System.exit(1);
            }

        return e;
    }

    public void resetCalendar()
    {
        if (minday == 0)
            return;

        for (Event e : a)
            e.d -= minday;

        maxday -= minday;

        int[] dc = new int[maxday+1];
        for (int i=0; i<dc.length; i++)
            dc[i] = daycounts[i+minday];

        daycounts = dc;
        minday = 0;
    }

    // Private data.

    private ArrayList<Event> a = null;
    private int[] daycounts = null;
    private int[] pat = null;
    private int minday = 0;
    private int maxday = 0;

    // Test main.
        
    public static void main(String[] args)
    {
        try
            {
                InputFormatter f = new InputFormatter();
                ArrayList<Event> c = new ArrayList<Event>();
                while (f.newLine())
                    c.add(Event.read(f));
                        
                EventHandler h = new EventHandler(c);

                double total = 0;
                double count = 0;

                for (int i=0; i<h.nPatients(); i++)
                    {
                        Event[] q = h.patientHistory(i);
                        int admit = q[0].day();

                        total += 1;

                        for (int j=1; j<q.length; j++)
                            {
                                if (q[j].type() == 2 && q[j].day()-admit <= 0)
                                    {
                                        System.out.println(q[0]);
                                        System.out.println(q[j]);

                                        count += 1;
                                        break;
                                    }
                            }
                    }

                System.err.println(count+" "+total+" "+(count/total));
                        
            }
        catch (Exception x)
            {
                x.printStackTrace();
            }
    }
}
