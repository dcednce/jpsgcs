package jpsgcs.infect;

import jpsgcs.markov.Parameter;
import jpsgcs.markov.Function;
import java.util.Random;

public class OneUnitModel extends InfectionModel
{
    public OneUnitModel(EventHandler h, TransmissionRate trans, Parameter impt, Parameter falsep, Parameter falsen, int init)
    {
        super(h,init);
                
        // Add import functions.

        for (int i=0; i<h.nPatients(); i++)
            add(new Import(pathist[i][0],impt));

        // Add transmission functions.

        for (int i=0; i<h.nPatients(); i++)
            for (int j=1; j<pathist[i].length; j++)
                add(new Link(pathist[i][j],pathist[i][j-1],pathist[i][j-1].getCounter(),trans));

        // Add test functions.

        for (int i=0; i<h.nPatients(); i++)
            {
                Event[] hist = h.patientHistory(i);
                Pady[] p = pathist[i];
                int k = hist[0].day();

                for (Event e : hist)
                    {
                        if (e.type() == Event.NEGTEST)
                            {
                                add(new Test(p[e.day()-k],0,falsep,falsen));
                                p[e.day()-k].setResult(-1);
                            }
                        if (e.type() == Event.POSTEST)
                            {
                                add(new Test(p[e.day()-k],1,falsep,falsen));
                                p[e.day()-k].setResult(1);
                            }
                    }
            }
    }
}
