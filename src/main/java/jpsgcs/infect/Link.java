package jpsgcs.infect;

import jpsgcs.markov.Variable;
import jpsgcs.markov.Parameter;
import jpsgcs.markov.Function;

public class Link extends Function
{
    public Link(Pady today, Pady yesterday, Counter c, TransmissionRate trans)
    {
        v = new Variable[3];
        v[0] = today;
        v[1] = yesterday;
        v[2] = c;
                
        t = trans;
        p = new Parameter[1];
        p[0] = t;
    }

    public Variable[] getVariables()
    {
        return v;
    }

    public Parameter[] getParameters()
    {
        return p;
    }

    public double getValue()
    {
        if (v[1].getState() == 1)
            return v[0].getState() == 1 ? 1 : 0;

        double x = t.getValue(v[2].getState());

        return v[0].getState() == 1 ? x : 1-x;
    }

    public double logValue()
    {
        if (v[1].getState() == 1)
            return v[0].getState() == 1 ? 0 : Double.NEGATIVE_INFINITY;

        return v[0].getState() == 1 ? t.logValue(v[2].getState()) : t.logOneMinusValue(v[2].getState());
    }

    public String toString()
    {
        return "Link:"+v[0]+":"+v[1];
    }

    private Variable[] v = null;
    private Parameter[] p = null;
    private TransmissionRate t = null;
}
