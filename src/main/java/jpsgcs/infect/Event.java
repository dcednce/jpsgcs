package jpsgcs.infect;

import jpsgcs.util.InputFormatter;
import java.util.Comparator;

public class Event implements Comparable<Event>
{
	public static final int ADMISSION = 0;
	public static final int NEGTEST = 1;
	public static final int POSTEST = 2;
	public static final int DISCHARGE = 3;
	public static final int INFECTION = 4;

	public Event(int day, int patient, int type)
	{
		d = day;
		p = patient;
		t = type;
	}

	public int compareTo(Event e)
	{
		if (p < e.p)
			return -1;
		if (e.p < p)
			return 1;
		if (d < e.d)
			return -1;
		if (e.d < d)
			return 1;
		if (t < e.t)
			return -1;
		if (e.t < t)
			return 1;
		return 0;
	}

	public static Event read(InputFormatter f)
	{
		return read(f,1);
	}

	public static Event read(InputFormatter f, int ints)
	{
		int d = (int) (f.nextDouble()*ints);
		int p = f.nextInt();
		int t = f.nextInt();

		if (d < 0) 
			throw new RuntimeException("Line "+f.lastLine()+":\t Day out of range:\t"+d);
		if (p < 0) 
			throw new RuntimeException("Line "+f.lastLine()+":\t Patient id out of range:\t"+p);
		if (t < 0 || t > 3) 
		{
		//	throw new RuntimeException("Line "+f.lastLine()+":\t Type code out of range:\t"+t);
			return null;
		}

		return new Event(d,p,t);
	}

	public String toString()
	{
		return "\t"+d+"\t"+p+"\t"+t;
	}

	public int day()
	{
		return d;
	}

	public int pat()
	{	
		return p;
	}

	public int type()
	{
		return t;
	}

	public static class ByDay implements Comparator<Event>
	{
		public int compare(Event a, Event b)
		{
			if (a.d < b.d)
				return -1;
			if (b.d < a.d)
				return 1;
			return 0;
		}
	}

	public static class ByPatient implements Comparator<Event>
	{
		public int compare(Event a, Event b)
		{
			if (a.p < b.p)
				return -1;
			if (b.p < a.p)
				return 1;
			return 0;
		}
	}

	public static class ByType implements Comparator<Event>
	{
		public int compare(Event a, Event b)
		{
			if (a.t < b.t)
				return -1;
			if (b.t < a.t)
				return 1;
			return 0;
		}
	}

// Private data and methods.

	protected int d = 0;
	protected int p = 0;
	protected int t = 0;
}
