package jpsgcs.infect;

import jpsgcs.util.InputFormatter;
import jpsgcs.graph.Network;
import jpsgcs.markov.Product;
import jpsgcs.markov.Variable;
import jpsgcs.markov.Function;
import jpsgcs.markov.Parameter;
import java.util.Collection;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.ArrayList;

public class InfectionModel extends Product
{
    public InfectionModel(EventHandler h, int init)
    {
        // Set up the patient-day and counter variables.

        Counter[] c = new Counter[h.nDays()];
        for (int i=0; i<h.nDays(); i++)
            {
                c[i] = new Counter(i,1+h.count(i));
                c[i].setState(init == 1 ? h.count(i) : 0);
            }

        pathist = new Pady[h.nPatients()][];

        for (int i=0; i<h.nPatients(); i++)
            {
                Event[] hist = h.patientHistory(i);
                        
                int kk = hist[hist.length-1].day() - hist[0].day() + 1;

                pathist[i] = new Pady[kk];

                for (int j = hist[0].day(), k = 0; j <= hist[hist.length-1].day(); j++, k++)
                    {
                        pathist[i][k] = new Pady(i,j);
                        pathist[i][k].setState(init == 1 ? 1 : 0);
                        pathist[i][k].setCounter(c[j]);
                    }

            }

        counters = c;
    }

    public void updatePatientHistories()
    {
        for (int i=0; i<pathist.length; i++)
            patFlip(i);
    }

    public int[] infectedAtDay(int k)
    {
        int[] res = new int[pathist.length];

        for (int i=0; i<res.length; i++)
            {
                Pady[] hist = pathist[i];

                int myk = k - hist[0].day();

                if (myk >= 0 && myk < hist.length)
                    res[i] = hist[myk].getState();
            }

        return res;
    }

    public int nPatients()
    {
        return pathist.length;
    }

    public int dayOfInfection(int i)
    {
        if (pathist[i][0].getState() == 1)
            return -1;

        for (int j=1; j<pathist[i].length; j++)
            if (pathist[i][j].getState() == 1)
                return pathist[i][j].day();

        return -1;
    }

    private void patFlip(int k)
    {
        Pady[] p = pathist[k];
        int[] old = new int[p.length];
        for (int i=0; i<p.length; i++)
            old[i] = p[i].getState();

        int[] cur = new int[p.length];
        for (int i=0; i<cur.length; i++)
            cur[i] = 1;

        int rand = (int)(Math.random() * (cur.length+1));
        for (int i=0; i<rand; i++)
            cur[i] = 0;

        Set<Variable> v = new LinkedHashSet<Variable>();
        for (int i=0; i<p.length; i++)
            if (cur[i] != old[i])
                {
                    p[i].flip();
                    v.add(p[i]);
                    v.add(p[i].getCounter());
                }

        double z = logValue(v);

        for (int i=0; i<p.length; i++)
            if (cur[i] != old[i])
                p[i].flip();

        if (!Double.isInfinite(z))
            z -= logValue(v);

        if (Math.log(Math.random()) < z)
            for (int i=0; i<p.length; i++)
                if (old[i] != cur[i])
                    p[i].flip();
    }

    public Network<Pady,Object> linkGraph()
    {
        if (g == null)
            {
                g = new Network<Pady,Object>();

                for (int i=0; i<pathist.length; i++)
                    {
                        g.add(pathist[i][0]);
                        for (int j=1; j<pathist[i].length; j++)
                            g.connect(pathist[i][j-1],pathist[i][j]);
                                        
                    }

            }

        return g;
    }

    // Private data and methods.

    private Network<Pady,Object> g = null;
    protected Pady[][] pathist = null;
    protected Counter[] counters = null;
}
