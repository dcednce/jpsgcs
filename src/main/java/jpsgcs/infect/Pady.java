package jpsgcs.infect;

import jpsgcs.markov.Variable;

public class Pady extends Variable
{
    public Pady(int patient, int day)
    {
        super(2);
        p = patient;
        d = day;
    }

    public String toString()
    {
        return "PD:"+p+":"+d;
    }

    public int day()
    {
        return d;
    }

    public int pat()
    {
        return p;
    }

    public void setCounter(Counter k)
    {
        c = k;
    }

    public Counter getCounter()
    {
        return c;
    }

    public void flip()
    {
        if (getState() == 0)
            {
                setState(1);
                c.increase();
            }
        else
            {
                setState(0);
                c.decrease();
            }
    }

    public void setResult(int i)
    {
        t = i;
    }

    public int getResult()
    {
        return t;
    }

    private Counter c = null;
    private int p = 0;
    private int d = 0;
    private int t = 0;
}
