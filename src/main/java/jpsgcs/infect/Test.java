package jpsgcs.infect;

import jpsgcs.markov.Variable;
import jpsgcs.markov.Parameter;
import jpsgcs.markov.Function;

public class Test extends Function
{
    public Test(Pady patientday, int result, Parameter falsepos, Parameter falseneg)
    {
        v = new Variable[1];
        v[0] = patientday;

        p = new Parameter[2];
        p[0] = falsepos;
        p[1] = falseneg;

        r = result;
    }

    public double getValue()
    {
        if (v[0].getState() == 0)
            return r == 0 ? 1-p[0].getValue() : p[0].getValue();
        
        if (v[0].getState() == 1)
            return r == 0 ? p[1].getValue() : 1-p[1].getValue();

        return 1;
    }

    public double logValue()
    {
        if (v[0].getState() == 0)
            return r == 0 ? p[0].logOneMinusValue() : p[0].logValue();
        
        if (v[0].getState() == 1)
            return r == 0 ? p[1].logValue() : p[1].logOneMinusValue();

        return 0;
    }

    public Variable[] getVariables()
    {
        return v;
    }

    public Parameter[] getParameters()
    {
        return p;
    }

    public int getResult()
    {
        return r;
    }

    public String toString()
    {
        return "Test:"+v[0]+"="+r;
    }

    private Variable[] v = null;
    private Parameter[] p = null;
    private int r = 0;
}
