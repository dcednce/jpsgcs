package jpsgcs.infect;

import jpsgcs.animate.PaintableFrame;
import jpsgcs.markov.Function;
import jpsgcs.markov.Parameter;
import java.util.ArrayList;
import java.util.Collection;
import java.awt.Color;

public class OneUnitSampler extends InfectionSampler
{
    public OneUnitSampler(Collection<Event> e, double deltatime)
    {
        this(e,false,deltatime);
    }

    public OneUnitSampler(Collection<Event> e, boolean show, double deltatime)
    {
        this(e,show,deltatime,1.0);
    }

    public OneUnitSampler(Collection<Event> e, boolean show, double deltatime, double sig)
    {
        this(e,show,deltatime,sig,0.01,0.05,0.05);
    }

    public OneUnitSampler(Collection<Event> e, boolean show, double deltatime, double sig, double trans0, double imp0, double falseneg0)
    {
        EventHandler h = new EventHandler(e);

        sigma = sig;

        trans = new TransmissionRate("Transmission",trans0,h.maxCount(),deltatime);
        impt = new Parameter("Importation",0,1,imp0);
        falsen = new Parameter("False Negative",0,1,falseneg0);
        Parameter falsep = new Parameter("False Positive",0,1,0);
                        
        model = new OneUnitModel(h,trans,impt,falsep,falsen,0);
                
        Parameter[] tpars = {trans, impt, falsen};
        pars = tpars;

        InfectionFrame frame = null;
        if (show)
            {
                frame = new InfectionFrame(model,5);
                frame.getCanvas().setBackground(new Color(230,255,255));
                try { Thread.sleep(200); } catch (Exception ex) {}
            }

        /*
          frame = null;
          if (show)
          {
          frame = new PaintableFrame(new InfectionImage(model,5),new Parameter[0]);
          try { Thread.sleep(2000); } catch (Exception ex) {}
          frame.getCanvas().setBackground(new Color(230,255,255));
          }
        */
    }

    public InfectionModel getModel()
    {
        return model;
    }

    public void update()
    {
        model.updatePatientHistories();

        // Gibbs update for importation probability.

        double x = rexp();
        double y = rexp();

        for (Function f : model.getFunctions(impt))
            {
                if (f.getVariables()[0].getState() == 1)
                    x += rexp();
                else
                    y += rexp();
            }

        impt.setValue(x/(x+y));

        // Gibbs update for false negative prob.

        x = rexp();
        y = rexp();

        for (Function f : model.getFunctions(falsen))
            if (f.getVariables()[0].getState() == 1)
                {
                    if (((Test)f).getResult() == 0)
                        x += rexp();
                    else
                        y += rexp();
                }

        falsen.setValue(x/(x+y));

        // Metropolis update for transmission probability using log transform.

        double oldval = trans.getValue();
        x = model.logValue(trans);

        double prop = invlog(log(oldval)+rnorm(sigma));
        trans.setValue(prop);
        y = model.logValue(trans);

        if (log(runif()) > y - x + log(dlog(oldval)/dlog(prop)))
            trans.setValue(oldval);

        /*
          double lambda = trans.getValue();
          x = model.logValue(trans);

          double beta = Math.log(lambda);
          beta += rnorm(sigma);

          trans.setValue(Math.exp(beta));
          y = model.logValue(trans);

          if (y < x - rexp())
          trans.setValue(lambda);
        */
    }

    public void maximize()
    {
        model.updatePatientHistories();

        // Gibbs update for importation probability.

        double x = 0.000000001;
        double y = 0.000000001 ;

        for (Function f : model.getFunctions(impt))
            {
                if (f.getVariables()[0].getState() == 1)
                    x += 1;
                else
                    y += 1;
            }

        impt.setValue(x/(x+y));

        // Gibbs update for false negative prob.

        x = 0.000000001;
        y = 0.000000001;

        for (Function f : model.getFunctions(falsen))
            if (f.getVariables()[0].getState() == 1)
                {
                    if (((Test)f).getResult() == 0)
                        x += 1;
                    else
                        y += 1;
                }

        falsen.setValue(x/(x+y));

        // Metropolis update for transmission probability using log transform.

        double oldval  = trans.getValue();
        x = model.logValue(trans);

        double prop = invlog(log(oldval)+rnorm(sigma));
        trans.setValue(prop);
        y = model.logValue(trans);

        if (y < x)
            trans.setValue(oldval );
    }

    //  Private data and methods.

    private     double sigma = 0.1;
    private InfectionModel model = null;
    private TransmissionRate trans = null;
    private Parameter impt = null;
    private Parameter falsen = null;
    private double delt = 1;
}
