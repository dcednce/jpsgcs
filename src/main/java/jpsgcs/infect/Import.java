package jpsgcs.infect;

import jpsgcs.markov.Variable;
import jpsgcs.markov.Parameter;
import jpsgcs.markov.Function;

public class Import extends Function
{
    public Import(Pady pad, Parameter imp)
    {
        v = new Variable[1];
        v[0] = pad;
        p = new Parameter[1];
        p[0] = imp;
    }

    public Variable[] getVariables()
    {
        return v;
    }

    public Parameter[] getParameters()
    {
        return p;
    }

    public double getValue()
    {
        return v[0].getState() == 1 ? p[0].getValue() : 1-p[0].getValue();
    }

    public double logValue()
    {
        return v[0].getState() == 1 ? p[0].logValue() : p[0].logOneMinusValue();
    }

    public String toString()    
    {
        return "Import:"+v[0];
    }

    private Variable[] v = null;
    private Parameter[] p = null;
}
