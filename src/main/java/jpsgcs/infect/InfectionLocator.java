package jpsgcs.infect;

import jpsgcs.graph.Coord;
import jpsgcs.graph.GraphLocator;
import jpsgcs.graph.LocatedGraph;
//import jpsgcs.graph.Point;
import jpsgcs.markov.Parameter;

public class InfectionLocator extends GraphLocator<Pady,Object>
{
    public InfectionLocator(double x, double y)
        {
            par = new Parameter[0];
            xscale = x;
            yscale = y;
        }

    public double move(LocatedGraph<Pady,Object> g)
    {
        return 0;
    }

    public void set(LocatedGraph<Pady,Object> g)
    {
        for (Pady v : g.getVertices())
            {
                Coord p = g.getCoord(v);
                //      p.y = v.day() * xscale;
                //      p.x = v.pat() * yscale;
                p.x = v.day() * xscale;
                p.y = -v.pat() * yscale;
            }
    }

    private double xscale = 0;
    private double yscale = 0;
}
