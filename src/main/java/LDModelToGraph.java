import jpsgcs.pedmcmc.LDModel;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageDataSet;

public class LDModelToGraph
{
	public static void main(String[] args)
	{
		try
		{
			LinkageFormatter parin = new LinkageFormatter();
			LinkageParameterData par = new LinkageParameterData(parin);
			LDModel ld = new LDModel(parin);
			if (ld.getLocusVariables() == null)
				ld = new LDModel(new LinkageInterface(new LinkageDataSet(par,null)));

			System.out.println(ld.markovGraph());
		}
		catch (Exception e)
		{
			System.err.println("Caught in LDModelToGraph.main()");
			e.printStackTrace();
		}
	}
}
