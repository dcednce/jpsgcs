import jpsgcs.util.*;
import jpsgcs.markov.*;
import jpsgcs.jtree.*;
import jpsgcs.graph.*;
import jpsgcs.viewgraph.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

public class EstimateGM
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();
			ArgParser a = new ArgParser(args);
			int totalits = a.intAfter("-its",100000000);
			int randomits = a.intAfter("-rand",100);
			boolean header = a.gotOpt("-h");
			boolean nogaussians = true;

			MixedDataMatrix data = new MixedDataMatrix(header);
			MixedMarginalLikelihood like = new MixedMarginalLikelihood(data,1,1,1,0,1);

			Network<Variable,Object> g = new Network<Variable,Object>();

			Variable sink = new Variable();
			sink.setName("Sink");

			for (Variable v : like.getVars())
			{
				if (v.isDefined())
				{
					g.connect(sink,v);
				}
				else
				{
					g.add(v);
					nogaussians = false;
				}
			}

			if (nogaussians)
				g.remove(sink);

			Parameter temp = new Parameter("Temperature",0,100,1);
			GraphFrame<Variable,Object> frame = new GraphFrame<Variable,Object>(g,temp);
			for (WindowListener w : frame.getWindowListeners())
				frame.removeWindowListener(w);
			frame.addWindowListener(new WindowCloser(g));
			frame.getGraph().hide(sink);
				
			if (frame.getRepresentation(sink) != null)
				frame.getRepresentation(sink).setColor(Color.green);

			for (Variable v : like.getVars())
			{
				if (v.isDefined())
					frame.getRepresentation(v).setColor(Color.yellow);
				else
					frame.getRepresentation(v).setColor(Color.cyan);
			}

			WSMGraphLaw<Variable>[] parts = (WSMGraphLaw<Variable>[]) new WSMGraphLaw[10];

			parts[0] = like;
			//parts[1] = new EdgePenaltyPrior<Variable>(1);
			//parts[2] = new MaxCliquePrior<Variable>(4);

			WSMGraphLaw<Variable> posterior = new ProductGraphLaw<Variable>(parts);

			JTree<Variable> jt = new JTree<Variable>(g,rand);
			//JTreeSampler<Variable> jts = new GiudiciGreen<Variable>(sink,jt,posterior);
			JTreeSampler<Variable> jts = new OnePairSampler<Variable>(sink,jt,posterior,true);
			//JTreeSampler<Variable> jts = new MultiPairJTree2<Variable>(sink,jt,posterior,true);
			//JTreeSampler<Variable> jts = new MultiPairJTree1<Variable>(sink,jt,posterior,true);
			//JTreeSampler<Variable> jts = new MultiPairJTree<Variable>(sink,jt,posterior,true);

			jts.setTemperature(1);
			jts.randomize();

			double best = Double.NEGATIVE_INFINITY;
			double ups = 0;

			for (int i=1; i<=totalits; i++)
			{
				jts.setTemperature(temp.getValue());
				if (i % randomits == 0)
					jts.randomize();

				if (jts.randomUpdate() == 0)
				{
					ups += 1;
					double current = posterior.logProbability(jt);
					if (current > best)
						best = current;
				}

				if (i % 1000000 == 0)
				{
					System.err.print(i);
					System.err.print("\t"+((int)(ups/i*10000.0))/100.0);
					System.err.print("\t"+best);
					System.err.print("\t"+posterior.logProbability(jt));
					//System.err.print("\t\t"+like.logProbability(jt));
					//System.err.print("\t"+prior.logProbability(jt));
					System.err.println();
				}
			}

			System.out.println(g);
		}
		catch (ConcurrentModificationException cme)
		{
		}
		catch (Exception e)
		{
			System.err.println("Caught in EstimateGM.main()");
			e.printStackTrace();
		}
	}
}

class WindowCloser implements WindowListener
{
	public Graph g = null;
	
	public WindowCloser(Graph graph)
	{
		g = graph;
	}

	public void windowClosing(WindowEvent e) 
	{ 
		System.out.println(g);
		System.exit(0);
	}

	public void windowActivated(WindowEvent e) { }
	public void windowClosed(WindowEvent e) { }
	public void windowDeactivated(WindowEvent e) { }
	public void windowDeiconified(WindowEvent e) { }
	public void windowIconified(WindowEvent e) { }
	public void windowOpened(WindowEvent e) { }
}
