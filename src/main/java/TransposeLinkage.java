import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkagePhenotype;
import java.util.Set;
import java.util.LinkedHashSet;

/**
	This program takes a LINKAGE paramter file, and a list of 
	LINKAGE pedigree files and concatenates the pedigree files into
	a single transposed LINKAGE pedigree file. A transposed LINKAGE
	pedigree file is essentially a locus x individual matrix of 
	genotypes, as opposed to the conventional LINKAGE pedigree file
	which is an individual x locus matrix.
	The output pedigree file is written to standard output.

<ul>
	<li> Usage: <b> java TransposeLinkage input.par input1.ped [input2.ped] ... [x%] [-u] [-r] </b> </li>
</ul>
	where
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> input1.ped </b> is the first LINKAGE pedigree file. </li>
<li> <b> input2.ped ... </b> are optional additional LINKAGE pedigree files. </li>
<li> <b> x% </b> where <b> x</b> is a number between 0 and 100 gives a minimum percentage
	of genotype calls that an individual must have to be included in the output.
	The default value is 0%. </li>
<li> <b> -u </b> if this option is specified the relationships between individuals
	in the pedigrees are removed so that they appear to be unrelated. 
	The default action is to keep the specified relationships. </li>
<li> <b> -s </b> if this option is specified the output is a standard, not transposed,
	LINKAGE pedigree file. This makes the program in effect the inverse of
	SelectPedigrees. The default is to output a transposed file. </li>
</ul>

	The only program that currently requires a transposed LINKAGE
	pedigree file for input is FitGMLD.
	The recommended usage for input into FitGMLD is with the options
	<b> 90% -u </b> set.
*/

public class TransposeLinkage
{
	public static void main(String[] args)
	{
		try
		{
			boolean unrel = false;
			boolean transposed = true;
			double need = 0;

			int n = 0;
			for (int i=1; i<args.length; i++)
			{
				if (args[i].equals("-u"))
					continue;
				if (args[i].equals("-s"))
					continue;
				if (args[i].endsWith("%"))
					continue;
				n++;
			}

			if (n == 0)
			{
				System.err.println("Usage: java TransposeLinkage input.par input1.ped [input2.ped] ... [x%] [-u] [-s]");
				System.exit(1);
			}

			String[] peds = new String[n];
			n = 0;
			for (int i=1; i<args.length; i++)
			{
				if (args[i].equals("-u"))
				{
					unrel = true;
					continue;
				}

				if (args[i].equals("-s"))
				{
					transposed = false;
					continue;
				}

				if (args[i].endsWith("%"))
				{
                                    need = Double.parseDouble(args[i].replace('%',' '));
					continue;
				}

				peds[n++] = args[i];
			}
				
			LinkageParameterData par = new LinkageParameterData(args[0]);
			Set<LinkageIndividual> s = new LinkedHashSet<LinkageIndividual>();

			for (int i=0; i<peds.length; i++)
			{
				LinkageIndividual[] ind = new LinkagePedigreeData(peds[i],par).getIndividuals();

				for (int j=0; j<ind.length; j++)
				{
					if (need > 0 && percentgot(ind[j]) < need)
						continue;

					if (unrel)
					{
						ind[j].paid = null;
						ind[j].maid = null;
						ind[j].firstbornid = 0;
						ind[j].pasnextid = 0;
						ind[j].masnextid = 0;
					}

					s.add(ind[j]);
				}
			}

			LinkagePedigreeData ped = new LinkagePedigreeData(s);

			if (transposed)
				ped.writeTranspose(System.out);
			else
				ped.writeTo(System.out);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static double percentgot(LinkageIndividual a)
	{
		LinkagePhenotype[] p = a.pheno;
		if (p == null)
			return 0;

		double n = 0;
		double x = 0;

		for (int i=0; i<p.length; i++)
		{
			n += 1;
			if (p[i] != null && p[i].informative())
				x += 1;
		}

		return 100*x/n;
	}
}
