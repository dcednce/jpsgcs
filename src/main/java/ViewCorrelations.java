
import jpsgcs.util.ArgParser;
import jpsgcs.util.Gauss;
import jpsgcs.util.Distributions;
import jpsgcs.markov.Parameter;
import jpsgcs.graph.Network;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.jtree.MixedDataMatrix;
import java.awt.Frame;
import java.awt.Color;

public class ViewCorrelations
{
	public static void main(String[] args)
	{
		try
		{
			ArgParser ap = new ArgParser(args);

			double threshold = 1;

			MixedDataMatrix data = new MixedDataMatrix(true);

/*
double n = data.nRows();
double p = 0.05;
double Z = Gauss.Finv(p/2);
System.err.println("Nominal 5% significance at\t"+Math.sqrt(Z*Z/(n-2+Z*Z)));
p = 0.01;
Z = Gauss.Finv(p/2);
System.err.println("Nominal 1% significance at\t"+Math.sqrt(Z*Z/(n-2+Z*Z)));

p = 0.05;
p = 1 - Math.pow((1-p),2/n/(n-1));
Z = Gauss.Finv(p/2);
System.err.println("Multiple testing corrected 5% significance at\t"+Math.sqrt(Z*Z/(n-2+Z*Z)));

p = 0.01;
p = 1 - Math.pow((1-p),2/n/(n-1));
Z = Gauss.Finv(p/2);
System.err.println("Multiple testing corrected 1% significance at\t"+Math.sqrt(Z*Z/(n-2+Z*Z)));
*/

			double m = data.nCols();
			double n = data.nRows();
			double p = 0.05;
			p = 1 - Math.pow((1-p),2/m/(m-1));
			double Z = Distributions.qStudent(p/2,n-2);

			System.err.println("Multiple testing corrected 5% significance at\t"+Math.sqrt(Z*Z/(n-2+Z*Z)));

			String[] name = new String[data.nCols()];
			double[][] corr = new double[data.nCols()][data.nCols()];

			Network<String,Object> g = new Network<String,Object>();

			for (int i=0; i<name.length; i++)
			{
				name[i] = data.getName(i);
				g.add(name[i]);
	
				for (int j=i+1; j<name.length; j++)
				{
					corr[i][j] = corr[j][i] = data.correlation(i,j);
					if (Math.abs(corr[i][j]) >= threshold)
					{
						g.connect(name[i],name[j], (corr[i][j] < 0 ? Color.red : Color.blue));
					}
				}
			}

			//Parameter thresh = new Parameter("Threshold",0,1000,1000);
			Parameter thresh = new Parameter("Threshold",0,1,1);
			Frame frame = new GraphFrame<String,Object>(g,thresh);

			while (true)
			{
				Thread.sleep(100);

				double newthresh = thresh.getValue();

				if (Math.abs(newthresh-threshold) > 0.0000001)
				{
					threshold = newthresh;

					for (int i=0; i<name.length; i++)
					{
						for (int j=i+1; j<name.length; j++)
						{
							if (Math.abs(corr[i][j]) >= threshold)
								g.connect(name[i],name[j], (corr[i][j] < 0 ? Color.red : Color.blue));
							else
								g.disconnect(name[i],name[j]);
						}
					}
				} 
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in ViewRels.main()");
			e.printStackTrace();
			System.exit(1);
		}
	}
}
