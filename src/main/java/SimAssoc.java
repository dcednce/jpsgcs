
import jpsgcs.linkage.Linkage;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.genio.ParameterData;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.util.Monitor;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.Variable;
import java.util.Random;

public class SimAssoc
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			Monitor.quiet(false);

			int nsims = 1000;
			int ncase = 1000;
			int ncont = ncase;
			
			switch (args.length)
			{
			case 2: ncase = Integer.parseInt(args[1]);
				ncont = ncase;

			case 1: nsims = Integer.parseInt(args[0]);
				break;

			case 0: break;
			default:
				System.err.println("Usage: java SimAssoc <input.par(ld) [nsims] [ncase/cont]> output");
			}

System.err.println("nsims = "+nsims);
System.err.println("ncase = "+ncase);
System.err.println("ncont = "+ncont);

			LinkageFormatter f = new LinkageFormatter();
			ParameterData p = Linkage.readParameterData(f);

			LDModel ld = new LDModel(f);
			if (ld.getLocusVariables() == null)
				ld = new LDModel(p);

			Variable[] v = ld.getLocusVariables();

			GraphicalModel g = new GraphicalModel(ld,rand,true);

			for (int k=0; k<nsims; k++)
			{
				int[][][] tab = new int[v.length][2][2];

				for (int i=0; i<2*ncase; i++)
				{
					g.simulate();
					for (int j=0; j<v.length; j++)
						tab[j][0][v[j].getState()] += 1;
				}
	
				for (int i=0; i<2*ncont; i++)
				{
					g.simulate();
					for (int j=0; j<v.length; j++)
						tab[j][1][v[j].getState()] += 1;
				}
	
				for (int j=0; j<v.length; j++)
					System.out.print(" "+chisq(tab[j]));

				System.out.println();
			}

		}
		catch (Exception e)
		{
			System.err.println("Caught in SimAssoc:main()");
			e.printStackTrace();
		}
	}
	
	public static double chisq(int[][] t)
	{
		double[] p = {t[0][0] + t[0][1], t[1][0] + t[1][1]};
		double[] q = {t[0][0] + t[1][0], t[0][1] + t[1][1]};
		double n = p[0]+p[1];

		double[][] ex = { {p[0]*q[0]/n, p[0]*q[1]/n} , {p[1]*q[0]/n, p[1]*q[1]/n} } ;

		double chis = 0;

		for (int i=0; i<2; i++)
			for (int j=0; j<2; j++)
			{
				if (ex[i][j] <= 0)
					continue;
				//double x = Math.abs(t[i][j] - ex[i][j]) - 0.5;
				double x = Math.abs(t[i][j] - ex[i][j]);
				chis +=  x*x/ex[i][j];
			}

		return chis;

	}
}
