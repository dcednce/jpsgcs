import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LocusSampler;
import jpsgcs.pedmcmc.LDLocusSampler;
import jpsgcs.pedmcmc.LocusMeiosisSampler;
import jpsgcs.pedmcmc.ExtendedLMSampler;
import jpsgcs.pedapps.MultiPoints;
import jpsgcs.pedapps.GeneticMap;
import java.util.Random;

/**
        This program samples the posterior distribution of inheritance vectors
        given the observed genotypes and, hence, calculated multi point linkage
	lod scores between a trait phenotype and genetic markers in pedigrees.

<ul>
        Usage : <b> java McMultiPoints input.par input.ped [n_samples] [n_burnin] [error] [-v/t] </b>
</ul>
<p>
        where
<ul>
        <li> <b> input.par </b> is the input LINKAGE parameter file. </li>
        <li> <b> input.ped </b> is the input LINAKGE pedigree file. </li>
        <li> <b> n_samples </b> is an optional parameter specifiying the number of iterations
        to sample. The default is 1000. </li>
        <li> <b> n_burnin  </b> is an optional parameter specifying the number iterations to
        do before sampling begins. The default is 0. </li>
        <li> <b> error </b> is an optional parameter specifying the probability of a genotyping
        error. The default is 0. </li>
        <li> <b> -v/t </b> switches between verbose and terse output. The default is verbose. </li>
</ul>

<p>
        Sampling is done using the Markov chain Monte Carlo method of
        blocked Gibbs updating.
<p>
	The output gives a table for each pedigree. The first column of each table
	is a genomic location in cMorgans with the first marker used as the origin.
	The second column gives the multi point lod score at that location for that pedigree.
*/

public class McMultiPoints extends McProg
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			double[] pos = null;

			readOptions(args,"McMultiPoints");

			pos = GeneticMap.locusCentiMorgans(alldata[0],false,5);

			for (GeneticDataSource data : alldata)
			{
				if (verbose)
					System.err.println("Setting up sampler");

				data.downcodeAlleles();

				LocusSampler mc = makeSampler(data,false,rand);
		
				mc.initialize();

				if (n_burnin > 0 && verbose)
					System.err.println("Burn in");

				for (int s=0; s<n_burnin; s++)
				{
					mc.sample();
					if (verbose) 
						System.err.print(".");
				}

				if (n_burnin > 0 && verbose) 
					System.err.println();

				MultiPoints mp = new MultiPoints(data,mc.getInheritances(),pos);

				if (verbose)
					System.err.println("Sampling");

				for (int s=0; s<n_samples; s++)
				{
					mc.sample();
					mp.update();
					if (verbose) 
						System.err.print(".");
				}

				if (verbose) 
					System.err.println();

				double[] lods = mp.results();

				for (int i=0; i<lods.length; i++)
					System.out.printf(" %8.4f\t%5.4f\n",pos[i],lods[i]);
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in McMultiPoints:main()");
			e.printStackTrace();
		}
	}
}
