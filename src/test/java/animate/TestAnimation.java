package jpsgcs.animate;

import jpsgcs.util.SafeRunnable;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.BorderLayout;

public class TestAnimation implements Paintable, SafeRunnable
{
    public ActiveCanvas canv = null;
    public Loop loop = null; 

    public TestAnimation()
    {
        loop = new Loop(this);

        canv = new ActiveCanvas(this);
        canv.setSize(1000,1000);
        canv.setBackground(Color.cyan);
    }

    private Color col = Color.red;

    public void loop()
    {
        try
            {
                col = col == Color.red ? Color.white : Color.red;
                canv.repaint();
                Thread.sleep(1000);
            }
        catch (Exception e)
            {
                e.printStackTrace();
            } 
    }

    public void paint(Graphics g)
    {
        g.setColor(col);
        g.fillRect(0,0,50,50);
        g.setColor(Color.black);
        g.drawRect(0,0,50,50);
        g.drawLine(80,40,100,100);
    }
        
    public static void main(String[] args)
    {
        try
            {
                TestAnimation t = new TestAnimation();

                Panel p = new Panel();
                p.setLayout(new BorderLayout());
                p.add(t.canv,BorderLayout.CENTER);

                Frame f = new Frame();
                f.addWindowListener(new FrameQuitter());
                f.add(p);
                f.pack();
                f.setVisible(true);
                t.loop.start();
                        
            }
        catch (Exception e)
            {
                e.printStackTrace();
            }
    }
}
