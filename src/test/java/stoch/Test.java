package jpsgcs.stoch;

import java.util.*;

public class Test
{
    public static void main(String[] args)
    {
        try
            {
                StochSys sys = new StochSys();

                System.out.println(sys.getValue());
                System.out.println();
                        
                Variable a = new MyVariable("a");
                a.setValue(1);
                Variable b = new MyVariable("b");
                b.setValue(2);
                Variable c = new MyVariable("c");
                c.setValue(3);
                Variable d = new MyVariable("d");
                d.setValue(100);
                Variable e = new MyVariable("e");
                e.setValue(1000);
        
                // Function fab = new MyFunction(a,b,"f(a,b)");
                // sys.plus(fab);
                // Function fbc = new MyFunction(b,c,"f(b,c)");
                // sys.plus(fbc);
                // Function fca = new MyFunction(c,a,"f(c,a)");
                // sys.plus(fca);

                // sys.plus(d);

                System.out.println(sys.getValue());
                System.out.println(sys.getValue(a));

                //sys.remove(fab);
                System.out.println(sys.getValue());
                System.out.println(sys.getValue(a));

                sys.remove(a);
                System.out.println(sys.getValue());
                System.out.println(sys.getValue(a));
                System.out.println();

                System.out.println(sys.getValue(d));
                System.out.println(sys.getValue(e));
                System.out.println();

                for (Function f : sys.getFunctions())
                    System.out.println(f+"\t"+sys.getVariables(f));
        
                for (Variable v : sys.getVariables())
                    System.out.println(v+"\t"+sys.getFunctions(v));

                System.out.println(e+"\t"+sys.getFunctions(e));
                //System.out.println(fab+"\t"+sys.getVariables(fab));
                System.out.println();
            }
        catch (Exception e)
            {
                e.printStackTrace();
                System.exit(1);
            }
    }
}

class MyVariable extends Continuous
{
    public MyVariable(String s)
    {
        name = s;
    }

    public String toString()
    {
        return name;
    }

    public String name = null;
}

class MyFunction implements Function
{
    public MyFunction(Variable a, Variable b, String s)
    {
        v = new LinkedHashSet<Variable>();
        // v.plus(a);
        // v.plus(b);
        name = s;
    }

    public Collection<Variable> getVariables()
    {
        return v;
    }

    public double getValue()
    {
        double tot = 0;
        for (Variable u : v)
            tot += u.getValue();
        return tot;
    } 

    public double logValue()
    {
        return Math.log(getValue());
    }

    public String toString()
    {
        return name;
    }

    private Set<Variable> v = null;
    public String name = null;
}
