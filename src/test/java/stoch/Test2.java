package jpsgcs.stoch;

import jpsgcs.util.Matrix;
import jpsgcs.util.MatrixInverseData;

import java.util.Random;

public class Test2
{
    public static void main(String[] args)
    {
        try
            {
                double tau0 = 5;
                double tau1 = 5;
                double r = 2;

                double[] mu = {0,0};
                double[][] Sigma = new double[2][2];
                        
                /*
                  Sigma[0][0] = tau0;
                  Sigma[1][0] = Sigma[0][1] = 0;
                  Sigma[1][1] = tau1;
                */
                Sigma[0][0] = tau0 + r*r*tau1;
                Sigma[0][1] = Sigma[1][0] = r * tau1;
                Sigma[1][1] = tau1;
                
                for (int i=0; i<1000; i++)
                    {
                        /*
                          double[] s = Gauss.random(mu,Sigma);
                                
                          double[] b = new double[2];
                          b[0] = s[0] + r * s[1];
                          b[1] = s[1];
                        */

                        double[] b = Gauss.random(mu,Sigma);

                        System.out.println(b[0]+"\t"+b[1]);
                    }
                        
            }
        catch (Exception e)
            {
                e.printStackTrace();
            }
    }
}
