
import jpsgcs.random.AdaptiveRejection;
import jpsgcs.random.AhrensBinomial;
import jpsgcs.random.AhrensDieter;
import jpsgcs.random.AliasRejection;
import jpsgcs.random.BernoulliRV;
import jpsgcs.random.BetaRV;
import jpsgcs.random.BinomialRV;
import jpsgcs.random.BirthAndDeathProcess;
import jpsgcs.random.BrownianMotion2D;
import jpsgcs.random.BrownianMotion;
import jpsgcs.random.BuiltInGenerator;
import jpsgcs.random.CauchyRV;
import jpsgcs.random.ChengFeast;
import jpsgcs.random.ChiSquaredRV;
import jpsgcs.random.CongruentialGenerator;
import jpsgcs.random.Count;
import jpsgcs.random.DegenerateIntegerRV;
import jpsgcs.random.DegenerateRV;
import jpsgcs.random.DensityFunction;
import jpsgcs.random.DifferentiableLogConcave;
import jpsgcs.random.DiscreteStochasticProcess;
import jpsgcs.random.Envelope;
import jpsgcs.random.ExponentialMixture;
import jpsgcs.random.ExponentialRV;
import jpsgcs.random.ExponentialSwitching;
import jpsgcs.random.FiniteStateMC;
import jpsgcs.random.FisherRV;
import jpsgcs.random.GammaRV;
import jpsgcs.random.GaussianRV;
import jpsgcs.random.GeneralExponentialRV;
import jpsgcs.random.GenerationMethod;
import jpsgcs.random.GeometricRV;
import jpsgcs.random.HotBitsEngine;
import jpsgcs.random.IntegerMethod;
import jpsgcs.random.IntegerValuedRV;
import jpsgcs.random.IntSampling;
import jpsgcs.random.IntWithoutReplacement;
import jpsgcs.random.IntWithReplacement;
import jpsgcs.random.InverseDistribution;
import jpsgcs.random.Inversion;
import jpsgcs.random.Locus2D;
import jpsgcs.random.Locus;
import jpsgcs.random.LogConcave;
import jpsgcs.random.MarkovChain;
import jpsgcs.random.MarkovState;
import jpsgcs.random.Metropolis;
import jpsgcs.random.Metropolitan;
import jpsgcs.random.MetropolitanMC;
import jpsgcs.random.Mixture;
import jpsgcs.random.NegativeBinomialRV;
import jpsgcs.random.NormalRV;
import jpsgcs.random.ParameterException;
import jpsgcs.random.PoissonProcess;
import jpsgcs.random.PoissonRV;
import jpsgcs.random.PopulationCount;
import jpsgcs.random.PseudoRandomEngine;
import jpsgcs.random.Rand;
import jpsgcs.random.RandomBag;
import jpsgcs.random.RandomColor;
import jpsgcs.random.RandomEngine;
import jpsgcs.random.Random;
import jpsgcs.random.RandomObject;
import jpsgcs.random.RandomPermutation;
import jpsgcs.random.RandomUrn;
import jpsgcs.random.RandomVariable;
import jpsgcs.random.RandomWalk;
import jpsgcs.random.RatioOfUniforms;
import jpsgcs.random.RatioRegion;
import jpsgcs.random.Rejection;
import jpsgcs.random.RenewalCount;
import jpsgcs.random.RenewalProcess;
import jpsgcs.random.Sampler;
import jpsgcs.random.Sampling;
import jpsgcs.random.StochasticProcess;
import jpsgcs.random.StochasticState;
import jpsgcs.random.StudentsRV;
import jpsgcs.random.ThrownString;
import jpsgcs.random.UniformIntegerRV;
import jpsgcs.random.UniformRV;
import jpsgcs.random.VonMisesRV;
import jpsgcs.random.WeibullRV;
import jpsgcs.random.WichmanHill;

/**
   This is a simple program to demonstrate using random variable and
   stochastic process utilities in the jpsgcs.random package.
*/

public class TestRandom
{
    public static void main(String[] args)
    {
        RandomVariable X = new BinomialRV(10,0.25);
        for (int i=0; i<10; i++)
            System.out.println(X.next());
        System.out.println();

        X = new GaussianRV(5,10);
        for (int i=0; i<10; i++)
            System.out.println(X.next());
        System.out.println();

        PoissonProcess P = new PoissonProcess(0.32);
        for (int i=0; i<10; i++)
            {
                P.next();
                StochasticState s = P.getState();
                System.out.println(s.time);
            }
        System.out.println();

        BirthAndDeathProcess B = new BirthAndDeathProcess(10,10);

        for (int i=0; i<10; i++)
            {
                B.next();
                PopulationCount c = (PopulationCount)B.getState();
                System.out.println(c.time+"\t"+c.births+"\t"+c.deaths);
            }
    }
}
