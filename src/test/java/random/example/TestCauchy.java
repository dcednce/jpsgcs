package jpsgcs.random.example;

import jpsgcs.random.*;

public class TestCauchy
{
    public static void main(String[] args)
    {
        CauchyRV C = new CauchyRV();
                
        for (int i=0; i<100; i++)
            {
                System.out.println(C.next());
            }
        System.out.println();

        for (int i=0; i<100; i++)
            {
                int n = 100;
                double x = 0;
                for (int j=0; j<n; j++)
                    x += C.next();
                System.out.println(x/n);
            }
        System.out.println();

        for (int i=0; i<100; i++)
            {
                int n = 1000;
                double x = 0;
                for (int j=0; j<n; j++)
                    x += C.next();
                System.out.println(x/n);
            }
        System.out.println();


        for (int i=0; i<100; i++)
            {
                int n = 10000;
                double x = 0;
                for (int j=0; j<n; j++)
                    x += C.next();
                System.out.println(x/n);
            }
        System.out.println();
    }
}
